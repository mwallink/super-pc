﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WfaSuperPC
{
    public partial class ClientEditForm : Form
    {

        private Klanten klant = new Klanten();
        private int klantId { get; set; }
        private String bericht;
        private ClientsForm cf;
        private bool fieldError = false;

        SuperPcContainer context = new SuperPcContainer();

        // Nieuwe klant
        public ClientEditForm(ClientsForm cf)
        {
            this.cf = cf;
            klant = context.Klanten.Create();

            // Form laden
            InitializeComponent();
        }

        // Wijzigen van een klant
        public ClientEditForm(ClientsForm cf, int id)
        {
            this.cf = cf;
            klant = context.Klanten.Find(id);
            klantId    = klant.Id;


            // Form laden
            InitializeComponent();
            
        }



        private void OpslaanBtn_Click(object sender, EventArgs e)
        {
            // Waarden worden in klant gezet
            this.klant.Voornaam         = voornaamTb.Text;
            this.klant.Achternaam       = achternaamTb.Text;
            this.klant.Straatnaam       = straatnaamTb.Text;
            this.klant.Land             = landTb.Text;
            this.klant.Postcode         = postcodeTb.Text;
            this.klant.Telefoonnummer   = telTb.Text;

            // Als waarde lege string is, dan wordt waarde null.
            this.klant.Huisnummertoevoeging = huisnummerToevoegingTb.Text != "" ? huisnummerToevoegingTb.Text : null;
            this.klant.Emailadres       = emailTb.Text != "" ? emailTb.Text : null;
            this.klant.Gsmnummer        = gsmTb.Text != "" ? gsmTb.Text : null;

            // Als zakelijke klant, dan is bedrijfsnaam verplicht
            if (this.checkBox1.Checked)
            {
                if (bedrijfsnaamTb.Text == "")
                {
                    fieldError = true;
                    this.bedrijfsnaamLabel.ForeColor = Color.Red;
                }
                else
                {
                    this.klant.Bedrijfsnaam = bedrijfsnaamTb.Text;
                    this.bedrijfsnaamLabel.ForeColor = Color.Black;
                }
            }
            else // Anders niet.
            {
                this.klant.Bedrijfsnaam = null;
            }

            // Huisnummer mag niet leeg zijn en moet een int zijn!
            try
            {
                this.klant.Huisnummer = Convert.ToInt32(huisnummerTb.Text);
                this.huisnummerLabel.ForeColor = Color.Black;
            }
            catch {
                fieldError = true;
                huisnummerLabel.ForeColor = Color.Red;
            }

            // Verplichte velden die niet ingevuld zijn, rood maken.
            // Zijn ze later wel ingevuld, dan weer zwart maken.
            voornaamLabel.ForeColor     = voornaamTb.Text   == "" ? Color.Red : Color.Black;
            achternaamLabel.ForeColor   = achternaamTb.Text == "" ? Color.Red : Color.Black;
            straatnaamLabel.ForeColor   = straatnaamTb.Text == "" ? Color.Red : Color.Black;
            landLabel.ForeColor         = landTb.Text       == "" ? Color.Red : Color.Black;
            postcodeLabel.ForeColor     = postcodeTb.Text   == "" ? Color.Red : Color.Black;
            telLabel.ForeColor          = telTb.Text        == "" ? Color.Red : Color.Black;
            emailLabel.ForeColor        = emailTb.Text      == "" ? Color.Red : Color.Black;
            huisnummerLabel.ForeColor   = huisnummerTb.Text == "" ? Color.Red : Color.Black;

            // Als een van bovenstaande waarden verkeerd is dan field error op true.
            if (voornaamTb.Text         == "" || 
                achternaamTb.Text       == "" || 
                straatnaamTb.Text       == "" || 
                landTb.Text             == "" || 
                postcodeTb.Text         == "" || 
                telTb.Text              == "")
            {
                fieldError = true;
            }

            if (fieldError == false)
            {
                try
                {
                    // Er is een id meegegeven dus we wijzigen een bestaande klant
                    if (this.klantId > 0)
                    {
                        //Display a success message.
                        bericht = klant.Voornaam + " " + klant.Achternaam + " is gewijzigd.";
                    }
                    else
                    {
                        // Data opslaan
                        var client = context.Set<Klanten>();
                        client.Add(klant);

                        // Display a success message.
                        bericht = voornaamTb.Text + " " + achternaamTb.Text + " is toegevoegd.";

                    }
                    // Wijzigingen opslaan
                    context.SaveChanges();
                }
                catch
                {
                    bericht = "Er is iets fout gegaan...";
                }
                this.Hide();

                MessageBox.Show(bericht);
                
                // Scherm sluiten.
                this.Close();

            }
            else
            {
                //MessageBox.Show("Er is een fout opgetreden.");
            }
        }

        private void ClientEditForm_Load(object sender, EventArgs e)
        {
            if (this.klantId > 0)
            {
                // Data ophalen van newClient en in de velden van het form zetten.
                voornaamTb.Text = this.klant.Voornaam;
                achternaamTb.Text = this.klant.Achternaam;
                bedrijfsnaamTb.Text = this.klant.Bedrijfsnaam;
                landTb.Text = this.klant.Land;
                straatnaamTb.Text = this.klant.Straatnaam;
                huisnummerTb.Text = this.klant.Huisnummer.ToString();
                huisnummerToevoegingTb.Text = this.klant.Huisnummertoevoeging;
                postcodeTb.Text = this.klant.Postcode;
                emailTb.Text = this.klant.Emailadres;
                telTb.Text = this.klant.Telefoonnummer;
                gsmTb.Text = this.klant.Gsmnummer;

               

            }

            
        }

        private void annulerenBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
           
        }
    }

}

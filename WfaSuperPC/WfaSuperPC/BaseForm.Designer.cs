﻿namespace WfaSuperPC
{
    partial class BaseForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BaseForm));
            this.toolStripTop = new System.Windows.Forms.ToolStrip();
            this.toolStripButtonPersonen = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonRollen = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonProducten = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonKlanten = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonAssemblage = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonStatistieken = new System.Windows.Forms.ToolStripButton();
            this.toolStripDropDownButton1 = new System.Windows.Forms.ToolStripDropDownButton();
            this.bestellingenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openstaandeBestellingenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripTop.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStripTop
            // 
            this.toolStripTop.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButtonPersonen,
            this.toolStripButtonRollen,
            this.toolStripButtonProducten,
            this.toolStripButtonKlanten,
            this.toolStripButtonAssemblage,
            this.toolStripButtonStatistieken,
            this.toolStripDropDownButton1});
            this.toolStripTop.Location = new System.Drawing.Point(0, 0);
            this.toolStripTop.Name = "toolStripTop";
            this.toolStripTop.Size = new System.Drawing.Size(647, 25);
            this.toolStripTop.TabIndex = 0;
            this.toolStripTop.Text = "toolStrip1";
            // 
            // toolStripButtonPersonen
            // 
            this.toolStripButtonPersonen.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButtonPersonen.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonPersonen.Image")));
            this.toolStripButtonPersonen.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonPersonen.Name = "toolStripButtonPersonen";
            this.toolStripButtonPersonen.Size = new System.Drawing.Size(99, 22);
            this.toolStripButtonPersonen.Text = "Personen beheer";
            this.toolStripButtonPersonen.Click += new System.EventHandler(this.toolStripButtonPersonen_Click);
            // 
            // toolStripButtonRollen
            // 
            this.toolStripButtonRollen.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButtonRollen.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonRollen.Image")));
            this.toolStripButtonRollen.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonRollen.Name = "toolStripButtonRollen";
            this.toolStripButtonRollen.Size = new System.Drawing.Size(83, 22);
            this.toolStripButtonRollen.Text = "Rollen beheer";
            this.toolStripButtonRollen.Click += new System.EventHandler(this.toolStripButtonRollen_Click);
            // 
            // toolStripButtonProducten
            // 
            this.toolStripButtonProducten.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButtonProducten.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonProducten.Image")));
            this.toolStripButtonProducten.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonProducten.Name = "toolStripButtonProducten";
            this.toolStripButtonProducten.Size = new System.Drawing.Size(105, 22);
            this.toolStripButtonProducten.Text = "Producten beheer";
            this.toolStripButtonProducten.Click += new System.EventHandler(this.toolStripButtonProducten_Click);
            // 
            // toolStripButtonKlanten
            // 
            this.toolStripButtonKlanten.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButtonKlanten.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonKlanten.Image")));
            this.toolStripButtonKlanten.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonKlanten.Name = "toolStripButtonKlanten";
            this.toolStripButtonKlanten.Size = new System.Drawing.Size(90, 22);
            this.toolStripButtonKlanten.Text = "Klanten beheer";
            this.toolStripButtonKlanten.Click += new System.EventHandler(this.toolStripButtonKlanten_Click);
            // 
            // toolStripButtonAssemblage
            // 
            this.toolStripButtonAssemblage.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButtonAssemblage.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonAssemblage.Image")));
            this.toolStripButtonAssemblage.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonAssemblage.Name = "toolStripButtonAssemblage";
            this.toolStripButtonAssemblage.Size = new System.Drawing.Size(75, 19);
            this.toolStripButtonAssemblage.Text = "Assemblage";
            this.toolStripButtonAssemblage.Click += new System.EventHandler(this.toolStripButtonAssemblage_Click);
            // 
            // toolStripButtonStatistieken
            // 
            this.toolStripButtonStatistieken.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButtonStatistieken.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonStatistieken.Image")));
            this.toolStripButtonStatistieken.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonStatistieken.Name = "toolStripButtonStatistieken";
            this.toolStripButtonStatistieken.Size = new System.Drawing.Size(71, 19);
            this.toolStripButtonStatistieken.Text = "Statistieken";
            this.toolStripButtonStatistieken.Click += new System.EventHandler(this.toolStripButtonStatistieken_Click);
            // 
            // toolStripDropDownButton1
            // 
            this.toolStripDropDownButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripDropDownButton1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bestellingenToolStripMenuItem,
            this.openstaandeBestellingenToolStripMenuItem});
            this.toolStripDropDownButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripDropDownButton1.Image")));
            this.toolStripDropDownButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripDropDownButton1.Name = "toolStripDropDownButton1";
            this.toolStripDropDownButton1.Size = new System.Drawing.Size(84, 22);
            this.toolStripDropDownButton1.Text = "Bestellingen";
            // 
            // bestellingenToolStripMenuItem
            // 
            this.bestellingenToolStripMenuItem.Name = "bestellingenToolStripMenuItem";
            this.bestellingenToolStripMenuItem.Size = new System.Drawing.Size(211, 22);
            this.bestellingenToolStripMenuItem.Text = "Bestelling plaatsen";
            this.bestellingenToolStripMenuItem.Click += new System.EventHandler(this.bestellingenToolStripMenuItem_Click);
            // 
            // openstaandeBestellingenToolStripMenuItem
            // 
            this.openstaandeBestellingenToolStripMenuItem.Name = "openstaandeBestellingenToolStripMenuItem";
            this.openstaandeBestellingenToolStripMenuItem.Size = new System.Drawing.Size(211, 22);
            this.openstaandeBestellingenToolStripMenuItem.Text = "Openstaande bestellingen";
            this.openstaandeBestellingenToolStripMenuItem.Click += new System.EventHandler(this.openstaandeBestellingenToolStripMenuItem_Click_1);
            // 
            // BaseForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(647, 440);
            this.Controls.Add(this.toolStripTop);
            this.Margin = new System.Windows.Forms.Padding(3);
            this.MaximumSize = new System.Drawing.Size(663, 494);
            this.Name = "BaseForm";
            this.Text = "BaseForm";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.BaseForm_FormClosed);
            this.Load += new System.EventHandler(this.BaseForm_Load);
            this.toolStripTop.ResumeLayout(false);
            this.toolStripTop.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStripTop;
        private System.Windows.Forms.ToolStripButton toolStripButtonPersonen;
        private System.Windows.Forms.ToolStripButton toolStripButtonRollen;
        private System.Windows.Forms.ToolStripButton toolStripButtonProducten;
        private System.Windows.Forms.ToolStripButton toolStripButtonKlanten;
        private System.Windows.Forms.ToolStripButton toolStripButtonAssemblage;
        private System.Windows.Forms.ToolStripButton toolStripButtonStatistieken;
        private System.Windows.Forms.ToolStripDropDownButton toolStripDropDownButton1;
        private System.Windows.Forms.ToolStripMenuItem bestellingenToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openstaandeBestellingenToolStripMenuItem;

    }
}
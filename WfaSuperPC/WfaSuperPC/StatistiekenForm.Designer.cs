﻿namespace WfaSuperPC
{
    partial class StatistiekenForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(StatistiekenForm));
            this.bestellingLogDataSet = new WfaSuperPC.BestellingLogDataSet();
            this.statusLoggersBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.statusLoggersTableAdapter = new WfaSuperPC.BestellingLogDataSetTableAdapters.StatusLoggersTableAdapter();
            this.tableAdapterManager = new WfaSuperPC.BestellingLogDataSetTableAdapters.TableAdapterManager();
            this.statusLoggersBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.statusLoggersDataGridView = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lblDagopbrengst = new System.Windows.Forms.Label();
            this.lblBesteVerkoper = new System.Windows.Forms.Label();
            this.lblPercentageUitstaandeKosten = new System.Windows.Forms.Label();
            this.tbDagopbrengst = new System.Windows.Forms.TextBox();
            this.tbBesteVerkoper = new System.Windows.Forms.TextBox();
            this.tbPercentageUitstaandeKosten = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.bestellingLogDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.statusLoggersBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.statusLoggersBindingNavigator)).BeginInit();
            this.statusLoggersBindingNavigator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.statusLoggersDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // bestellingLogDataSet
            // 
            this.bestellingLogDataSet.DataSetName = "BestellingLogDataSet";
            this.bestellingLogDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // statusLoggersBindingSource
            // 
            this.statusLoggersBindingSource.DataMember = "StatusLoggers";
            this.statusLoggersBindingSource.DataSource = this.bestellingLogDataSet;
            // 
            // statusLoggersTableAdapter
            // 
            this.statusLoggersTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.StatusLoggersTableAdapter = this.statusLoggersTableAdapter;
            this.tableAdapterManager.UpdateOrder = WfaSuperPC.BestellingLogDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // statusLoggersBindingNavigator
            // 
            this.statusLoggersBindingNavigator.AddNewItem = null;
            this.statusLoggersBindingNavigator.BindingSource = this.statusLoggersBindingSource;
            this.statusLoggersBindingNavigator.CountItem = this.bindingNavigatorCountItem;
            this.statusLoggersBindingNavigator.DeleteItem = null;
            this.statusLoggersBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorMoveFirstItem,
            this.bindingNavigatorMovePreviousItem,
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.bindingNavigatorSeparator1,
            this.bindingNavigatorMoveNextItem,
            this.bindingNavigatorMoveLastItem,
            this.bindingNavigatorSeparator2});
            this.statusLoggersBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.statusLoggersBindingNavigator.MoveFirstItem = this.bindingNavigatorMoveFirstItem;
            this.statusLoggersBindingNavigator.MoveLastItem = this.bindingNavigatorMoveLastItem;
            this.statusLoggersBindingNavigator.MoveNextItem = this.bindingNavigatorMoveNextItem;
            this.statusLoggersBindingNavigator.MovePreviousItem = this.bindingNavigatorMovePreviousItem;
            this.statusLoggersBindingNavigator.Name = "statusLoggersBindingNavigator";
            this.statusLoggersBindingNavigator.PositionItem = this.bindingNavigatorPositionItem;
            this.statusLoggersBindingNavigator.Size = new System.Drawing.Size(914, 27);
            this.statusLoggersBindingNavigator.TabIndex = 0;
            this.statusLoggersBindingNavigator.Text = "bindingNavigator1";
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(45, 24);
            this.bindingNavigatorCountItem.Text = "of {0}";
            this.bindingNavigatorCountItem.ToolTipText = "Total number of items";
            // 
            // bindingNavigatorMoveFirstItem
            // 
            this.bindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveFirstItem.Image")));
            this.bindingNavigatorMoveFirstItem.Name = "bindingNavigatorMoveFirstItem";
            this.bindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 24);
            this.bindingNavigatorMoveFirstItem.Text = "Move first";
            // 
            // bindingNavigatorMovePreviousItem
            // 
            this.bindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMovePreviousItem.Image")));
            this.bindingNavigatorMovePreviousItem.Name = "bindingNavigatorMovePreviousItem";
            this.bindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 24);
            this.bindingNavigatorMovePreviousItem.Text = "Move previous";
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 27);
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "Position";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 27);
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "Current position";
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator1";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 27);
            // 
            // bindingNavigatorMoveNextItem
            // 
            this.bindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveNextItem.Image")));
            this.bindingNavigatorMoveNextItem.Name = "bindingNavigatorMoveNextItem";
            this.bindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 24);
            this.bindingNavigatorMoveNextItem.Text = "Move next";
            // 
            // bindingNavigatorMoveLastItem
            // 
            this.bindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveLastItem.Image")));
            this.bindingNavigatorMoveLastItem.Name = "bindingNavigatorMoveLastItem";
            this.bindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 24);
            this.bindingNavigatorMoveLastItem.Text = "Move last";
            // 
            // bindingNavigatorSeparator2
            // 
            this.bindingNavigatorSeparator2.Name = "bindingNavigatorSeparator2";
            this.bindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 27);
            // 
            // statusLoggersDataGridView
            // 
            this.statusLoggersDataGridView.AutoGenerateColumns = false;
            this.statusLoggersDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.statusLoggersDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn5,
            this.dataGridViewTextBoxColumn6});
            this.statusLoggersDataGridView.DataSource = this.statusLoggersBindingSource;
            this.statusLoggersDataGridView.Location = new System.Drawing.Point(12, 52);
            this.statusLoggersDataGridView.Name = "statusLoggersDataGridView";
            this.statusLoggersDataGridView.RowTemplate.Height = 24;
            this.statusLoggersDataGridView.Size = new System.Drawing.Size(890, 325);
            this.statusLoggersDataGridView.TabIndex = 1;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "Id";
            this.dataGridViewTextBoxColumn1.HeaderText = "Id";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "PersoonId";
            this.dataGridViewTextBoxColumn2.HeaderText = "PersoonId";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "BestellingId";
            this.dataGridViewTextBoxColumn3.HeaderText = "BestellingId";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "Wijzigingsdatum";
            this.dataGridViewTextBoxColumn4.HeaderText = "Wijzigingsdatum";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.Width = 160;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "BestellingsStatusVan";
            this.dataGridViewTextBoxColumn5.HeaderText = "BestellingsStatusVan";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.Width = 200;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.DataPropertyName = "BestellingsStatusNaar";
            this.dataGridViewTextBoxColumn6.HeaderText = "BestellingsStatusNaar";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.Width = 200;
            // 
            // lblDagopbrengst
            // 
            this.lblDagopbrengst.AutoSize = true;
            this.lblDagopbrengst.Location = new System.Drawing.Point(12, 399);
            this.lblDagopbrengst.Name = "lblDagopbrengst";
            this.lblDagopbrengst.Size = new System.Drawing.Size(98, 17);
            this.lblDagopbrengst.TabIndex = 2;
            this.lblDagopbrengst.Text = "Dagopbrengst";
            // 
            // lblBesteVerkoper
            // 
            this.lblBesteVerkoper.AutoSize = true;
            this.lblBesteVerkoper.Location = new System.Drawing.Point(12, 433);
            this.lblBesteVerkoper.Name = "lblBesteVerkoper";
            this.lblBesteVerkoper.Size = new System.Drawing.Size(104, 17);
            this.lblBesteVerkoper.TabIndex = 2;
            this.lblBesteVerkoper.Text = "Beste verkoper";
            // 
            // lblPercentageUitstaandeKosten
            // 
            this.lblPercentageUitstaandeKosten.AutoSize = true;
            this.lblPercentageUitstaandeKosten.Location = new System.Drawing.Point(12, 469);
            this.lblPercentageUitstaandeKosten.Name = "lblPercentageUitstaandeKosten";
            this.lblPercentageUitstaandeKosten.Size = new System.Drawing.Size(197, 17);
            this.lblPercentageUitstaandeKosten.TabIndex = 2;
            this.lblPercentageUitstaandeKosten.Text = "Percentage uitstaande kosten";
            // 
            // tbDagopbrengst
            // 
            this.tbDagopbrengst.Location = new System.Drawing.Point(224, 399);
            this.tbDagopbrengst.Name = "tbDagopbrengst";
            this.tbDagopbrengst.Size = new System.Drawing.Size(290, 22);
            this.tbDagopbrengst.TabIndex = 3;
            // 
            // tbBesteVerkoper
            // 
            this.tbBesteVerkoper.Location = new System.Drawing.Point(224, 430);
            this.tbBesteVerkoper.Name = "tbBesteVerkoper";
            this.tbBesteVerkoper.Size = new System.Drawing.Size(290, 22);
            this.tbBesteVerkoper.TabIndex = 3;
            // 
            // tbPercentageUitstaandeKosten
            // 
            this.tbPercentageUitstaandeKosten.Location = new System.Drawing.Point(224, 464);
            this.tbPercentageUitstaandeKosten.Name = "tbPercentageUitstaandeKosten";
            this.tbPercentageUitstaandeKosten.Size = new System.Drawing.Size(290, 22);
            this.tbPercentageUitstaandeKosten.TabIndex = 3;
            // 
            // StatistiekenForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(914, 630);
            this.Controls.Add(this.tbPercentageUitstaandeKosten);
            this.Controls.Add(this.tbBesteVerkoper);
            this.Controls.Add(this.tbDagopbrengst);
            this.Controls.Add(this.lblPercentageUitstaandeKosten);
            this.Controls.Add(this.lblBesteVerkoper);
            this.Controls.Add(this.lblDagopbrengst);
            this.Controls.Add(this.statusLoggersDataGridView);
            this.Controls.Add(this.statusLoggersBindingNavigator);
            this.Name = "StatistiekenForm";
            this.Text = "StatistiekenForm";
            this.Load += new System.EventHandler(this.StatistiekenForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.bestellingLogDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.statusLoggersBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.statusLoggersBindingNavigator)).EndInit();
            this.statusLoggersBindingNavigator.ResumeLayout(false);
            this.statusLoggersBindingNavigator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.statusLoggersDataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private BestellingLogDataSet bestellingLogDataSet;
        private System.Windows.Forms.BindingSource statusLoggersBindingSource;
        private BestellingLogDataSetTableAdapters.StatusLoggersTableAdapter statusLoggersTableAdapter;
        private BestellingLogDataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.BindingNavigator statusLoggersBindingNavigator;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveFirstItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMovePreviousItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveNextItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveLastItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator2;
        private System.Windows.Forms.DataGridView statusLoggersDataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.Label lblDagopbrengst;
        private System.Windows.Forms.Label lblBesteVerkoper;
        private System.Windows.Forms.Label lblPercentageUitstaandeKosten;
        private System.Windows.Forms.TextBox tbDagopbrengst;
        private System.Windows.Forms.TextBox tbBesteVerkoper;
        private System.Windows.Forms.TextBox tbPercentageUitstaandeKosten;

    }
}
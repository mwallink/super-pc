﻿namespace WfaSuperPC
{
    partial class AssemblageForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnClearEventLog = new System.Windows.Forms.Button();
            this.tbEventLog = new System.Windows.Forms.TextBox();
            this.lvBestelRegels = new System.Windows.Forms.ListView();
            this.colCheckBox = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colId = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colBestellingId = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colProductId = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colProductNaam = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lvBestellingenOnvoltooid = new System.Windows.Forms.ListView();
            this.bestelling_id = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.gecreeerd = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.countBestelRegels = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lblBestellingenOnvoltooid = new System.Windows.Forms.Label();
            this.colAantal = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colVoorraad = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.SuspendLayout();
            // 
            // btnClearEventLog
            // 
            this.btnClearEventLog.Location = new System.Drawing.Point(1207, 360);
            this.btnClearEventLog.Name = "btnClearEventLog";
            this.btnClearEventLog.Size = new System.Drawing.Size(215, 35);
            this.btnClearEventLog.TabIndex = 5;
            this.btnClearEventLog.Text = "Clear Event Log";
            this.btnClearEventLog.UseVisualStyleBackColor = true;
            this.btnClearEventLog.Click += new System.EventHandler(this.btnClearEventLog_Click);
            // 
            // tbEventLog
            // 
            this.tbEventLog.Location = new System.Drawing.Point(39, 414);
            this.tbEventLog.Multiline = true;
            this.tbEventLog.Name = "tbEventLog";
            this.tbEventLog.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tbEventLog.Size = new System.Drawing.Size(1383, 325);
            this.tbEventLog.TabIndex = 4;
            // 
            // lvBestelRegels
            // 
            this.lvBestelRegels.CheckBoxes = true;
            this.lvBestelRegels.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colCheckBox,
            this.colId,
            this.colBestellingId,
            this.colProductId,
            this.colProductNaam,
            this.colAantal,
            this.colVoorraad});
            this.lvBestelRegels.FullRowSelect = true;
            this.lvBestelRegels.GridLines = true;
            this.lvBestelRegels.HideSelection = false;
            this.lvBestelRegels.Location = new System.Drawing.Point(409, 163);
            this.lvBestelRegels.MultiSelect = false;
            this.lvBestelRegels.Name = "lvBestelRegels";
            this.lvBestelRegels.Size = new System.Drawing.Size(670, 232);
            this.lvBestelRegels.TabIndex = 3;
            this.lvBestelRegels.UseCompatibleStateImageBehavior = false;
            this.lvBestelRegels.View = System.Windows.Forms.View.Details;
            this.lvBestelRegels.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.lvBestelRegels_ItemCheck);
            this.lvBestelRegels.ItemSelectionChanged += new System.Windows.Forms.ListViewItemSelectionChangedEventHandler(this.lvBestelRegels_ItemSelectionChanged);
            this.lvBestelRegels.SelectedIndexChanged += new System.EventHandler(this.lvBestelRegels_SelectedIndexChanged);
            this.lvBestelRegels.KeyDown += new System.Windows.Forms.KeyEventHandler(this.lvBestelRegels_KeyDown);
            this.lvBestelRegels.MouseClick += new System.Windows.Forms.MouseEventHandler(this.lvBestelRegels_MouseClick);
            // 
            // colCheckBox
            // 
            this.colCheckBox.Text = "Voltooid";
            this.colCheckBox.Width = 52;
            // 
            // colId
            // 
            this.colId.Text = "id";
            this.colId.Width = 52;
            // 
            // colBestellingId
            // 
            this.colBestellingId.Text = "bestelling id";
            this.colBestellingId.Width = 94;
            // 
            // colProductId
            // 
            this.colProductId.Text = "product id";
            this.colProductId.Width = 90;
            // 
            // colProductNaam
            // 
            this.colProductNaam.Text = "productnaam";
            this.colProductNaam.Width = 188;
            // 
            // lvBestellingenOnvoltooid
            // 
            this.lvBestellingenOnvoltooid.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.bestelling_id,
            this.gecreeerd,
            this.countBestelRegels});
            this.lvBestellingenOnvoltooid.FullRowSelect = true;
            this.lvBestellingenOnvoltooid.GridLines = true;
            this.lvBestellingenOnvoltooid.HideSelection = false;
            this.lvBestellingenOnvoltooid.LabelEdit = true;
            this.lvBestellingenOnvoltooid.Location = new System.Drawing.Point(39, 163);
            this.lvBestellingenOnvoltooid.MultiSelect = false;
            this.lvBestellingenOnvoltooid.Name = "lvBestellingenOnvoltooid";
            this.lvBestellingenOnvoltooid.Size = new System.Drawing.Size(364, 232);
            this.lvBestellingenOnvoltooid.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.lvBestellingenOnvoltooid.TabIndex = 2;
            this.lvBestellingenOnvoltooid.UseCompatibleStateImageBehavior = false;
            this.lvBestellingenOnvoltooid.View = System.Windows.Forms.View.Details;
            this.lvBestellingenOnvoltooid.SelectedIndexChanged += new System.EventHandler(this.lvBestellingenOnvoltooid_SelectedIndexChanged);
            // 
            // bestelling_id
            // 
            this.bestelling_id.Text = "id";
            // 
            // gecreeerd
            // 
            this.gecreeerd.Text = "gecreeerd";
            this.gecreeerd.Width = 200;
            // 
            // countBestelRegels
            // 
            this.countBestelRegels.Text = "# bestelregels";
            this.countBestelRegels.Width = 100;
            // 
            // lblBestellingenOnvoltooid
            // 
            this.lblBestellingenOnvoltooid.AutoSize = true;
            this.lblBestellingenOnvoltooid.Location = new System.Drawing.Point(36, 134);
            this.lblBestellingenOnvoltooid.Name = "lblBestellingenOnvoltooid";
            this.lblBestellingenOnvoltooid.Size = new System.Drawing.Size(165, 17);
            this.lblBestellingenOnvoltooid.TabIndex = 1;
            this.lblBestellingenOnvoltooid.Text = "Onvoltooide Bestellingen";
            // 
            // colAantal
            // 
            this.colAantal.Text = "aantal";
            this.colAantal.Width = 93;
            // 
            // colVoorraad
            // 
            this.colVoorraad.Text = "voorraad";
            this.colVoorraad.Width = 90;
            // 
            // AssemblageForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1487, 790);
            this.Controls.Add(this.btnClearEventLog);
            this.Controls.Add(this.tbEventLog);
            this.Controls.Add(this.lvBestelRegels);
            this.Controls.Add(this.lvBestellingenOnvoltooid);
            this.Controls.Add(this.lblBestellingenOnvoltooid);
            this.Name = "AssemblageForm";
            this.Text = "AssemblageForm";
            this.Deactivate += new System.EventHandler(this.frmAssemblage_Deactivate);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmAssemblage_FormClosing);
            this.Load += new System.EventHandler(this.AssemblageForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblBestellingenOnvoltooid;
        private System.Windows.Forms.ColumnHeader bestelling_id;
        private System.Windows.Forms.ColumnHeader gecreeerd;
        private System.Windows.Forms.ColumnHeader countBestelRegels;
        private System.Windows.Forms.ListView lvBestellingenOnvoltooid;
        private System.Windows.Forms.ListView lvBestelRegels;
        private System.Windows.Forms.ColumnHeader colId;
        private System.Windows.Forms.ColumnHeader colBestellingId;
        private System.Windows.Forms.ColumnHeader colProductId;
        private System.Windows.Forms.ColumnHeader colProductNaam;
        private System.Windows.Forms.ColumnHeader colCheckBox;
        private System.Windows.Forms.TextBox tbEventLog;
        private System.Windows.Forms.Button btnClearEventLog;
        private System.Windows.Forms.ColumnHeader colAantal;
        private System.Windows.Forms.ColumnHeader colVoorraad;




    }
}
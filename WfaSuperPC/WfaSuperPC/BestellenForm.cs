﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace WfaSuperPC
{
    public partial class BestellenForm : AuthorizedForm
    {
        public  Klanten klant = null;
        public  Personen ingelogdePersoon = null;
        public  Bestellingen bestelling;

        public List<ProductenTemp> producten = new List<ProductenTemp>();
        public  Int32 korting { get; set; }

        private ClientsForm clientsForm;
        private ProductenForm prodForm;
        private double Som = 0;
        private BestellingenMachine bestellingenMachine = new BestellingenMachine();

        public SuperPcContainer context = new SuperPcContainer();

        // Constructor.
        public BestellenForm()
        {
            InitializeComponent();
            DrawTable();
        }

        // nieuwe bestelling.
        public BestellenForm(int persoonid)
        {
            ingelogdePersoon = context.Personen.Find(persoonid);
            
            
            InitializeComponent();
            init();
            DrawTable();
        }

        // Bestaande bestelling.
        public BestellenForm(int persoonid, int bestellingId)
        {
            InitializeComponent();

            ingelogdePersoon = context.Personen.Find(persoonid);
            bestelling = context.Bestellingen.Find(bestellingId);

            bestelling.FkVerkoperId = persoonid;

            // Klant inladen
            AddCustomer(bestelling.KlantenId);

            // producten inladen
            foreach (var bs in bestelling.Bestelregels)
            {
                Boolean editable = false;
                if (bestelling.Statussen.Naam == "klantAfkeuring" || bestelling.Statussen.Naam == "managerAfkeuring")
                {
                    editable = true;
                }

                ProductenTemp product = new ProductenTemp(this, bs.ProductenId, bs.Aantal, false, editable);
                producten.Add(product);
            }



            // Als Zakelijke klant, dan knop laten zien.
            if (this.bestelling.Klanten.ZakelijkKlant)
                this.offerteCB.Visible = true;

            double openstaandBedrag = Som - bestelling.Aanbetaling;
            label3.Text = "Openstaand bedrag: €" + openstaandBedrag.ToString();


            init();
            DrawTable();
        }

        // Initiele methode die aangeroepen wordt door beide constructors.
        public void init()
        {
            // standaard dingen verbergen die alleen voor bepaalde personen / bestelstatussen zichtbaar zijn.
            this.createOrderBtn.Visible = false;
            this.disapproveOrderBtn.Visible = false;
            this.approveOrderBtn.Visible = false;
            this.weigerOfferteBtn.Visible = false;
            this.placeOrderBtn.Visible = false;
            this.offerteCB.Visible = false;
            this.offerteCB.Enabled = false;

            this.addCustomerBtn.Visible = false;
            this.addProductBtn.Visible = false;
            this.discountBtn.Enabled = false;

            this.payBtn.Visible = false;
            this.payTb.Visible = false;
            this.label3.Visible = false;


            if(bestelling==null)
                this.offerteCB.Enabled = true;


            // Knoppen zichtbaar maken bij juiste rechten/ statussen.
            foreach (var rol in ingelogdePersoon.Rollen)
            {
                foreach (var permissie in rol.Permissies)
                {
                    if (bestelling == null || bestelling.Statussen.Naam == "klantAfgekeuring" || bestelling.Statussen.Naam == "managerAfkeuring")
                    {
                        // Er is nog geen bestelling. Als de rechten er voor zijn, dan kan de bestelling aangemaakt worden
                        // Of er is wel een bestelling/offerte, maar deze is afgekeurd door klant.
                        // Of er is wel een bestelling/offerte, maar deze is afgekeurd door manager.
                        // In de laatste twee gevallen moet de bestelling opnieuw aangemaakt worden.
                        if (permissie.Control.Name == "createOrderBtn")
                            this.createOrderBtn.Visible = true;

                        if (permissie.Control.Name == "addCustomerBtn")
                            this.addCustomerBtn.Visible = true;

                        if (permissie.Control.Name == "addProductBtn")
                            this.addProductBtn.Visible = true;

                        if (permissie.Control.Name == "discountBtn")
                            this.discountBtn.Enabled = true;

                    }
                    else
                    {
                        // De klant krijgt een offerte. Deze moet eerst goedgekeurd worden door de manager.
                        if (permissie.Control.Name == "disapproveOrderBtn" && bestelling.Statussen.Naam == "aangemaakt")
                            this.disapproveOrderBtn.Visible = true;
                        // De klant krijgt een offerte. Deze moet eerst goedgekeurd worden door de manager.
                        if (permissie.Control.Name == "approveOrderBtn" && bestelling.Statussen.Naam == "aangemaakt")
                            this.approveOrderBtn.Visible = true;

                        // Manager heeft goedkeuring gegeven, dus de klant kan de bestelling/offerte afkeuren.
                        if (permissie.Control.Name == "weigerOfferteBtn" &&
                            bestelling.Statussen.Naam == "managerGoedkeuring")
                        {
                            this.weigerOfferteBtn.Visible = true;
                        }

                        // Manager heeft goedkeuring gegeven, dus als de klant dat ook doet mag de bestelling geplaatst worden.
                        if (permissie.Control.Name == "placeOrderBtn" &&
                            bestelling.Statussen.Naam == "managerGoedkeuring")
                        {
                            this.placeOrderBtn.Visible = true;
                        }

                        if (permissie.Control.Name == "payBtn" &&
                            bestelling.Statussen.Naam == "klantGoedkeuring")
                        {
                            this.payBtn.Visible = true;
                            this.payTb.Visible = true;
                            this.label3.Visible = true;
                        }
                    }
                }
            }

            if (bestelling == null)
            {
                // We gaan de bestelling aanmaken.
                bestelling = context.Bestellingen.Create();
            }
        }

        // Prijs wordt berekend en labels met prijzen wordne veranderd.
        public void MaakFactuur()
        {
            double Som = 0;
            foreach (ProductenTemp product in producten)
            {
                Som += product.Prijs * product.Aantal;
            }

            // Totaal van producten berekenen.
            productTotalLabel.Text = "€" + Som.ToString();

            // Korting berekenen en eraf halen.
            var totaalKorting = Som - (Som / 100 * Convert.ToInt32(discountBtn.Text));
            productTotalDiscountLabel.Text = "€" + totaalKorting.ToString();

            this.korting = (short)Convert.ToInt32(discountBtn.Text);
            bestelling.Korting = (short) Convert.ToInt32(discountBtn.Text);

            // Btw berekenen en er bij opdoen.
            var totaalBtw = totaalKorting + (totaalKorting / 100 * 21);
            productTotalPriceLabel.Text = "€" + totaalBtw.ToString();

            this.Som = Som;
        }

        // Klanten form openen.
        private void addCustomerBtn_Click(object sender, EventArgs e)
        {
            clientsForm = new ClientsForm(this);
            this.clientsForm.ShowDialog(this);
        }

        // Toevoegen van een klant.
        public Klanten AddCustomer(int klantId)
        {
            klant = context.Klanten.Find(klantId);
            bestelling.KlantenId = klantId;

            this.customerLabel.Text = klant.Voornaam + " " + klant.Achternaam;

            if (klant.ZakelijkKlant)
            {
                this.offerteCB.Visible = true;
            }

            return klant;
        }

        

        // Toevoegen van een product
        private void addProductBtn_Click(object sender, EventArgs e)
        {
            prodForm = new ProductenForm(this);
            this.prodForm.ShowDialog();

            // Tabel tekenen.
            DrawTable();

            // Totaal berekenen.
            MaakFactuur();
        }

        // Product toevoegen aan productenlijst.
        public ProductenTemp AddProduct(int Id)
        {
            ProductenTemp product = new ProductenTemp(this, Id, 1, false);
            producten.Add(product);
            return product;
        }

        public void DrawTable()
        {
            // Table leeg maken.
            productTable.Controls.Clear();
            productTable.RowStyles.Clear();

            // Header tekenen.
            AddProductRow("Naam", "Prijs", "Aantal", "Totaalprijs", "Verwijderen");

            // Tabel invullen.
            foreach (ProductenTemp p in producten)
            {
                if (p.Deleted == false)
                {
                    this.AddProductRow(p.Naam, p.Prijs.ToString(), p.AantalNum, (p.Totaal * p.Aantal).ToString(), p.Delete);
                }
            }
        }

        // Een nieuwe rij aan de tabel toevoegen en de kolommen vullen met teksten.
        private int AddProductRow(String naam, String prijs, String totaal)
        {
            Label naamLabel = new Label();
            Label prijsLabel = new Label();
            Label totaalLabel = new Label();

            naamLabel.Text = naam;
            prijsLabel.Text = prijs;
            totaalLabel.Text = totaal;

            int rowIndex = AddProductTableRow();
            this.productTable.Controls.Add(naamLabel, 0, rowIndex);
            this.productTable.Controls.Add(prijsLabel, 1, rowIndex);
            this.productTable.Controls.Add(totaalLabel, 3, rowIndex);

            // Geef de index van de laatste rij terug.
            return rowIndex;
        }

        // Roep bovenstaande functie en voeg twee waarden extra toe (voor table header)
        private void AddProductRow(String naam, String prijs, String aantal, String totaal, String acties)
        {
            Label actieLabel = new Label();
            Label aantalLabel = new Label();
            actieLabel.Text = acties;
            aantalLabel.Text = aantal;

            int last = AddProductRow(naam, prijs, totaal);

            this.productTable.Controls.Add(actieLabel, 4, last);
            this.productTable.Controls.Add(aantalLabel, 2, last);
        }

        // Roep bovenstaande functie en voeg een button en een teller extra toe
        private void AddProductRow(String naam, String prijs, NumericUpDown aantal, String totaal, Button verwijder)
        {
            int last = AddProductRow(naam, prijs, totaal);
            this.productTable.Controls.Add(aantal, 2, last);
            this.productTable.Controls.Add(verwijder, 4, last);
        }

        // Een nieuwe rij maken voor de tabel
        private int AddProductTableRow()
        {
            int index = productTable.RowCount++;
            RowStyle style = new RowStyle(SizeType.AutoSize);
            productTable.RowStyles.Add(style);
            return index;
        }

        // Als de korting wordt aangepast, moet alles opnieuw berekend worden.
        private void discountBtn_ValueChanged(object sender, EventArgs e)
        {
            this.MaakFactuur();
        }

        private void BestellenForm_Load(object sender, EventArgs e)
        {

        }

        private void createOrderBtn_Click(object sender, EventArgs e)
        {
            // Bestelling maken
            if (producten.Count > 0 && klant != null)
            {

                bestelling.FkVerkoperId = ingelogdePersoon.Id;
                bestellingenMachine.maakBestelling(klant, producten, bestelling, ingelogdePersoon, context);
                MessageBox.Show("De bestelling is aangemaakt.");
                this.Close();
            }
            else
            {
                MessageBox.Show("U dient een klant en minimaal één product toe te voegen als u een bestelling wilt maken.");
            }
        }

        private void offerteCB_CheckedChanged(object sender, EventArgs e)
        {
            bestelling.HeeftOfferte = offerteCB.Checked;
        }

        private void approveOrderBtn_Click(object sender, EventArgs e)
        {
            bestellingenMachine.managerKeurGoed(this.bestelling);
            MessageBox.Show("De offerte is goedgekeurd door de manager.");
            this.Close();
        }

        private void disapproveOrderBtn_Click(object sender, EventArgs e)
        {
            bestellingenMachine.managerKeurAf(this.bestelling);
            MessageBox.Show("De offerte is afgekeurd door de manager.");
            this.Close();
        }

        private void placeOrderBtn_Click(object sender, EventArgs e)
        {
            bestellingenMachine.klantKeurGoed(this.bestelling);
            MessageBox.Show("De bestelling is geplaatst.");
            this.Close();
        }

        private void weigerOfferteBtn_Click(object sender, EventArgs e)
        {
            bestellingenMachine.klantKeurAf(this.bestelling);
            MessageBox.Show("De offerte is door de klant geweigerd. Pas deze eventueel aan.");
            this.Close();
        }

        private void payBtn_Click(object sender, EventArgs e)
        {
            Double payment = Convert.ToDouble(payTb.Text);
            bestelling.Aanbetaling += payment;
            double openstaandBedrag = Som - bestelling.Aanbetaling;
            label3.Text = "Openstaand bedrag: €" + openstaandBedrag.ToString();
        }
    }

    public class ProductenTemp
    {
        public int Id { get; set; }
        public String Naam { get; set; }
        public double Prijs { get; set; }
        public int Aantal { get; set; }
        public double Totaal { get; set; }
        public Button Delete = new Button();
        public NumericUpDown AantalNum = new NumericUpDown();
        public Boolean Deleted { get; set; }

        private BestellenForm bf;

        // Voor een bestaande regel
        public ProductenTemp(BestellenForm bf, int ProductId, int aantal, Boolean deleted)
        {
            Delete.Click += new EventHandler(this.deleteButton_Click);
            AantalNum.ValueChanged += new EventHandler(this.aantalNum_ValueChanged);
            this.bf = bf;
        
            SuperPcContainer context = new SuperPcContainer();
            Producten product = context.Producten.Find(ProductId);

            this.Id = product.Id;
            this.Deleted = deleted;
            Delete.Text = "X";
            this.Naam = product.Naam;
            this.Prijs = product.Prijs;
            this.Aantal = aantal;
            this.AantalNum.Text = Aantal.ToString();
            this.Totaal = this.Prijs * this.Aantal;
        }

        // Voor een bestaande regel
        public ProductenTemp(BestellenForm bf, int ProductId, int aantal, Boolean deleted, Boolean editable)
        {
            Delete.Click += new EventHandler(this.deleteButton_Click);
            AantalNum.ValueChanged += new EventHandler(this.aantalNum_ValueChanged);
            this.bf = bf;
            this.Delete.Enabled = editable;
            this.AantalNum.Enabled = editable;
            

            SuperPcContainer context = new SuperPcContainer();
            Producten product = context.Producten.Find(ProductId);

            this.Id = product.Id;
            this.Deleted = deleted;
            Delete.Text = "X";
            this.Naam = product.Naam;
            this.Prijs = product.Prijs;
            this.Aantal = aantal;
            this.AantalNum.Text = Aantal.ToString();
            this.Totaal = this.Prijs * this.Aantal;
        }


        void deleteButton_Click(object sender, System.EventArgs e)
        {
            this.Deleted = true;
            bf.MaakFactuur();
            bf.DrawTable();
        }

        void aantalNum_ValueChanged(object sender, EventArgs e)
        {
            this.Aantal = Convert.ToInt32(AantalNum.Text);

            bf.MaakFactuur();
            bf.DrawTable();
        }

    }
}
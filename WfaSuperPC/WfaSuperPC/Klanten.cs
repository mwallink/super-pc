//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WfaSuperPC
{
    using System;
    using System.Collections.Generic;
    
    public partial class Klanten
    {
        public Klanten()
        {
            this.Bedrijfsnaam = "\"  \"";
            this.Land = "Nederland";
            this.Straatnaam = "\" \"";
            this.Huisnummer = 0;
            this.Huisnummertoevoeging = "\" \"";
            this.Postcode = "\" \"";
            this.ZakelijkKlant = false;
            this.Bestellingen = new HashSet<Bestellingen>();
        }
    
        public string Bedrijfsnaam { get; set; }
        public string Land { get; set; }
        public string Straatnaam { get; set; }
        public Nullable<int> Huisnummer { get; set; }
        public string Huisnummertoevoeging { get; set; }
        public string Postcode { get; set; }
        public string Emailadres { get; set; }
        public string Telefoonnummer { get; set; }
        public string Gsmnummer { get; set; }
        public int Id { get; set; }
        public string Voornaam { get; set; }
        public string Achternaam { get; set; }
        public bool ZakelijkKlant { get; set; }
    
        public virtual ICollection<Bestellingen> Bestellingen { get; set; }
    }
}

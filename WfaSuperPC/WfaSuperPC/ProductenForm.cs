﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WfaSuperPC
{
    public partial class ProductenForm : Form
    {
        private BestellenForm bs;
        private bool _bestellenForm = false;

        public ProductenForm()
        {
            InitializeComponent();
        }
        public ProductenForm(BestellenForm bs)
        {
            this.bs = bs;
            this._bestellenForm = true;
            InitializeComponent();
        }

        private void ToevoegenBtn_Click(object sender, EventArgs e)
        {
            ProductForm pf = new ProductForm();
            pf.ShowDialog();
        }

        private void ProductenForm_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'productenDataSet.Producten' table. You can move, or remove it, as needed.
            this.productenTableAdapter.Fill(this.productenDataSet.Producten);

        }


        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            // Als dit form vanuit bestellenform geopend is.
            if (_bestellenForm)
            {

                int rowIndex = e.RowIndex;
                int productId = Convert.ToInt32(dataGridView1.Rows[rowIndex].Cells[0].Value);


                ProductenTemp toegevoegdProduct = bs.AddProduct(productId);

                MessageBox.Show(toegevoegdProduct.Naam + " met ID: " + toegevoegdProduct.Id.ToString() +
                                " is toegevoegd.");
            }
            else
            {
                int rowIndex = e.RowIndex;
                int productId = Convert.ToInt32(dataGridView1.Rows[rowIndex].Cells[0].Value);

                ProductForm pf = new ProductForm(productId);
                pf.ShowDialog();
                dataGridView1.Refresh();
            }
        }

    }
}

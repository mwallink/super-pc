﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WfaSuperPC
{
    public partial class AuthorizedForm : Form
    {
        
        protected int PersoonId{get; set;} //geset via constructor
        Type childFormType; //nodig om controls op te halen
        SuperPcContainer context = new SuperPcContainer(); 
        //List<FormControls> formcontrols;

        public AuthorizedForm()
        {
            InitializeComponent();
            this.childFormType = this.GetType();
            this.PersoonId = -1; //default waarde, event load doet een check of de waarde is geset
        }

        
        
        //public List<FormControls> FormControls{
          //  get{
            //    return (from fc in context.FormControls where fc.Name == this.childFormType.ToString() select fc).ToList();
            //}
            //private set { throw new NotImplementedException(); }
        //}



        public Rollen CurrentRole
        {
            get
            {
                var query  =  (Rollen)(from r in context.Rollen 
                        where r.Personen.Any(persoon => this.PersoonId == persoon.Id) select r).Single();
                return query; 
            }
            private set { throw new NotImplementedException(); }
        }



        private void AuthorizedForm_Load(object sender, EventArgs e)
        {
                if(this.PersoonId != -1)
                HandlePermissionOnControl();

                if (this.PersoonId == -1)
                     new System.ArgumentException("Property PersoonId heeft waarde -1. " + 
                    "Zet in de subklasse van Authorizedform een waarde die verkregen is. " +
                    "Bijvoorbeeld vanuit het inlogform", "PersoonId");
        }



        private void HandlePermissionOnControl()
        {
            //get controls
            foreach (object obj in RecurseObjects(this))
            {
                //get objectname
                if(obj  is AuthorizedForm)
                    continue;

                //get objectname
                string name;
                if (obj.GetType() == typeof(ToolStrip))
                {
                    ToolStrip t = (ToolStrip)obj;
                    name = t.Name;
                }
                else if (obj.GetType() == typeof(Control))
                {
                    Control ctl = (Control)obj;
                    name = ctl.Name;
                } else if (obj.GetType() == typeof(ToolStripItem))
                {
                    ToolStripItem ti = (ToolStripItem)obj;
                    name = ti.Name;
                }
                else
                {
                    throw new NotSupportedException("Geen waard voor object op form gevonden");
                    //name = " ";
                }

                
                //get permissie
                Permissies permissie = getPermissie(name);
                
                //set permission on control
                //set editability
                if (permissie.Editable == 0)
                {
                    if (obj.GetType() == typeof(ToolStrip))
                    {
                        ToolStrip t = (ToolStrip)obj;
                        t.Enabled = false;
                    }
                    else if (obj.GetType() == typeof(ToolStripItem))
                    {
                        ToolStripItem ti = (ToolStripItem)(object)obj;
                        ti.Enabled = false;

                    }
                    else if (obj.GetType() == typeof(Control))
                    {
                        Control c = (Control)(object)obj;
                        c.Enabled = false;
                    }
                    else
                    {
                        throw new NotSupportedException("Objecttype in HandlePermissionOnControl niet ondersteund.");
                    }
                }

                //set visibility
                if (permissie.Visible == 0)
                {
                    if (obj.GetType() == typeof(ToolStrip))
                    {
                        ToolStrip t = (ToolStrip)obj;
                        t.Visible = false;
                    }
                    else if (obj.GetType() == typeof(ToolStripItem))
                    {
                        ToolStripItem ti = (ToolStripItem)(object)obj;
                        ti.Visible = false;

                    }
                    else if (obj.GetType() == typeof(Control))
                    {
                        Control c = (Control)(object)obj;
                        c.Visible = false;
                    }
                    else
                    {
                        throw new NotSupportedException("Objecttype in HandlePermissionOnControl niet ondersteund.");
                    }
                }
            }
        }

        private Permissies getPermissie(string ControlNameOnForm)
        {
            //find formcontrol in database
            //FormControls formcontrol = this.FormControls.Find(fc => fc.Name == ControlNameOnForm);
            FormControls formcontrol = (FormControls)(from fc in context.FormControls where fc.Name == ControlNameOnForm select fc).Single();
            //Permissies permissie = (Permissies)(formcontrol.Permissies.Any(perm => perm.Id== this.CurrentRole.Id));
            return (Permissies)formcontrol.Permissies.Where(p => p.RollenId == this.CurrentRole.Id).Single();
        }

        private IEnumerable<object> RecurseObjects(object root)
        //BRON : http://stackoverflow.com/questions/1852064/form-to-object-and-loop-through-objects-in-c
        {
            Queue<object> items = new Queue<object>();
            items.Enqueue(root);
            while (items.Count > 0)
            {
                object obj = items.Dequeue();
                yield return obj;
                Control control = obj as Control;
                if (control != null)
                {
                    // regular controls and sub-controls
                    foreach (Control item in control.Controls)
                    {
                        items.Enqueue(item);
                    }
                    // top-level menu items
                    ToolStrip ts = control as ToolStrip;
                    if (ts != null)
                    {
                        foreach (ToolStripItem tsi in ts.Items)
                        {
                            items.Enqueue(tsi);
                        }
                    }
                }
                // child menus
                ToolStripDropDownItem tsddi = obj as ToolStripDropDownItem;
                if (tsddi != null && tsddi.HasDropDownItems)
                {
                    foreach (ToolStripItem item in tsddi.DropDownItems)
                    {
                        items.Enqueue(item);
                    }
                }
            }
        }
    }
}

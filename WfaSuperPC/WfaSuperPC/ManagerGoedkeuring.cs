﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WfaSuperPC
{
    class ManagerGoedkeuring : BestellingenState
    {
        BestellingenMachine bestellingenMachine;
        SuperPcContainer context = new SuperPcContainer();

        public ManagerGoedkeuring(BestellingenMachine bm)
        {
            this.bestellingenMachine = bm;
        }


        // Wordt initieel aangeroepen
        public void maakBestelling(Klanten klant, List<ProductenTemp> productenLijst, Bestellingen bestelling, Personen ingelogdePersoon, SuperPcContainer context)
        {

        }

        public void managerKeurGoed(Bestellingen bestelling)
        {
            /*
             * aangemaakt
             * managerGoedkeuring
             * managerAfkeuring
             * klantGoedkeuring
             * klantAfkeuring
             * geassembleerd
             */
            
            bestelling = context.Bestellingen.Find(bestelling.Id);

            var statussen =
                from status in context.Statussen
                where status.Naam == "managerGoedkeuring"
                select new
                {
                    statusId = status.Id
                };

            foreach (var status in statussen)
            {
                bestelling.StatussenId = status.statusId;
            }

            context.SaveChanges();
        }

        public void managerKeurAf(Bestellingen bestelling)
        {

        }

        public void klantKeurGoed(Bestellingen bestelling)
        {

        }

        public void klantKeurAf(Bestellingen bestelling)
        {

        }

        public void assembleren(Bestellingen bestelling)
        {

        }
       
    }
}

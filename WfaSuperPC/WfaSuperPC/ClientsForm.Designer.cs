﻿namespace WfaSuperPC
{
    partial class ClientsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.addBtn = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.klantenBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.klantenDataSet = new WfaSuperPC.KlantenDataSet();
            this.klantenTableAdapter = new WfaSuperPC.KlantenDataSetTableAdapters.KlantenTableAdapter();
            this.idDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bedrijfsnaamDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.landDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.straatnaamDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.huisnummerDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.huisnummertoevoegingDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.postcodeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.emailadresDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.telefoonnummerDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gsmnummerDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.voornaamDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.achternaamDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.klantenBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.klantenDataSet)).BeginInit();
            this.SuspendLayout();
            // 
            // addBtn
            // 
            this.addBtn.Location = new System.Drawing.Point(666, 374);
            this.addBtn.Name = "addBtn";
            this.addBtn.Size = new System.Drawing.Size(75, 23);
            this.addBtn.TabIndex = 0;
            this.addBtn.Text = "&add";
            this.addBtn.UseVisualStyleBackColor = true;
            this.addBtn.Click += new System.EventHandler(this.addBtn_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idDataGridViewTextBoxColumn,
            this.bedrijfsnaamDataGridViewTextBoxColumn,
            this.landDataGridViewTextBoxColumn,
            this.straatnaamDataGridViewTextBoxColumn,
            this.huisnummerDataGridViewTextBoxColumn,
            this.huisnummertoevoegingDataGridViewTextBoxColumn,
            this.postcodeDataGridViewTextBoxColumn,
            this.emailadresDataGridViewTextBoxColumn,
            this.telefoonnummerDataGridViewTextBoxColumn,
            this.gsmnummerDataGridViewTextBoxColumn,
            this.voornaamDataGridViewTextBoxColumn,
            this.achternaamDataGridViewTextBoxColumn});
            this.dataGridView1.DataSource = this.klantenBindingSource;
            this.dataGridView1.Location = new System.Drawing.Point(9, 13);
            this.dataGridView1.Margin = new System.Windows.Forms.Padding(2);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowTemplate.Height = 24;
            this.dataGridView1.Size = new System.Drawing.Size(733, 355);
            this.dataGridView1.TabIndex = 1;
            this.dataGridView1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellClick);
            // 
            // klantenBindingSource
            // 
            this.klantenBindingSource.DataMember = "Klanten";
            this.klantenBindingSource.DataSource = this.klantenDataSet;
            // 
            // klantenDataSet
            // 
            this.klantenDataSet.DataSetName = "KlantenDataSet";
            this.klantenDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // klantenTableAdapter
            // 
            this.klantenTableAdapter.ClearBeforeFill = true;
            // 
            // idDataGridViewTextBoxColumn
            // 
            this.idDataGridViewTextBoxColumn.DataPropertyName = "Id";
            this.idDataGridViewTextBoxColumn.HeaderText = "Id";
            this.idDataGridViewTextBoxColumn.Name = "idDataGridViewTextBoxColumn";
            this.idDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // bedrijfsnaamDataGridViewTextBoxColumn
            // 
            this.bedrijfsnaamDataGridViewTextBoxColumn.DataPropertyName = "Bedrijfsnaam";
            this.bedrijfsnaamDataGridViewTextBoxColumn.HeaderText = "Bedrijfsnaam";
            this.bedrijfsnaamDataGridViewTextBoxColumn.Name = "bedrijfsnaamDataGridViewTextBoxColumn";
            // 
            // landDataGridViewTextBoxColumn
            // 
            this.landDataGridViewTextBoxColumn.DataPropertyName = "Land";
            this.landDataGridViewTextBoxColumn.HeaderText = "Land";
            this.landDataGridViewTextBoxColumn.Name = "landDataGridViewTextBoxColumn";
            // 
            // straatnaamDataGridViewTextBoxColumn
            // 
            this.straatnaamDataGridViewTextBoxColumn.DataPropertyName = "Straatnaam";
            this.straatnaamDataGridViewTextBoxColumn.HeaderText = "Straatnaam";
            this.straatnaamDataGridViewTextBoxColumn.Name = "straatnaamDataGridViewTextBoxColumn";
            // 
            // huisnummerDataGridViewTextBoxColumn
            // 
            this.huisnummerDataGridViewTextBoxColumn.DataPropertyName = "Huisnummer";
            this.huisnummerDataGridViewTextBoxColumn.HeaderText = "Huisnummer";
            this.huisnummerDataGridViewTextBoxColumn.Name = "huisnummerDataGridViewTextBoxColumn";
            // 
            // huisnummertoevoegingDataGridViewTextBoxColumn
            // 
            this.huisnummertoevoegingDataGridViewTextBoxColumn.DataPropertyName = "Huisnummertoevoeging";
            this.huisnummertoevoegingDataGridViewTextBoxColumn.HeaderText = "Huisnummertoevoeging";
            this.huisnummertoevoegingDataGridViewTextBoxColumn.Name = "huisnummertoevoegingDataGridViewTextBoxColumn";
            // 
            // postcodeDataGridViewTextBoxColumn
            // 
            this.postcodeDataGridViewTextBoxColumn.DataPropertyName = "Postcode";
            this.postcodeDataGridViewTextBoxColumn.HeaderText = "Postcode";
            this.postcodeDataGridViewTextBoxColumn.Name = "postcodeDataGridViewTextBoxColumn";
            // 
            // emailadresDataGridViewTextBoxColumn
            // 
            this.emailadresDataGridViewTextBoxColumn.DataPropertyName = "Emailadres";
            this.emailadresDataGridViewTextBoxColumn.HeaderText = "Emailadres";
            this.emailadresDataGridViewTextBoxColumn.Name = "emailadresDataGridViewTextBoxColumn";
            // 
            // telefoonnummerDataGridViewTextBoxColumn
            // 
            this.telefoonnummerDataGridViewTextBoxColumn.DataPropertyName = "Telefoonnummer";
            this.telefoonnummerDataGridViewTextBoxColumn.HeaderText = "Telefoonnummer";
            this.telefoonnummerDataGridViewTextBoxColumn.Name = "telefoonnummerDataGridViewTextBoxColumn";
            // 
            // gsmnummerDataGridViewTextBoxColumn
            // 
            this.gsmnummerDataGridViewTextBoxColumn.DataPropertyName = "Gsmnummer";
            this.gsmnummerDataGridViewTextBoxColumn.HeaderText = "Gsmnummer";
            this.gsmnummerDataGridViewTextBoxColumn.Name = "gsmnummerDataGridViewTextBoxColumn";
            // 
            // voornaamDataGridViewTextBoxColumn
            // 
            this.voornaamDataGridViewTextBoxColumn.DataPropertyName = "Voornaam";
            this.voornaamDataGridViewTextBoxColumn.HeaderText = "Voornaam";
            this.voornaamDataGridViewTextBoxColumn.Name = "voornaamDataGridViewTextBoxColumn";
            // 
            // achternaamDataGridViewTextBoxColumn
            // 
            this.achternaamDataGridViewTextBoxColumn.DataPropertyName = "Achternaam";
            this.achternaamDataGridViewTextBoxColumn.HeaderText = "Achternaam";
            this.achternaamDataGridViewTextBoxColumn.Name = "achternaamDataGridViewTextBoxColumn";
            // 
            // ClientsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(749, 407);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.addBtn);
            this.Name = "ClientsForm";
            this.Text = "ClientsForm";
            this.Load += new System.EventHandler(this.clientsForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.klantenBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.klantenDataSet)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button addBtn;
        private System.Windows.Forms.DataGridView dataGridView1;
        private KlantenDataSet klantenDataSet;
        private System.Windows.Forms.BindingSource klantenBindingSource;
        private KlantenDataSetTableAdapters.KlantenTableAdapter klantenTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn bedrijfsnaamDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn landDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn straatnaamDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn huisnummerDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn huisnummertoevoegingDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn postcodeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn emailadresDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn telefoonnummerDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn gsmnummerDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn voornaamDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn achternaamDataGridViewTextBoxColumn;


    }
}
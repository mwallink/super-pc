﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WfaSuperPC
{
    public partial class RollenBeheerForm : Form
    {
        public RollenBeheerForm()
        {
            InitializeComponent();
        }

        private void rollenBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.rollenBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.personenRollenDataSet);

            SuperPcContainer context = new SuperPcContainer();

            //get last insertid - role id
            var lastinsertid = (from lastid in context.Rollen select lastid.Id).Max(); 

            //get all formcontrols
            var queryControlList = (from control in context.FormControls
                                    select control);

            //add foreach form control permission
            foreach (var c in queryControlList)
            {
                //add permissies
                Permissies p = new Permissies();
                p.ControlsId = c.Id;
                p.RollenId = lastinsertid;
                context.Permissies.Add(p);
            }
            context.SaveChanges();

            this.rollenDataGridView.EndEdit();
            this.viewPermissiesTableAdapter.Fill(this.viewPermissiesDataSet.ViewPermissies);
            string cellvalue = rollenDataGridView.CurrentRow.Cells[0].Value.ToString();
            viewPermissiesBindingSource.Filter = "rol_id = " + cellvalue;
            this.permissiesDataGridView.Refresh();
        }

        private void RollenBeheerForm_Load(object sender, EventArgs e)
        {
            this.viewPermissiesTableAdapter.Fill(this.viewPermissiesDataSet.ViewPermissies);
            this.rollenTableAdapter.Fill(this.personenRollenDataSet.Rollen);
        }

        private void rollenDataGridView_SelectionChanged(object sender, EventArgs e)
        {
            string cellvalue = rollenDataGridView.CurrentRow.Cells[0].Value.ToString();
            viewPermissiesBindingSource.Filter = "rol_id = " + cellvalue;
        }

        private void permissiesDataGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            SuperPcContainer context = new SuperPcContainer();

            //get permissionid
            int permissionid = Convert.ToInt32(this.permissiesDataGridView.Rows[e.RowIndex].Cells[6].Value.ToString());

            //get permission
            Permissies permissie = (Permissies)(from p in context.Permissies 
                                                where p.Id == permissionid
                                                select p).Single();
            if (e.ColumnIndex == 7)
            {
                //update visibility
                if (permissie.Visible == 1)
                {
                    permissie.Visible = 0;
                }
                else
                {
                    permissie.Visible = 1;
                }
            }

            if (e.ColumnIndex == 8)
            {
                //update editability
                if (permissie.Editable == 1)
                {
                    permissie.Editable = 0;
                }
                else
                {
                    permissie.Editable = 1;
                }
            }

            context.SaveChanges();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WfaSuperPC
{
    class UserMachine
    {
        UserState isBeheerder;
        UserState isManager;
        UserState isDirrecteur;
        UserState isVerkoper;
        UserState isAssembleerder;

        UserState userState;

        public UserMachine()
        {
      
           /* isBeheerder = new IsBeheerder(this);
            isManager = new IsManager(this);
            isDirrecteur = new IsDirrecteur(this);
            isVerkoper = new IsVerkoper(this);
            isAssembleerder = new IsAssembleerder(this);

            userState = isBeheerder;
            */

        }

        public void setUserState(UserState newUserState)
        {
            this.userState = newUserState;
        }
        void toonManagementInfo()
        {
            userState.toonManagementInfo();
        }

        void keurOfferte()
        {
            userState.keurOfferte();
        }


        void keurLeverancierBestelling(){
            userState.keurLeverancierBestelling();
        }

        //Dirrecteur
        void toonLogboek(int bestellingId){
            //userState.toonLogboek(int bestellingId);
        }

        //verkoper
        void maakOfferte(){
            userState.maakOfferte();
        }

        void plaatsBestelling(){
            userState.plaatsBestelling();
        }

        //Assembleerder
        void toonAssemblageTaken(){
            userState.toonAssemblageTaken();
        }

        void pakAssemblagetaak(){
            userState.pakAssemblagetaak();
        }

        void bestelProduct(int productId){
            //userState.bestelProduct(int productId);
        }

        //Beheerder
        // Met beheren wordt aanmaken en of wijzigen bedoeld.
        void beheerProduct(){
            userState.beheerProduct();
        }

        void beheerMedewerker(){
            userState.beheerMedewerker();
        }

        void beheerRollen(){
            userState.beheerRollen();
        }

        void beheerRechten(){
            userState.beheerRechten();
        }
    
    }
}
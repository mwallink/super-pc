﻿namespace WfaSuperPC
{
    partial class ProductForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.OpslaanBtn = new System.Windows.Forms.Button();
            this.AfbeeldingBtn = new System.Windows.Forms.Button();
            this.NaamTb = new System.Windows.Forms.TextBox();
            this.PrijsTb = new System.Windows.Forms.TextBox();
            this.NaamLabel = new System.Windows.Forms.Label();
            this.PrijsLabel = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.OmschrijvingTb = new System.Windows.Forms.RichTextBox();
            this.AnnulerenBtn = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.dataGridViewProduct = new System.Windows.Forms.DataGridView();
            this.ProductListId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProductImageColumn = new System.Windows.Forms.DataGridViewImageColumn();
            this.ProductNameColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewProduct)).BeginInit();
            this.SuspendLayout();
            // 
            // OpslaanBtn
            // 
            this.OpslaanBtn.Location = new System.Drawing.Point(408, 457);
            this.OpslaanBtn.Name = "OpslaanBtn";
            this.OpslaanBtn.Size = new System.Drawing.Size(75, 23);
            this.OpslaanBtn.TabIndex = 0;
            this.OpslaanBtn.Text = "Opslaan";
            this.OpslaanBtn.UseVisualStyleBackColor = true;
            this.OpslaanBtn.Click += new System.EventHandler(this.OpslaanBtn_Click);
            // 
            // AfbeeldingBtn
            // 
            this.AfbeeldingBtn.Location = new System.Drawing.Point(15, 254);
            this.AfbeeldingBtn.Name = "AfbeeldingBtn";
            this.AfbeeldingBtn.Size = new System.Drawing.Size(140, 23);
            this.AfbeeldingBtn.TabIndex = 1;
            this.AfbeeldingBtn.Text = "Afbeeldingen toevoegen";
            this.AfbeeldingBtn.UseVisualStyleBackColor = true;
            this.AfbeeldingBtn.Click += new System.EventHandler(this.AfbeeldingBtn_Click);
            // 
            // NaamTb
            // 
            this.NaamTb.Location = new System.Drawing.Point(97, 33);
            this.NaamTb.Name = "NaamTb";
            this.NaamTb.Size = new System.Drawing.Size(386, 20);
            this.NaamTb.TabIndex = 2;
            // 
            // PrijsTb
            // 
            this.PrijsTb.Location = new System.Drawing.Point(97, 63);
            this.PrijsTb.Name = "PrijsTb";
            this.PrijsTb.Size = new System.Drawing.Size(94, 20);
            this.PrijsTb.TabIndex = 3;
            // 
            // NaamLabel
            // 
            this.NaamLabel.AutoSize = true;
            this.NaamLabel.Location = new System.Drawing.Point(15, 36);
            this.NaamLabel.Name = "NaamLabel";
            this.NaamLabel.Size = new System.Drawing.Size(77, 13);
            this.NaamLabel.TabIndex = 4;
            this.NaamLabel.Text = "* Productnaam";
            // 
            // PrijsLabel
            // 
            this.PrijsLabel.AutoSize = true;
            this.PrijsLabel.Location = new System.Drawing.Point(15, 66);
            this.PrijsLabel.Name = "PrijsLabel";
            this.PrijsLabel.Size = new System.Drawing.Size(33, 13);
            this.PrijsLabel.TabIndex = 5;
            this.PrijsLabel.Text = "* Prijs";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(15, 93);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(67, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Omschrijving";
            // 
            // OmschrijvingTb
            // 
            this.OmschrijvingTb.Location = new System.Drawing.Point(97, 93);
            this.OmschrijvingTb.Name = "OmschrijvingTb";
            this.OmschrijvingTb.Size = new System.Drawing.Size(386, 143);
            this.OmschrijvingTb.TabIndex = 7;
            this.OmschrijvingTb.Text = "";
            // 
            // AnnulerenBtn
            // 
            this.AnnulerenBtn.Location = new System.Drawing.Point(316, 457);
            this.AnnulerenBtn.Name = "AnnulerenBtn";
            this.AnnulerenBtn.Size = new System.Drawing.Size(75, 23);
            this.AnnulerenBtn.TabIndex = 8;
            this.AnnulerenBtn.Text = "Annuleren";
            this.AnnulerenBtn.UseVisualStyleBackColor = true;
            this.AnnulerenBtn.Click += new System.EventHandler(this.AnnulerenBtn_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 467);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(152, 13);
            this.label1.TabIndex = 9;
            this.label1.Text = "Velden met een * zijn verplicht.";
            // 
            // dataGridViewProduct
            // 
            this.dataGridViewProduct.AllowUserToAddRows = false;
            this.dataGridViewProduct.AllowUserToDeleteRows = false;
            this.dataGridViewProduct.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewProduct.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ProductListId,
            this.ProductImageColumn,
            this.ProductNameColumn});
            this.dataGridViewProduct.Location = new System.Drawing.Point(18, 283);
            this.dataGridViewProduct.Name = "dataGridViewProduct";
            this.dataGridViewProduct.ReadOnly = true;
            this.dataGridViewProduct.Size = new System.Drawing.Size(465, 168);
            this.dataGridViewProduct.TabIndex = 10;
            // 
            // ProductListId
            // 
            this.ProductListId.HeaderText = "Id";
            this.ProductListId.Name = "ProductListId";
            this.ProductListId.ReadOnly = true;
            // 
            // ProductImageColumn
            // 
            this.ProductImageColumn.HeaderText = "Afbeeldingen";
            this.ProductImageColumn.Name = "ProductImageColumn";
            this.ProductImageColumn.ReadOnly = true;
            // 
            // ProductNameColumn
            // 
            this.ProductNameColumn.HeaderText = "Afbeeldingnaam";
            this.ProductNameColumn.Name = "ProductNameColumn";
            this.ProductNameColumn.ReadOnly = true;
            // 
            // ProductForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(495, 492);
            this.Controls.Add(this.dataGridViewProduct);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.AnnulerenBtn);
            this.Controls.Add(this.OmschrijvingTb);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.PrijsLabel);
            this.Controls.Add(this.NaamLabel);
            this.Controls.Add(this.PrijsTb);
            this.Controls.Add(this.NaamTb);
            this.Controls.Add(this.AfbeeldingBtn);
            this.Controls.Add(this.OpslaanBtn);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "ProductForm";
            this.Text = "ProductForm";
            this.Load += new System.EventHandler(this.ProductForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewProduct)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button OpslaanBtn;
        private System.Windows.Forms.Button AfbeeldingBtn;
        private System.Windows.Forms.TextBox NaamTb;
        private System.Windows.Forms.TextBox PrijsTb;
        private System.Windows.Forms.Label NaamLabel;
        private System.Windows.Forms.Label PrijsLabel;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.RichTextBox OmschrijvingTb;
        private System.Windows.Forms.Button AnnulerenBtn;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dataGridViewProduct;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProductListId;
        private System.Windows.Forms.DataGridViewImageColumn ProductImageColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProductNameColumn;
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WfaSuperPC
{
    class BestellingenMachine
    {
        /* Statussen:
             * 
             * aangemaakt
             * managerGoedkeuring
             * managerAfkeuring
             * klantGoedkeuring
             * klantAfkeuring
             * geassembleerd
             */

        BestellingenState aangemaakt; // Al dan niet voor een  zakelijke klant.
        BestellingenState managerGoedkeuring;
        BestellingenState managerAfkeuring;
        BestellingenState klantGoedkeuring;
        BestellingenState klantAfkeuring;
        BestellingenState geassembleerd;

        public BestellingenState bestellingenState { get; set; }

        public BestellingenMachine()
        {
            aangemaakt = new Aangemaakt(this);
            managerGoedkeuring = new ManagerGoedkeuring(this);
            managerAfkeuring = new ManagerAfkeuring(this);
            klantGoedkeuring = new KlantGoedkeuring(this);
            klantAfkeuring = new KlantAfkeuring(this);
            geassembleerd = new Geassembleerd(this);
        }

        // Wordt initieel aangeroepen
        public void maakBestelling(Klanten klant, List<ProductenTemp> productenLijst, Bestellingen bestelling, Personen ingelogdePersoon, SuperPcContainer context)
        {
            aangemaakt.maakBestelling(klant, productenLijst, bestelling, ingelogdePersoon, context);
        }

        public void managerKeurGoed(Bestellingen bestelling)
        {
            managerGoedkeuring.managerKeurGoed(bestelling);
        }

        public void managerKeurAf(Bestellingen bestelling)
        {
            managerAfkeuring.managerKeurAf(bestelling);
        }


        public void klantKeurGoed(Bestellingen bestelling)
        {
            klantGoedkeuring.klantKeurGoed(bestelling);
        }
        public void klantKeurAf(Bestellingen bestelling)
        {
            klantAfkeuring.klantKeurAf(bestelling);
        }

        // Gereed voor assemblage
        public void assembleren(Bestellingen bestelling)
        {
            geassembleerd.assembleren(bestelling);
        }



    }
}
﻿namespace WfaSuperPC
{
    partial class Openstaand
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.bestellingenBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.bestellingenDataSet = new WfaSuperPC.BestellingenDataSet();
            this.bestellingenTableAdapter = new WfaSuperPC.BestellingenDataSetTableAdapters.BestellingenTableAdapter();
            this.idCell = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.statusCell = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.klantCell = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bestellingenBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bestellingenDataSet)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idCell,
            this.statusCell,
            this.klantCell});
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(0, 0);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.Size = new System.Drawing.Size(920, 443);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellClick);
            // 
            // bestellingenBindingSource
            // 
            this.bestellingenBindingSource.DataMember = "Bestellingen";
            this.bestellingenBindingSource.DataSource = this.bestellingenDataSet;
            // 
            // bestellingenDataSet
            // 
            this.bestellingenDataSet.DataSetName = "BestellingenDataSet";
            this.bestellingenDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // bestellingenTableAdapter
            // 
            this.bestellingenTableAdapter.ClearBeforeFill = true;
            // 
            // idCell
            // 
            this.idCell.HeaderText = "bestelling id";
            this.idCell.Name = "idCell";
            this.idCell.ReadOnly = true;
            // 
            // statusCell
            // 
            this.statusCell.HeaderText = "status";
            this.statusCell.Name = "statusCell";
            this.statusCell.ReadOnly = true;
            // 
            // klantCell
            // 
            this.klantCell.HeaderText = "klant";
            this.klantCell.Name = "klantCell";
            this.klantCell.ReadOnly = true;
            // 
            // Openstaand
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(920, 443);
            this.Controls.Add(this.dataGridView1);
            this.Name = "Openstaand";
            this.Text = "Openstaand";
            this.Load += new System.EventHandler(this.Openstaand_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bestellingenBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bestellingenDataSet)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private BestellingenDataSet bestellingenDataSet;
        private System.Windows.Forms.BindingSource bestellingenBindingSource;
        private BestellingenDataSetTableAdapters.BestellingenTableAdapter bestellingenTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn idCell;
        private System.Windows.Forms.DataGridViewTextBoxColumn statusCell;
        private System.Windows.Forms.DataGridViewTextBoxColumn klantCell;
    }
}
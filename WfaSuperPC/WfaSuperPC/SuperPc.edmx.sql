
-- Warning: There were errors validating the existing SSDL. Drop statements
-- will not be generated.
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, and Azure
-- --------------------------------------------------
-- Date Created: 11/01/2013 14:41:28
-- Generated from EDMX file: D:\VSWorkspace\super-pc\WfaSuperPC\WfaSuperPC\SuperPc.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [SuperPc];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------


-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------


-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'Personen'
CREATE TABLE [dbo].[Personen] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Voornaam] nvarchar(max)  NOT NULL,
    [Achternaam] nvarchar(max)  NOT NULL,
    [Gebruikersnaam] nvarchar(max)  NOT NULL,
    [Wachtwoord] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'Rollen'
CREATE TABLE [dbo].[Rollen] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Naam] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'Producten'
CREATE TABLE [dbo].[Producten] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Naam] nvarchar(max)  NOT NULL,
    [Prijs] int  NOT NULL,
    [Omschrijving] nvarchar(max)  NULL,
    [Voorraad] int  NOT NULL,
    [LeveranciersId] int  NULL
);
GO

-- Creating table 'Statussen'
CREATE TABLE [dbo].[Statussen] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Naam] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'Leveranciers'
CREATE TABLE [dbo].[Leveranciers] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'Bestelregels'
CREATE TABLE [dbo].[Bestelregels] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [BestellingenId] int  NOT NULL,
    [FkAssembleerderId] int  NOT NULL,
    [ProductenId] int  NOT NULL,
    [AssemblageVoltooid] int  NOT NULL,
    [InBehandeling] int  NOT NULL,
    [Aantal] int  NOT NULL
);
GO

-- Creating table 'Bestellingen'
CREATE TABLE [dbo].[Bestellingen] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [KlantenId] int  NOT NULL,
    [FkVerkoperId] int  NOT NULL,
    [Korting] smallint  NOT NULL,
    [KlantAkkoord] bit  NOT NULL,
    [ManagerAkkoord] int  NOT NULL,
    [HeeftOfferte] bit  NOT NULL,
    [AanmaakDatum] datetime  NULL,
    [Aanbetaling] float  NOT NULL,
    [Betaald] bit  NOT NULL,
    [StatussenId] int  NOT NULL
);
GO

-- Creating table 'Permissies'
CREATE TABLE [dbo].[Permissies] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Visible] int  NOT NULL,
    [Editable] int  NOT NULL,
    [RollenId] int  NOT NULL,
    [ControlsId] int  NOT NULL
);
GO

-- Creating table 'FormControls'
CREATE TABLE [dbo].[FormControls] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Parent] nvarchar(max)  NOT NULL,
    [Name] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'ViewPermissies1'
CREATE TABLE [dbo].[ViewPermissies1] (
    [rol_id] int  NOT NULL,
    [rol_name] nvarchar(max)  NOT NULL,
    [control_id] int  NOT NULL,
    [control_name] nvarchar(max)  NOT NULL,
    [Parent] nvarchar(max)  NOT NULL,
    [RollenId] int  NOT NULL,
    [permissie_id] int  NOT NULL,
    [Editable] int  NOT NULL,
    [Visible] int  NOT NULL
);
GO

-- Creating table 'Klanten'
CREATE TABLE [dbo].[Klanten] (
    [Bedrijfsnaam] nvarchar(max)  NULL,
    [Land] nvarchar(max)  NULL,
    [Straatnaam] nvarchar(max)  NULL,
    [Huisnummer] int  NULL,
    [Huisnummertoevoeging] nvarchar(max)  NULL,
    [Postcode] nvarchar(max)  NULL,
    [Emailadres] nvarchar(max)  NULL,
    [Telefoonnummer] nvarchar(max)  NULL,
    [Gsmnummer] nvarchar(max)  NULL,
    [Id] int IDENTITY(1,1) NOT NULL,
    [Voornaam] nvarchar(max)  NOT NULL,
    [Achternaam] nvarchar(max)  NOT NULL,
    [ZakelijkKlant] bit  NOT NULL
);
GO

-- Creating table 'Afbeeldingen'
CREATE TABLE [dbo].[Afbeeldingen] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [url] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'StatusLoggers'
CREATE TABLE [dbo].[StatusLoggers] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [PersoonId] int  NOT NULL,
    [BestellingId] int  NOT NULL,
    [Wijzigingsdatum] datetime  NOT NULL,
    [BestellingsStatusVan] nvarchar(max)  NOT NULL,
    [BestellingsStatusNaar] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'PersonenRollen'
CREATE TABLE [dbo].[PersonenRollen] (
    [Personen_Id] int  NOT NULL,
    [Rollen_Id] int  NOT NULL
);
GO

-- Creating table 'AfbeeldingenProducten'
CREATE TABLE [dbo].[AfbeeldingenProducten] (
    [Afbeeldingen_Id] int  NOT NULL,
    [Producten_Id] int  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id] in table 'Personen'
ALTER TABLE [dbo].[Personen]
ADD CONSTRAINT [PK_Personen]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Rollen'
ALTER TABLE [dbo].[Rollen]
ADD CONSTRAINT [PK_Rollen]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Producten'
ALTER TABLE [dbo].[Producten]
ADD CONSTRAINT [PK_Producten]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Statussen'
ALTER TABLE [dbo].[Statussen]
ADD CONSTRAINT [PK_Statussen]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Leveranciers'
ALTER TABLE [dbo].[Leveranciers]
ADD CONSTRAINT [PK_Leveranciers]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Bestelregels'
ALTER TABLE [dbo].[Bestelregels]
ADD CONSTRAINT [PK_Bestelregels]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Bestellingen'
ALTER TABLE [dbo].[Bestellingen]
ADD CONSTRAINT [PK_Bestellingen]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Permissies'
ALTER TABLE [dbo].[Permissies]
ADD CONSTRAINT [PK_Permissies]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'FormControls'
ALTER TABLE [dbo].[FormControls]
ADD CONSTRAINT [PK_FormControls]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [rol_id], [control_id], [RollenId], [permissie_id] in table 'ViewPermissies1'
ALTER TABLE [dbo].[ViewPermissies1]
ADD CONSTRAINT [PK_ViewPermissies1]
    PRIMARY KEY CLUSTERED ([rol_id], [control_id], [RollenId], [permissie_id] ASC);
GO

-- Creating primary key on [Id] in table 'Klanten'
ALTER TABLE [dbo].[Klanten]
ADD CONSTRAINT [PK_Klanten]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Afbeeldingen'
ALTER TABLE [dbo].[Afbeeldingen]
ADD CONSTRAINT [PK_Afbeeldingen]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'StatusLoggers'
ALTER TABLE [dbo].[StatusLoggers]
ADD CONSTRAINT [PK_StatusLoggers]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Personen_Id], [Rollen_Id] in table 'PersonenRollen'
ALTER TABLE [dbo].[PersonenRollen]
ADD CONSTRAINT [PK_PersonenRollen]
    PRIMARY KEY NONCLUSTERED ([Personen_Id], [Rollen_Id] ASC);
GO

-- Creating primary key on [Afbeeldingen_Id], [Producten_Id] in table 'AfbeeldingenProducten'
ALTER TABLE [dbo].[AfbeeldingenProducten]
ADD CONSTRAINT [PK_AfbeeldingenProducten]
    PRIMARY KEY NONCLUSTERED ([Afbeeldingen_Id], [Producten_Id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [BestellingenId] in table 'Bestelregels'
ALTER TABLE [dbo].[Bestelregels]
ADD CONSTRAINT [FK_BestellingenBestelregels]
    FOREIGN KEY ([BestellingenId])
    REFERENCES [dbo].[Bestellingen]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_BestellingenBestelregels'
CREATE INDEX [IX_FK_BestellingenBestelregels]
ON [dbo].[Bestelregels]
    ([BestellingenId]);
GO

-- Creating foreign key on [RollenId] in table 'Permissies'
ALTER TABLE [dbo].[Permissies]
ADD CONSTRAINT [FK_RollenPermissies]
    FOREIGN KEY ([RollenId])
    REFERENCES [dbo].[Rollen]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_RollenPermissies'
CREATE INDEX [IX_FK_RollenPermissies]
ON [dbo].[Permissies]
    ([RollenId]);
GO

-- Creating foreign key on [ControlsId] in table 'Permissies'
ALTER TABLE [dbo].[Permissies]
ADD CONSTRAINT [FK_ControlsPermissies]
    FOREIGN KEY ([ControlsId])
    REFERENCES [dbo].[FormControls]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_ControlsPermissies'
CREATE INDEX [IX_FK_ControlsPermissies]
ON [dbo].[Permissies]
    ([ControlsId]);
GO

-- Creating foreign key on [Personen_Id] in table 'PersonenRollen'
ALTER TABLE [dbo].[PersonenRollen]
ADD CONSTRAINT [FK_PersonenRollen_Personen]
    FOREIGN KEY ([Personen_Id])
    REFERENCES [dbo].[Personen]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [Rollen_Id] in table 'PersonenRollen'
ALTER TABLE [dbo].[PersonenRollen]
ADD CONSTRAINT [FK_PersonenRollen_Rollen]
    FOREIGN KEY ([Rollen_Id])
    REFERENCES [dbo].[Rollen]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_PersonenRollen_Rollen'
CREATE INDEX [IX_FK_PersonenRollen_Rollen]
ON [dbo].[PersonenRollen]
    ([Rollen_Id]);
GO

-- Creating foreign key on [KlantenId] in table 'Bestellingen'
ALTER TABLE [dbo].[Bestellingen]
ADD CONSTRAINT [FK_KlantenBestellingen]
    FOREIGN KEY ([KlantenId])
    REFERENCES [dbo].[Klanten]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_KlantenBestellingen'
CREATE INDEX [IX_FK_KlantenBestellingen]
ON [dbo].[Bestellingen]
    ([KlantenId]);
GO

-- Creating foreign key on [Afbeeldingen_Id] in table 'AfbeeldingenProducten'
ALTER TABLE [dbo].[AfbeeldingenProducten]
ADD CONSTRAINT [FK_AfbeeldingenProducten_Afbeeldingen]
    FOREIGN KEY ([Afbeeldingen_Id])
    REFERENCES [dbo].[Afbeeldingen]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [Producten_Id] in table 'AfbeeldingenProducten'
ALTER TABLE [dbo].[AfbeeldingenProducten]
ADD CONSTRAINT [FK_AfbeeldingenProducten_Producten]
    FOREIGN KEY ([Producten_Id])
    REFERENCES [dbo].[Producten]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_AfbeeldingenProducten_Producten'
CREATE INDEX [IX_FK_AfbeeldingenProducten_Producten]
ON [dbo].[AfbeeldingenProducten]
    ([Producten_Id]);
GO

-- Creating foreign key on [FkVerkoperId] in table 'Bestellingen'
ALTER TABLE [dbo].[Bestellingen]
ADD CONSTRAINT [FK_PersonenBestellingen]
    FOREIGN KEY ([FkVerkoperId])
    REFERENCES [dbo].[Personen]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_PersonenBestellingen'
CREATE INDEX [IX_FK_PersonenBestellingen]
ON [dbo].[Bestellingen]
    ([FkVerkoperId]);
GO

-- Creating foreign key on [FkAssembleerderId] in table 'Bestelregels'
ALTER TABLE [dbo].[Bestelregels]
ADD CONSTRAINT [FK_PersonenBestelregels]
    FOREIGN KEY ([FkAssembleerderId])
    REFERENCES [dbo].[Personen]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_PersonenBestelregels'
CREATE INDEX [IX_FK_PersonenBestelregels]
ON [dbo].[Bestelregels]
    ([FkAssembleerderId]);
GO

-- Creating foreign key on [ProductenId] in table 'Bestelregels'
ALTER TABLE [dbo].[Bestelregels]
ADD CONSTRAINT [FK_BestelregelsProducten]
    FOREIGN KEY ([ProductenId])
    REFERENCES [dbo].[Producten]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_BestelregelsProducten'
CREATE INDEX [IX_FK_BestelregelsProducten]
ON [dbo].[Bestelregels]
    ([ProductenId]);
GO

-- Creating foreign key on [LeveranciersId] in table 'Producten'
ALTER TABLE [dbo].[Producten]
ADD CONSTRAINT [FK_LeveranciersProducten]
    FOREIGN KEY ([LeveranciersId])
    REFERENCES [dbo].[Leveranciers]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_LeveranciersProducten'
CREATE INDEX [IX_FK_LeveranciersProducten]
ON [dbo].[Producten]
    ([LeveranciersId]);
GO

-- Creating foreign key on [StatussenId] in table 'Bestellingen'
ALTER TABLE [dbo].[Bestellingen]
ADD CONSTRAINT [FK_StatussenBestellingen]
    FOREIGN KEY ([StatussenId])
    REFERENCES [dbo].[Statussen]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_StatussenBestellingen'
CREATE INDEX [IX_FK_StatussenBestellingen]
ON [dbo].[Bestellingen]
    ([StatussenId]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------
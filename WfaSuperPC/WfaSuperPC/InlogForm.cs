﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WfaSuperPC
{
    public partial class InlogForm : Form
    {
        public bool LoggedIn { get; private set; }
        public int PersoonId{get; set;}

        public InlogForm()
        {
            this.LoggedIn = false;
            InitializeComponent();

            //hide UnitTestForm Button in productie modus
            if (AppConfigSuperPC.productionModus == true)
            {
                this.btnFormUnitTest.Hide();
            }
            else
            {
                this.gebruikersnaamTextBox.Text = "admin";
                this.wachtwoordTextBox.Text = "admin";

            }

            this.lblFeedback.Visible = false;
        }

        private void btnInlog_Click(object sender, EventArgs e)
        {
            this.PersoonId = CheckPassword(this.gebruikersnaamTextBox.Text, this.wachtwoordTextBox.Text);
            if (this.LoggedIn == false)
            {
                this.gebruikersnaamTextBox.Text = " ";
                this.wachtwoordTextBox.Text = " ";

                this.lblFeedback.Text = "Uw gebruikersnaam en/of wachtwoord is niet juist. Probeer opnieuw."; 
                this.lblFeedback.Visible = true;
            }
            else
            {
                this.Close();
            }
        }

        private void btnFormUnitTest_Click(object sender, EventArgs e)
        {
            Form unitTest = new UnitTestForm();
            unitTest.Show();
        }

        private int CheckPassword(String username, String password)
        {
            SuperPcContainer context = new SuperPcContainer();
            //get username - password
            var ngebruiker = (from p in context.Personen 
                                 where p.Gebruikersnaam == username && p.Wachtwoord == password
                                 select p).Count();

            if (ngebruiker == 1)
            {
                this.LoggedIn = true;
                return (from p in context.Personen
                                  where p.Gebruikersnaam == username && p.Wachtwoord == password
                                  select p.Id).Single();
            }
            else
            {
                return -1;
            }
        }

        private void InlogForm_Load(object sender, EventArgs e)
        {

        }
    }
}

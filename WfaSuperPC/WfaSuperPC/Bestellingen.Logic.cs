﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WfaSuperPC
{
    public partial class Bestellingen
    {
        public void IAmPartial()
        {
            Console.WriteLine("I'm partial");
        }


        public void LogBestellingsStatus(int persoonid, string bestellingsstatusvan, string bestellingsstatusnaar)
        {
            SuperPcContainer ctx = new SuperPcContainer();
            StatusLoggers status = new StatusLoggers();
            status.BestellingId = this.Id;
            status.BestellingsStatusVan = bestellingsstatusvan;
            status.BestellingsStatusNaar = bestellingsstatusnaar;
            status.PersoonId = persoonid;
            ctx.SaveChanges();
        }

    }
}

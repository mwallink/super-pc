﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.EntityClient;
using System.Data.Objects;
using System.Data.Objects.DataClasses;
using System.Text;

namespace WfaSuperPC
{

    static class Program
    {
        //http://stackoverflow.com/questions/1122483/c-sharp-random-string-generator
        private static Random random = new Random((int)DateTime.Now.Ticks);//thanks to McAden
        private static string RandomString(int size)
        {
            StringBuilder builder = new StringBuilder();
            char ch;
            for (int i = 0; i < size; i++)
            {
                ch = Convert.ToChar(Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 65)));
                builder.Append(ch);
            }

            return builder.ToString();
        }


        private static Dictionary<int,String> FakeParentNames;   
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            FakeParentNames = new Dictionary<int,string>();

            if (AppConfigSuperPC.emptyContext == true)
                emptyContext();

            if (!AppConfigSuperPC.productionModus == true)
                fillContext();
            
            BaseForm baseform = new BaseForm();
            //baseform.Visible = false; => constructor baseform heeft een blocking method
            // showdialog => voor het inlogform
            Application.Run();
        }

        private static void fillContext()
        {
            if (AppConfigSuperPC.registerControls == true)
            {
                AppConfigSuperPC.registeringConrolsOperation = true;
                registerControls();
            }
            AppConfigSuperPC.registeringConrolsOperation = false;

            if (AppConfigSuperPC.registerRollen == true)
                registerRollen();

            if (AppConfigSuperPC.registerPermissies == true)
                registerPermissies();

            if (AppConfigSuperPC.registerPersonen == true)
                registerPersonen();

            if (AppConfigSuperPC.registerLeveranciers == true)
                registerLeveranciers();

            if (AppConfigSuperPC.registerProducten == true)
                registerProducten();

            if (AppConfigSuperPC.registerKlanten == true)
                registerKlanten();

            if (AppConfigSuperPC.registerStatussen == true)
                registerStatussen();

            if (AppConfigSuperPC.registerBestellingen == true)
                registerBestellingen();
        }

        private static void emptyContext()
        {
            SuperPcContainer context = new SuperPcContainer();

            //////////////////////////////
            //Remove bestellingen
            //////////////////////////////
            var queryObjectList = from o in context.Bestellingen select o;
            foreach (var o in queryObjectList)
                context.Bestellingen.Remove(o);
            context.SaveChanges();

            //////////////////////////////
            //Remove leveranciers en producten
            //////////////////////////////
            var queryLeveranciersList = from l in context.Leveranciers select l;
            foreach (var l in queryLeveranciersList)
            {

                foreach (var p in l.Producten)
                {
                    p.LeveranciersId = null;
                }

                context.Leveranciers.Remove(l);
            }
            context.SaveChanges();


            var queryProductenList = from product in context.Producten select product;
            foreach (var product in queryProductenList)
            {
                context.Producten.Remove(product);
            }

            //////////////////////////////
            // Remove Statussen
            //////////////////////////////
            var queryStatussenList = (from Statussen in context.Statussen select Statussen);

            foreach (var s in queryStatussenList)
            {
                context.Statussen.Remove(s);
            }
            context.SaveChanges();



            //////////////////////////////
            //Remove rollen
            //////////////////////////////
            //Verzamel bestaande rollen in de database 
            var queryRollenList = (from control in context.Rollen
                                   select control);

            foreach (var r in queryRollenList)
            {
                foreach (var p in r.Personen.ToList())
                {
                    r.Personen.Remove(p);
                }
                context.Rollen.Remove(r);
            }
            context.SaveChanges();
                        

            //////////////////////////////
            //Remove formcontrols
            //////////////////////////////
            var queryControlList = (from control in context.FormControls
                                    select control);
            foreach (var c in queryControlList)
            {
                context.FormControls.Remove(c);
            }
            context.SaveChanges();


            //////////////////////////////
            //Remove personen
            //////////////////////////////
            var queryPersonenList = from persoon in context.Personen select persoon;
            foreach (var p in queryPersonenList)
            {
                context.Personen.Remove(p);
            }
            context.SaveChanges();
            

            //////////////////////////////
            //Remove klanten
            //////////////////////////////
            var queryKlantenList = from o in context.Klanten select o;
            foreach (var o in queryKlantenList)
                context.Klanten.Remove(o);

            context.SaveChanges();

            ////////////////////////////
            ///PERMISSIES CASCADE DELETED
            ////////////////////////////
        }

        //done
        private static void registerBestellingen()
        {
            SuperPcContainer context = new SuperPcContainer();

            //Selecteer klanten
            var queryKlantList = from k in context.Klanten select k;

            //Selecteer producten
            var queryProductList = from p in context.Producten select p;

            //loop klant
            ////loop 2 bestelling
            //////loop 3 producten toevoegen -> bestelregel
            foreach(var k in queryKlantList){

                for (int i = 0; i < 2; i++ )
                {
                    Bestellingen b = new Bestellingen();

                    // Standaard status is aangemaakt.
                    b.StatussenId = (from s in context.Statussen select s.Id).First();

                    //FOREIGN KEY constraint \"FK_StatussenBestellingen\". 
                    //The conflict occurred in database \"SuperPc\", table \"dbo.Statussen\", column 'Id'.\r\nThe statement has been terminated."}

                    b.AanmaakDatum = DateTime.Now;
                    b.FkVerkoperId = (from p in context.Personen select p.Id).First();
                    b.KlantenId = k.Id;

                    int productid = 0;
                    int persoonid = 0;
                    foreach (var p in queryProductList)
                    {
                        Bestelregels br = new Bestelregels();
                        br.ProductenId = p.Id;
                        //br.BestellingenId = b.Id;
                        br.FkAssembleerderId = (from per in context.Personen select per.Id).First();
                        b.Bestelregels.Add(br);

                        productid = p.Id;
                        persoonid = br.FkAssembleerderId;
                    }

                    Bestelregels brl = new Bestelregels();
                    brl.ProductenId = productid;
                    brl.FkAssembleerderId = persoonid;
                    context.Bestellingen.Add(b);
                }
            }
            context.SaveChanges();
            
        }

        private static void registerStatussen()
        {
            SuperPcContainer context = new SuperPcContainer();

            /* Statussen:
             * 
             * aangemaakt
             * managerGoedkeuring
             * managerAfkeuring
             * klantGoedkeuring
             * klantAfkeuring
             * geassembleerd
             */

            string[] statussen = new string[6];
            statussen[0] = "aangemaakt";
            statussen[1] = "managerGoedkeuring";
            statussen[2] = "managerAfkeuring";
            statussen[3] = "klantGoedkeuring";
            statussen[4] = "klantAfkeuring";
            statussen[5] = "geassembleerd";

            for (int i = 0; i < 6; i++)
            {
                Statussen s = new Statussen();
                s.Naam = statussen[i];
                context.Statussen.Add(s);
            }
            context.SaveChanges();
        }

        private static void registerKlanten()
        {
            SuperPcContainer context = new SuperPcContainer();

            for (int i = 0; i < 2; i++)
            {
                Klanten o = new Klanten();
                o.Voornaam = RandomString(6);
                o.Achternaam = RandomString(10);
                context.Klanten.Add(o);
            }
            context.SaveChanges();
        }

        //done
        private static void registerProducten()
        {
            SuperPcContainer context = new SuperPcContainer();

            //Selecteer Leveranciers
            var queryLeveranciersList = from l in context.Leveranciers select l;

            //Voeg producten toe aan leveranciers
            foreach (var l in queryLeveranciersList)
            {
                Producten p = new Producten();
                p.LeveranciersId = l.Id;
                p.Naam = RandomString(10);
                p.Omschrijving = RandomString(20);
                p.Prijs = 15;
                l.Producten.Add(p);
            }
            context.SaveChanges();
        }

        //done
        private static void registerLeveranciers()
        {
            SuperPcContainer context = new SuperPcContainer();
            
            for (int i = 0 ; i < 2 ; i++)
            {
                Leveranciers l = new Leveranciers();
                l.Name = RandomString(6);
                context.Leveranciers.Add(l);
            }
            context.SaveChanges();
        }

        //done
        private static void registerPersonen()
        {
            SuperPcContainer context = new SuperPcContainer();

            //Selecteer rollen
            var queryRollenList = (from control in context.Rollen
                                   select control);
            
            foreach(var r in queryRollenList){
            Personen persoon = new Personen();
            persoon.Gebruikersnaam = r.Naam;
            persoon.Voornaam = r.Naam;
            persoon.Achternaam = r.Naam;
            persoon.Wachtwoord = r.Naam;
            context.Personen.Add(persoon);
            r.Personen.Add(persoon);
            }
            context.SaveChanges();
        }

        private static void registerPermissies()
        {
            SuperPcContainer context = new SuperPcContainer();

            //Selecteer rollen, geef rollen permissies in de database 
            var queryRollenList = (from control in context.Rollen
                                   select control);

            var queryFormControls = (from control in context.FormControls
                                     select control);
            
            //voeg voor elke rol alle formcontrols toe
            foreach (var r in queryRollenList)
            {
                foreach (var fc in queryFormControls)
                {
                    Permissies permissie = new Permissies();
                    permissie.Visible = 1;
                    permissie.Editable = 1;
                    permissie.RollenId = r.Id;
                    permissie.ControlsId = fc.Id;
                    context.Permissies.Add(permissie);
                }
            }
            context.SaveChanges();
        }

        //done
        private static void registerRollen()
        {
            SuperPcContainer context = new SuperPcContainer();

            //Creeer default rollen
            List<string> listrollen = new List<string>();
            listrollen = (List<string>)AppConfigSuperPC.getDefaultRollen();
            foreach (string s in listrollen)
            {
                Rollen rol = new Rollen();
                rol.Naam = s;
                context.Rollen.Add(rol);
            }
            context.SaveChanges();
        }

        //done check applicatie configuratie
        static void registerControls()
        {
            SuperPcContainer context = new SuperPcContainer();

            //Selecteer alle subklasses van AuthorizedForm
             var query = from type in
                            Assembly.GetExecutingAssembly().GetTypes().Where(x => x.IsSubclassOf(typeof(AuthorizedForm)))
                        select type;

            //Registreer van elke form de controls
             foreach (Type t in query)
             {
                 //String an = typeof(Program).Assembly.GetName().Name;
                 //ObjectType instance = (ObjectType)Activator.CreateInstance("MyNamespace.ObjectType, MyAssembly");

                 //Maak instantie
                 Form countForm = (Form)Activator.CreateInstance(t);

                 List<Control> listFormControls = new List<Control>();

                 //get controls
                 foreach (Control c in countForm.Controls)
                 {
                     listFormControls = (List<Control>)getListControlsRecursive(c);

                     //register controls
                     foreach (Control ctl in listFormControls)
                         pushControlToDatabase(ctl);
                 }
             }
        }

        
        private static IEnumerable<Control> getListControlsRecursive(Control parent)
        {
            List<Control> controls = new List<Control>();

            //Een uitzondering voor Toolstrips, want die worden niet gezien
            //als controls...
            if (parent.GetType() == typeof(ToolStrip))
            {
                foreach (ToolStripItem item in ((ToolStrip)parent).Items)
                {
                    //ToolStripItem komen niet van control, dus we faken de control voor opslag in de database.

                    Control c = new Button();
                    c.Name = item.Name;
                    c.Text = item.Text;

                    //registreer fake parentname
                    FakeParentNames.Add(c.GetHashCode(), parent.Name);

                    controls.AddRange(getListControlsRecursive(c));
                }
            }
            else
            {
                foreach (Control child in parent.Controls)
                {
                    controls.AddRange(getListControlsRecursive(child));
                }
            }

            controls.Add(parent);

            return controls;
        }

        private static void pushControlToDatabase(Control c)
        {
            SuperPcContainer context = new SuperPcContainer();
                
                var control = new FormControls();

                //check on null
                if (c.Parent == null)
                {
                    
                    if (FakeParentNames.ContainsKey(c.GetHashCode()))
                    {
                        control.Parent = FakeParentNames[c.GetHashCode()];
                    }
                    else
                    {
                        control.Parent = "null";
                    }
                }

                //create parent name
                else if (c.Parent.Name == "")
                {
                    control.Parent = c.Parent.GetType().ToString() + " " + c.Parent.GetHashCode();
                }
                else
                {
                    control.Parent = c.Parent.Name;
                }
                
                //create control name
                if (c.Name == "")
                {
                    control.Name = control.GetType().ToString() + " " + control.GetHashCode();
                }
                else
                {
                    control.Name = c.Name;
                }
                context.FormControls.Add(control);
                context.SaveChanges();
        }
    }
}

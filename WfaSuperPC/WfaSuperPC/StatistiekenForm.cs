﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace WfaSuperPC
{
    public partial class StatistiekenForm : AuthorizedForm
    {
        public StatistiekenForm()
        {
            InitializeComponent();
        }

        public StatistiekenForm(int persoonid)
        {
            InitializeComponent();
            this.PersoonId = persoonid;
            GetStatistiekenFromSp();
        
        }

        private void StatistiekenForm_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'bestellingLogDataSet.StatusLoggers' table. You can move, or remove it, as needed.
            this.statusLoggersTableAdapter.Fill(this.bestellingLogDataSet.StatusLoggers);
            // TODO: This line of code loads data into the 'bestellingLogDataSet.StatusLoggers' table. You can move, or remove it, as needed.
            this.statusLoggersTableAdapter.Fill(this.bestellingLogDataSet.StatusLoggers);

        }

        private void statusLoggersBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.statusLoggersBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.bestellingLogDataSet);

        }

        private void GetStatistiekenFromSp()
        {
            var opbrengst = 0.0;
            var uitstaandekosten = 0.0;
            var besteverkoper = 0;
            var dt = DateTime.Now;
            //dt = EntityFunctions.TruncateTime(dt);

            try
            {
                SqlConnection con = new SqlConnection("Data Source=ERNSTBOLT-PC\\SQLEXPRESS;Initial Catalog=SuperPC;"
                                                      + "Integrated Security=true");
                con.Open();

                SqlCommand cmd = new SqlCommand("statistieken", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@dagopbrengstdatum", SqlDbType.DateTime));
                cmd.Parameters.Add(new SqlParameter("@besteverkoperid", SqlDbType.Int, 10, ParameterDirection.Output,
                                                    false, 4, 2, "verkoperid", DataRowVersion.Default, null));
                cmd.Parameters.Add(new SqlParameter("@dagopbrengst", SqlDbType.Float, 10, ParameterDirection.Output,
                                                    false, 4, 2, "dagopbrengst", DataRowVersion.Default, null));
                cmd.Parameters.Add(new SqlParameter("@pctuitstaandekosten", SqlDbType.Float, 10,
                                                    ParameterDirection.Output, false, 4, 1, "pctuitstaandekosten",
                                                    DataRowVersion.Default, null));

                cmd.Parameters[0].Value = DateTime.Now;
                //cmd.UpdatedRowSource = UpdateRowSource.OutputParameters;
                cmd.ExecuteNonQuery();
                besteverkoper = (Int32) cmd.Parameters["@besteverkoperid"].Value;

                opbrengst = cmd.Parameters["@dagopbrengst"].Value == DBNull.Value
                                ? 0
                                : (double) cmd.Parameters["@dagopbrengst"].Value;
                uitstaandekosten = cmd.Parameters["@pctuitstaandekosten"].Value == DBNull.Value
                                       ? 0
                                       : (double) cmd.Parameters["@pctuitstaandekosten"].Value;

                SuperPcContainer ctx = new SuperPcContainer();
                Personen p = ctx.Personen.Where(per => per.Id == besteverkoper).Select(per => per).Single();
                this.tbBesteVerkoper.Text = p.Id.ToString() + " " + p.Voornaam + " " + p.Achternaam;
                this.tbDagopbrengst.Text = opbrengst.ToString();
                this.tbPercentageUitstaandeKosten.Text = uitstaandekosten.ToString();
            }
            catch
            {
                MessageBox.Show("Kan geen connectie maken met DB.");
            }
        }
    }
}

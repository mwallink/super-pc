﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WfaSuperPC
{
    class ManagerAfkeuring : BestellingenState
    {

        BestellingenMachine bestellingenMachine;
        SuperPcContainer context = new SuperPcContainer();

        public ManagerAfkeuring(BestellingenMachine bm)
        {
            this.bestellingenMachine = bm;
        }


        // Wordt initieel aangeroepen
        public void maakBestelling(Klanten klant, List<ProductenTemp> productenLijst, Bestellingen bestelling, Personen ingelogdePersoon, SuperPcContainer context)
        {
           
        }

        public void managerKeurGoed(Bestellingen bestelling)
        {
           
        }

        public void managerKeurAf(Bestellingen bestelling)
        {
            /*
            * aangemaakt
            * managerGoedkeuring
            * managerAfkeuring
            * klantGoedkeuring
            * klantAfkeuring
            * geassembleerd
            */

            bestelling = context.Bestellingen.Find(bestelling.Id);

            var statussen =
                from status in context.Statussen
                where status.Naam == "managerAfkeuring"
                select new
                {
                    statusId = status.Id
                };

            foreach (var status in statussen)
            {
                bestelling.StatussenId = status.statusId;
            }

            context.SaveChanges();
        }

        public void klantKeurGoed(Bestellingen bestelling)
        {
            
        }

        public void klantKeurAf(Bestellingen bestelling)
        {
            
        }

        public void assembleren(Bestellingen bestelling)
        {
           
        }


    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.Objects;
using System.Data.Entity;

namespace WfaSuperPC
{
    public partial class UnitTestForm : Form
    {
        public UnitTestForm()
        {
            InitializeComponent();
        }

        private void btnDeleteSetNull_Click(object sender, EventArgs e)
        {
            //using (var context = new Context()) <= snap ik niet

            
            SuperPcContainer context = new SuperPcContainer();
            {
                var bestelling = new Bestellingen();

                var bestelregel = new Bestelregels();
                bestelling.Bestelregels.Add(bestelregel);
                
                context.Bestellingen.Add(bestelling);
                context.SaveChanges();

                context.Bestellingen.Remove(bestelling);
                context.SaveChanges();
            }
             /**/
        }

        private void UnitTest_Load(object sender, EventArgs e)
        {
            SuperPcContainer context = new SuperPcContainer();
            //check aanwezigheid controls in database
            lblDbFormControlCount.Text = "n FormControls in database: " + ((from c in context.FormControls
                               select c).Count().ToString());
        }

        private void btnPartialClassExecute_Click(object sender, EventArgs e)
        {
            var bestelling = new Bestellingen();
            bestelling.IAmPartial();
        }

        private void btnLinqFetchLimit_Click(object sender, EventArgs e)
        {
            SuperPcContainer context = new SuperPcContainer();

            var Bestellingen = (from t in context.Bestellingen
                       select t).OrderBy(b => b.Id).Skip(1).Take(2);

            StringBuilder Output = new StringBuilder();

            foreach (var Bestelling in Bestellingen)
                Output.Append(Bestelling.Id + "\r\n");

            MessageBox.Show(Output.ToString());

        }

        private void btnInlogForm_Click(object sender, EventArgs e)
        {
            InlogForm InlogForm = new InlogForm();
            InlogForm.Show();
        }

        private void btnAdoSelect_Click(object sender, EventArgs e)
        {
            string connectionString =
            "Data Source=ERNSTBOLT-PC\\SQLEXPRESS;Initial Catalog=SuperPC;"
            + "Integrated Security=true";

            // Provide the query string with a parameter placeholder.
            string queryString =
            "SELECT * from dbo.Personen "
            + "WHERE Id = @certainId "
            + "ORDER BY Voornaam DESC;";

            // Specify the parameter value.
            int paramValue = 2;

            // Create and open the connection in a using block. This
            // ensures that all resources will be closed and disposed
            // when the code exits.
            using (SqlConnection connection =
            new SqlConnection(connectionString))
            {
                // Create the Command and Parameter objects.
                SqlCommand command = new SqlCommand(queryString, connection);
                command.Parameters.AddWithValue("@certainId", paramValue);

                // Open the connection in a try/catch block.
                // Create and execute the DataReader, writing the result
                // set to the console window.
                try
                {
                    connection.Open();
                    SqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        Console.WriteLine("\t{0}\t{1}\t{2}",
                        reader[0], reader[1], reader[2]);
                    }
                    reader.Close();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
                Console.ReadLine();
            }

        }

        private void btnAdoInsert_Click(object sender, EventArgs e)
        {
            SqlConnection conn = new SqlConnection( "Data Source=ERNSTBOLT-PC\\SQLEXPRESS;Initial Catalog=SuperPC;"
            + "Integrated Security=true");

            string sql = "INSERT INTO Personen (Voornaam,Achternaam,Gebruikersnaam,Wachtwoord ) VALUES (@Val1,@Val2,@Val3,@Val4)";

            try
            {

                conn.Open();

                SqlCommand cmd = new SqlCommand(sql, conn);

                cmd.Parameters.AddWithValue("@Val1", "Ernst");

                cmd.Parameters.AddWithValue("@Val2", "Bolt" );

                cmd.Parameters.AddWithValue("@Val3", "erbo");

                cmd.Parameters.AddWithValue("@Val4", "erbo");

                cmd.CommandType = CommandType.Text;

                cmd.ExecuteNonQuery();

            }

            catch (System.Data.SqlClient.SqlException ex)
            {

                string msg = "Insert Error:";

                msg += ex.Message;

                throw new Exception(msg);

            }

            finally
            {

                conn.Close();

            }

        }

        private void btnAuthorizedForm_Click(object sender, EventArgs e)
        {
            AuthorizedForm authForm = new TestAuthorizedForm();
            authForm.Show();
        }

        private void btnListForms_Click(object sender, EventArgs e)
        {
            /*
            string @namespace = "WfaSuperPC";

            var q = (from t in Assembly.GetExecutingAssembly().GetTypes()
                    where t.IsClass && t.Namespace == @namespace
                    select t);
            q.ToList().ForEach(t => Console.WriteLine(t.Name));
            q.ToList().ForEach(t => Console.WriteLine(t.GetType()));
            */

            var query = from type in 
                            Assembly.GetExecutingAssembly().GetTypes().Where(x => x.IsSubclassOf(typeof(Form)))
                            select type;

            foreach (Type t in query)
            {
                Console.WriteLine(t.FullName);
            }
        
    }
        //http://stackoverflow.com/questions/10911249/returning-multiple-values-from-a-stored-procedure
        private void btnStatistieken_Click(object sender, EventArgs e)
        {
            var opbrengst = 0.0;
            var uitstaandekosten = 0.0;
            var besteverkoper = 0;
            var dt = DateTime.Now;
            //dt = EntityFunctions.TruncateTime(dt);

            SqlConnection con = new SqlConnection( "Data Source=ERNSTBOLT-PC\\SQLEXPRESS;Initial Catalog=SuperPC;"
            + "Integrated Security=true");
            con.Open();

            SqlCommand cmd = new SqlCommand("statistieken", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@dagopbrengstdatum", SqlDbType.DateTime));
            cmd.Parameters.Add(new SqlParameter("@besteverkoperid",SqlDbType.Int,10,ParameterDirection.Output,false,4,2,"verkoperid",DataRowVersion.Default,null));
            cmd.Parameters.Add(new SqlParameter("@dagopbrengst", SqlDbType.Float, 10, ParameterDirection.Output, false, 4, 2, "dagopbrengst", DataRowVersion.Default, null));
            cmd.Parameters.Add(new SqlParameter("@pctuitstaandekosten", SqlDbType.Float, 10, ParameterDirection.Output, false, 4, 1, "pctuitstaandekosten", DataRowVersion.Default, null));

            cmd.Parameters[0].Value = DateTime.Now;
            //cmd.UpdatedRowSource = UpdateRowSource.OutputParameters;
            cmd.ExecuteNonQuery();
            besteverkoper = (Int32)cmd.Parameters["@besteverkoperid"].Value;
            
            opbrengst = cmd.Parameters["@dagopbrengst"].Value == DBNull.Value ? 0 : (double)cmd.Parameters["@dagopbrengst"].Value;
            uitstaandekosten = cmd.Parameters["@pctuitstaandekosten"].Value == DBNull.Value ? 0 : (double)cmd.Parameters["@pctuitstaandekosten"].Value;
        }
    
    }
}

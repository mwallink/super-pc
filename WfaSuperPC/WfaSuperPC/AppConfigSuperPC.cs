﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WfaSuperPC
{
    public static class AppConfigSuperPC
    {
        public static bool productionModus = false;
        public static bool registerControls = true;
        public static bool registeringConrolsOperation = false; //altijd op false laten staan
        public static bool registerRollen = true;
        public static bool registerPermissies = true;
        public static bool registerPersonen = true;
        public static bool registerLeveranciers= true;
        public static bool registerProducten = true;
        public static bool registerKlanten = true;
        public static bool registerStatussen = true;
        public static bool registerBestellingen = true;
        public static bool emptyContext = true;


        public static IEnumerable<string> getDefaultRollen(){
        List<string> listRollen = new List<string>();
        listRollen.Add("admin");
        listRollen.Add("manager");
        listRollen.Add("gebruiker");
        return listRollen;
    }

    }    

}
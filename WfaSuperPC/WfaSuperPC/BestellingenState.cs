﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WfaSuperPC
{
    /*
     * statussen:
     * 
     * 
     */
    interface BestellingenState
    {
        /* Statussen:
             * 
             * aangemaakt
             * managerGoedkeuring
             * managerAfkeuring
             * klantGoedkeuring
             * klantAfkeuring
             * geassembleerd
             */

        // Wordt initieel aangeroepen
        void maakBestelling(Klanten klant, List<ProductenTemp> productenLijst, Bestellingen bestelling, Personen ingelogdePersoon, SuperPcContainer context);
        void managerKeurGoed(Bestellingen bestelling);
        void managerKeurAf(Bestellingen bestelling);
        void klantKeurGoed(Bestellingen bestelling);
        void klantKeurAf(Bestellingen bestelling);
        void assembleren(Bestellingen bestelling);
    }
}

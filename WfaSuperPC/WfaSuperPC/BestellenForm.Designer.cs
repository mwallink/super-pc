﻿namespace WfaSuperPC
{
    partial class BestellenForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.createOrderBtn = new System.Windows.Forms.Button();
            this.approveOrderBtn = new System.Windows.Forms.Button();
            this.disapproveOrderBtn = new System.Windows.Forms.Button();
            this.placeOrderBtn = new System.Windows.Forms.Button();
            this.productTable = new System.Windows.Forms.TableLayoutPanel();
            this.label12 = new System.Windows.Forms.Label();
            this.addProductBtn = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.customerLabel = new System.Windows.Forms.Label();
            this.addCustomerBtn = new System.Windows.Forms.Button();
            this.payBtn = new System.Windows.Forms.Button();
            this.fontDialog1 = new System.Windows.Forms.FontDialog();
            this.payTb = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.productTotalLabel = new System.Windows.Forms.Label();
            this.productTotalDiscountLabel = new System.Windows.Forms.Label();
            this.taxLabel = new System.Windows.Forms.Label();
            this.productTotalPriceLabel = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.discountBtn = new System.Windows.Forms.NumericUpDown();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.offerteCB = new System.Windows.Forms.CheckBox();
            this.weigerOfferteBtn = new System.Windows.Forms.Button();
            this.productTable.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.discountBtn)).BeginInit();
            this.tableLayoutPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // createOrderBtn
            // 
            this.createOrderBtn.Location = new System.Drawing.Point(617, 299);
            this.createOrderBtn.Name = "createOrderBtn";
            this.createOrderBtn.Size = new System.Drawing.Size(131, 23);
            this.createOrderBtn.TabIndex = 0;
            this.createOrderBtn.Text = "Bestelling aanmaken";
            this.createOrderBtn.UseVisualStyleBackColor = true;
            this.createOrderBtn.Click += new System.EventHandler(this.createOrderBtn_Click);
            // 
            // approveOrderBtn
            // 
            this.approveOrderBtn.Location = new System.Drawing.Point(617, 326);
            this.approveOrderBtn.Name = "approveOrderBtn";
            this.approveOrderBtn.Size = new System.Drawing.Size(131, 23);
            this.approveOrderBtn.TabIndex = 1;
            this.approveOrderBtn.Text = "Bestelling goedkeuren";
            this.approveOrderBtn.UseVisualStyleBackColor = true;
            this.approveOrderBtn.Click += new System.EventHandler(this.approveOrderBtn_Click);
            // 
            // disapproveOrderBtn
            // 
            this.disapproveOrderBtn.Location = new System.Drawing.Point(617, 353);
            this.disapproveOrderBtn.Name = "disapproveOrderBtn";
            this.disapproveOrderBtn.Size = new System.Drawing.Size(131, 23);
            this.disapproveOrderBtn.TabIndex = 2;
            this.disapproveOrderBtn.Text = "Bestelling afkeuren";
            this.disapproveOrderBtn.UseVisualStyleBackColor = true;
            this.disapproveOrderBtn.Click += new System.EventHandler(this.disapproveOrderBtn_Click);
            // 
            // placeOrderBtn
            // 
            this.placeOrderBtn.Location = new System.Drawing.Point(617, 407);
            this.placeOrderBtn.Name = "placeOrderBtn";
            this.placeOrderBtn.Size = new System.Drawing.Size(131, 23);
            this.placeOrderBtn.TabIndex = 3;
            this.placeOrderBtn.Text = "Bestelling plaatsen";
            this.placeOrderBtn.UseVisualStyleBackColor = true;
            this.placeOrderBtn.Click += new System.EventHandler(this.placeOrderBtn_Click);
            // 
            // productTable
            // 
            this.productTable.AccessibleRole = System.Windows.Forms.AccessibleRole.ScrollBar;
            this.productTable.AutoScroll = true;
            this.productTable.ColumnCount = 5;
            this.productTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.productTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.productTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.productTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.productTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.productTable.Controls.Add(this.label12, 0, 0);
            this.productTable.Location = new System.Drawing.Point(10, 114);
            this.productTable.Name = "productTable";
            this.productTable.RowCount = 1;
            this.productTable.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.productTable.Size = new System.Drawing.Size(537, 172);
            this.productTable.TabIndex = 4;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(2, 0);
            this.label12.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(0, 13);
            this.label12.TabIndex = 3;
            // 
            // addProductBtn
            // 
            this.addProductBtn.Location = new System.Drawing.Point(427, 83);
            this.addProductBtn.Name = "addProductBtn";
            this.addProductBtn.Size = new System.Drawing.Size(122, 24);
            this.addProductBtn.TabIndex = 0;
            this.addProductBtn.Text = "Product toevoegen";
            this.addProductBtn.UseVisualStyleBackColor = true;
            this.addProductBtn.Click += new System.EventHandler(this.addProductBtn_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 91);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(56, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Producten";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(31, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Klant";
            // 
            // customerLabel
            // 
            this.customerLabel.AutoSize = true;
            this.customerLabel.Location = new System.Drawing.Point(78, 9);
            this.customerLabel.Name = "customerLabel";
            this.customerLabel.Size = new System.Drawing.Size(97, 13);
            this.customerLabel.TabIndex = 7;
            this.customerLabel.Text = "Voeg een klant toe";
            // 
            // addCustomerBtn
            // 
            this.addCustomerBtn.Location = new System.Drawing.Point(427, 12);
            this.addCustomerBtn.Name = "addCustomerBtn";
            this.addCustomerBtn.Size = new System.Drawing.Size(121, 23);
            this.addCustomerBtn.TabIndex = 8;
            this.addCustomerBtn.Text = "Klant toevoegen";
            this.addCustomerBtn.UseVisualStyleBackColor = true;
            this.addCustomerBtn.Click += new System.EventHandler(this.addCustomerBtn_Click);
            // 
            // payBtn
            // 
            this.payBtn.Location = new System.Drawing.Point(673, 12);
            this.payBtn.Name = "payBtn";
            this.payBtn.Size = new System.Drawing.Size(75, 23);
            this.payBtn.TabIndex = 9;
            this.payBtn.Text = "Betaal";
            this.payBtn.UseVisualStyleBackColor = true;
            this.payBtn.Click += new System.EventHandler(this.payBtn_Click);
            // 
            // payTb
            // 
            this.payTb.Location = new System.Drawing.Point(584, 13);
            this.payTb.Name = "payTb";
            this.payTb.Size = new System.Drawing.Size(80, 20);
            this.payTb.TabIndex = 10;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(584, 53);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(101, 13);
            this.label3.TabIndex = 11;
            this.label3.Text = "Openstaand bedrag";
            // 
            // productTotalLabel
            // 
            this.productTotalLabel.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.productTotalLabel.AutoSize = true;
            this.productTotalLabel.Location = new System.Drawing.Point(222, 3);
            this.productTotalLabel.Name = "productTotalLabel";
            this.productTotalLabel.Size = new System.Drawing.Size(13, 13);
            this.productTotalLabel.TabIndex = 12;
            this.productTotalLabel.Text = "€";
            this.productTotalLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // productTotalDiscountLabel
            // 
            this.productTotalDiscountLabel.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.productTotalDiscountLabel.AutoSize = true;
            this.productTotalDiscountLabel.Location = new System.Drawing.Point(222, 43);
            this.productTotalDiscountLabel.Name = "productTotalDiscountLabel";
            this.productTotalDiscountLabel.Size = new System.Drawing.Size(13, 13);
            this.productTotalDiscountLabel.TabIndex = 13;
            this.productTotalDiscountLabel.Text = "€";
            this.productTotalDiscountLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // taxLabel
            // 
            this.taxLabel.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.taxLabel.AutoSize = true;
            this.taxLabel.Location = new System.Drawing.Point(208, 63);
            this.taxLabel.Name = "taxLabel";
            this.taxLabel.Size = new System.Drawing.Size(27, 13);
            this.taxLabel.TabIndex = 14;
            this.taxLabel.Text = "21%";
            this.taxLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // productTotalPriceLabel
            // 
            this.productTotalPriceLabel.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.productTotalPriceLabel.AutoSize = true;
            this.productTotalPriceLabel.Location = new System.Drawing.Point(222, 85);
            this.productTotalPriceLabel.Name = "productTotalPriceLabel";
            this.productTotalPriceLabel.Size = new System.Drawing.Size(13, 13);
            this.productTotalPriceLabel.TabIndex = 15;
            this.productTotalPriceLabel.Text = "€";
            this.productTotalPriceLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(3, 3);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(79, 13);
            this.label4.TabIndex = 17;
            this.label4.Text = "Som producten";
            // 
            // label5
            // 
            this.label5.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(3, 43);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(90, 13);
            this.label5.TabIndex = 18;
            this.label5.Text = "Totaal excl. BTW";
            // 
            // label6
            // 
            this.label6.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(3, 63);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(58, 13);
            this.label6.TabIndex = 19;
            this.label6.Text = "BTW tarief";
            // 
            // label7
            // 
            this.label7.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(3, 85);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(55, 13);
            this.label7.TabIndex = 20;
            this.label7.Text = "Totaalprijs";
            // 
            // label8
            // 
            this.label8.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(3, 23);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(62, 13);
            this.label8.TabIndex = 21;
            this.label8.Text = "Korting in %";
            // 
            // discountBtn
            // 
            this.discountBtn.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.discountBtn.Location = new System.Drawing.Point(175, 23);
            this.discountBtn.Name = "discountBtn";
            this.discountBtn.Size = new System.Drawing.Size(60, 20);
            this.discountBtn.TabIndex = 22;
            this.discountBtn.ValueChanged += new System.EventHandler(this.discountBtn_ValueChanged);
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Controls.Add(this.label4, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.label8, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.productTotalPriceLabel, 1, 4);
            this.tableLayoutPanel2.Controls.Add(this.discountBtn, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.taxLabel, 1, 3);
            this.tableLayoutPanel2.Controls.Add(this.label5, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.productTotalDiscountLabel, 1, 2);
            this.tableLayoutPanel2.Controls.Add(this.label7, 0, 4);
            this.tableLayoutPanel2.Controls.Add(this.label6, 0, 3);
            this.tableLayoutPanel2.Controls.Add(this.productTotalLabel, 1, 0);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(310, 299);
            this.tableLayoutPanel2.Margin = new System.Windows.Forms.Padding(2);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 5;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(238, 104);
            this.tableLayoutPanel2.TabIndex = 24;
            // 
            // offerteCB
            // 
            this.offerteCB.AutoSize = true;
            this.offerteCB.Location = new System.Drawing.Point(10, 302);
            this.offerteCB.Name = "offerteCB";
            this.offerteCB.Size = new System.Drawing.Size(56, 17);
            this.offerteCB.TabIndex = 25;
            this.offerteCB.Text = "offerte";
            this.offerteCB.UseVisualStyleBackColor = true;
            this.offerteCB.CheckedChanged += new System.EventHandler(this.offerteCB_CheckedChanged);
            // 
            // weigerOfferteBtn
            // 
            this.weigerOfferteBtn.Location = new System.Drawing.Point(617, 380);
            this.weigerOfferteBtn.Name = "weigerOfferteBtn";
            this.weigerOfferteBtn.Size = new System.Drawing.Size(131, 23);
            this.weigerOfferteBtn.TabIndex = 26;
            this.weigerOfferteBtn.Text = "Offerte weigeren";
            this.weigerOfferteBtn.UseVisualStyleBackColor = true;
            this.weigerOfferteBtn.Click += new System.EventHandler(this.weigerOfferteBtn_Click);
            // 
            // BestellenForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(760, 444);
            this.Controls.Add(this.weigerOfferteBtn);
            this.Controls.Add(this.offerteCB);
            this.Controls.Add(this.tableLayoutPanel2);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.payTb);
            this.Controls.Add(this.payBtn);
            this.Controls.Add(this.addCustomerBtn);
            this.Controls.Add(this.customerLabel);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.addProductBtn);
            this.Controls.Add(this.productTable);
            this.Controls.Add(this.placeOrderBtn);
            this.Controls.Add(this.disapproveOrderBtn);
            this.Controls.Add(this.approveOrderBtn);
            this.Controls.Add(this.createOrderBtn);
            this.Name = "BestellenForm";
            this.Text = "Order";
            this.Load += new System.EventHandler(this.BestellenForm_Load);
            this.productTable.ResumeLayout(false);
            this.productTable.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.discountBtn)).EndInit();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button createOrderBtn;
        private System.Windows.Forms.Button approveOrderBtn;
        private System.Windows.Forms.Button disapproveOrderBtn;
        private System.Windows.Forms.Button placeOrderBtn;
        private System.Windows.Forms.TableLayoutPanel productTable;
        private System.Windows.Forms.Button addProductBtn;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label customerLabel;
        private System.Windows.Forms.Button addCustomerBtn;
        private System.Windows.Forms.Button payBtn;
        private System.Windows.Forms.FontDialog fontDialog1;
        private System.Windows.Forms.TextBox payTb;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label productTotalLabel;
        private System.Windows.Forms.Label productTotalDiscountLabel;
        private System.Windows.Forms.Label taxLabel;
        private System.Windows.Forms.Label productTotalPriceLabel;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.NumericUpDown discountBtn;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.CheckBox offerteCB;
        private System.Windows.Forms.Button weigerOfferteBtn;
    }
}
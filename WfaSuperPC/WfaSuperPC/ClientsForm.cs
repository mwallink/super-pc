﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace WfaSuperPC
{
    public partial class ClientsForm : Form
    {
        SuperPcContainer context = new SuperPcContainer();
        public ClientEditForm clientEditForm;
        private BestellenForm parentForm;


        public ClientsForm()
        {
            InitializeComponent();
        }

        // Overloaderconstructor als vanuit bestelling geopend wordt.
        public ClientsForm(BestellenForm parentForm)
        {
            this.parentForm = parentForm;
            InitializeComponent();
        }



        private void addBtn_Click(object sender, EventArgs e)
        {
            this.clientEditForm = new ClientEditForm(this);
            this.clientEditForm.ShowDialog(this);
        }

        private void clientsForm_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'klantenDataSet.Klanten' table. You can move, or remove it, as needed.
            this.klantenTableAdapter.Fill(this.klantenDataSet.Klanten);

        }


        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {

            int rowIndex = e.RowIndex;
            int klantId = Convert.ToInt32(dataGridView1.Rows[rowIndex].Cells[0].Value);

            if (parentForm != null)
            {
                Klanten toegevoegdeKlant = parentForm.AddCustomer(klantId);
                MessageBox.Show(toegevoegdeKlant.Voornaam + " " + toegevoegdeKlant.Achternaam + " met ID: " +
                                toegevoegdeKlant.Id.ToString() + " is toegevoegd.");
            }
            else
            {
                this.clientEditForm = new ClientEditForm(this, klantId);
                this.clientEditForm.ShowDialog(this);
            }

        }

    }



}

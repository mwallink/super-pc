﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WfaSuperPC
{
    public partial class AfbeeldingenForm : Form
    {
        ProductForm PF = new ProductForm();
        private string _fileName = null;
        private string _newFileName = null;
        private string _sourcePath = null;
        private string _targetPath = @"D:\VSWorkspace\super-pc\WfaSuperPC\productafbeeldingen";
        public int[] AfbeeldingIds = new int[3];
        public int picturesNum = 3;

        SuperPcContainer context = new SuperPcContainer();

        
        // Constructor als afbeeldingen form vanuit product wordt geopend.
        public AfbeeldingenForm(ProductForm PF)
        {
            this.PF = PF;

            for (int i = 0; i < picturesNum; i++)
            {
                AfbeeldingIds[i] = -1;
            }


            InitializeComponent();
            //Afbeeldingen uit bieb te halen en in programma zetten.
            ShowBieb();

        }

        private void ShowBieb()
        {
            var query = (from c in context.Afbeeldingen select c);
            var bieb = query.ToList();
            foreach (var afbeelding in bieb)
            {
                AddToProgramList(afbeelding.Id, afbeelding.url);
            }
        }




        private void AfbeeldingenForm_Load(object sender, EventArgs e)
        {

        }

        private void kiezenBtn_Click(object sender, EventArgs e)
        {
            // open file dialog 
            OpenFileDialog open = new OpenFileDialog(); 
            
            // image filters
            open.Filter = "Image Files(*.jpg; *.jpeg; *.gif; *.bmp)|*.jpg; *.jpeg; *.gif; *.bmp"; 
            if (open.ShowDialog() == DialogResult.OK)
            {
                // display image in picture box
                pictureBox1.Image = new Bitmap(open.FileName);

                this._fileName = open.SafeFileName;
                this._sourcePath = Regex.Replace(open.FileName, _fileName, "");

                // image file path
                textBox1.Text = _fileName;
            } 
        }

        private void button1_Click(object sender, EventArgs e)
        {
            /*
             * Afbeelding kopieren naar programmalocatie:
             * D:\VSWorkspace\super-pc\WfaSuperPC\productafbeeldingen
             */
            string sourceFile = null;

            _newFileName = textBox1.Text;

            try
            {
                // Use Path class to manipulate file and directory paths. 
                sourceFile = System.IO.Path.Combine(_sourcePath, _fileName);

                try
                {
                    string destFile = System.IO.Path.Combine(_targetPath, _newFileName);

                    // Als de map niet bestaad, dan nieuwe map aanmaken
                    if (!System.IO.Directory.Exists(_targetPath))
                    {
                        System.IO.Directory.CreateDirectory(_targetPath);
                    }

                    // To copy a file to another location and  
                    // overwrite the destination file if it already exists.
                    System.IO.File.Copy(sourceFile, destFile, false);


                    try
                    {

                        //Afbeelding toevoegen aan database
                        //afbeelding = new Afbeeldingen();
                        Afbeeldingen afbeelding = context.Afbeeldingen.Create();
                        afbeelding.url = _newFileName;

                        // Data opslaan
                        var afbeeldingen = context.Set<Afbeeldingen>();
                        afbeeldingen.Add(afbeelding);

                        int countSavedObjects = context.SaveChanges();
                        
                        //Afbeelding toevoegen aan afbeeldingenlijst
                        AddToProgramList(afbeelding.Id, _newFileName);
                    }
                    catch
                    {
                       MessageBox.Show("Er is iets fout gegaan met het opslaan.");
                    }

                }
                catch
                {
                    MessageBox.Show("Een bestand met deze naam bestaad al. Wijzig a.u.b. de naam om dit bestand toe te voegen.");
                }

            }
            catch
            {
                MessageBox.Show("Er is geen afbeelding geselecteerd.");

            }

        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            // Afbeelding toevoegen aan product
            int rowIndex = e.RowIndex;
            int id = Convert.ToInt32(dataGridView1.Rows[rowIndex].Cells[0].Value);
            string naam = (string) dataGridView1.Rows[rowIndex].Cells[2].Value;
            AddToProductList(id, naam);

            // Afbeelding uit programmalijst halen
        }

        // Afbeelding uit productlijst halen.
        // En toevoegen aan programmalijst.
        private void dataGridViewProduct_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int rowIndex = e.RowIndex;
            int Id = Convert.ToInt32(dataGridViewProduct.Rows[rowIndex].Cells[0].Value);
            dataGridViewProduct.Rows.Remove(dataGridViewProduct.Rows[rowIndex]);


            //TODO: methode aanroepen die afbeeldingen uit de productForm table haalt.

            for (int i = 0; i < picturesNum; i++)
            {
                if (AfbeeldingIds[i] == Id) AfbeeldingIds[i] = -1;
            }
        }

        //Afbeelding toevoegen aan afbeeldingenlijst
        private void AddToProgramList(int id, string bestandsNaam)
        {

            MessageBox.Show(id + " " + bestandsNaam);
            try
            {
                dataGridView1.Rows.Add(id, Image.FromFile(_targetPath + "\\" + bestandsNaam), bestandsNaam);
            }
            catch
            {
                // Afbeedling bestaad niet meer.
            }
            
        }

        // Afbeelding toevoegen aan lijst met productafbeeldingen
        private void AddToProductList(int id, string bestandsNaam)
        {

            bool exists = false;
            int empty = picturesNum + 1;

            for (int i = 0; i < picturesNum; i++)
            {
                if (AfbeeldingIds[i] == id) exists = true;
                if (AfbeeldingIds[i] < 0) empty = i;
            }

            if (exists)
            {
                MessageBox.Show("Deze afbeelding staat al in de lijst met productafbeeldingen.");
            }
            else
            {
                if (empty > picturesNum)
                {
                    MessageBox.Show("Het maximale is berijkt. Verwijder eerst een afbeelding.");
                }
                else
                {
                    dataGridViewProduct.Rows.Add(id, Image.FromFile(_targetPath + "\\" + bestandsNaam), bestandsNaam);
                    PF.AddImageRow(id, Image.FromFile(_targetPath + "\\" + bestandsNaam), bestandsNaam);
                    AfbeeldingIds[empty] = id;
                }
            }

            
        }

    }
}

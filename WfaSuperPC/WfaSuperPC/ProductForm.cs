﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WfaSuperPC
{
    public partial class ProductForm : Form
    {
        private Producten Product = new Producten();
        private int ProductId { get; set; }
        private String Bericht;
        private bool FieldError = false;

        SuperPcContainer context = new SuperPcContainer();

        // Constructor voor nieuw product.
        public ProductForm()
        {
            Product = context.Producten.Create();
            ProductId = Product.Id;
            InitializeComponent();
        }

        // Overloader constructor als een bestaand product geopend wordt.
        public ProductForm(int id)
        {
            Product = context.Producten.Find(id);
            ProductId = id;
            InitializeComponent();
        }

        private void AfbeeldingBtn_Click(object sender, EventArgs e)
        {
            AfbeeldingenForm AF = new AfbeeldingenForm(this);
            AF.ShowDialog();
        }

        

        private void ProductForm_Load(object sender, EventArgs e)
        {
            // Als er een bestaand product is
            if (this.ProductId > 0)
            {
                NaamTb.Text = Product.Naam;
                PrijsTb.Text = Product.Prijs.ToString();

                // Omschrijving nog opslaan!!!
                // OmschrijvingTb.Text = Product.Omschrijving.te
            }
        }

        private void OpslaanBtn_Click(object sender, EventArgs e)
        {
            // Waarden in product zetten
            // Naam is verplicht.
            if (NaamTb.Text != "")
            {
                this.Product.Naam = NaamTb.Text;
                NaamLabel.ForeColor = Color.Black;
                FieldError = false;
            }
            else
            {
                FieldError = true;
                NaamLabel.ForeColor = Color.Red;
            }

            // Prijs is ook verplicht.
            try
            {
                this.Product.Prijs = Convert.ToInt32(PrijsTb.Text);
                PrijsLabel.ForeColor = Color.Black;
                FieldError = false;
            }
            catch
            {
                FieldError = true;
                PrijsLabel.ForeColor = Color.Red;
            }

            // Omschrijving is niet verplicht.
            Product.Omschrijving = OmschrijvingTb.Text;

            if (!FieldError)
            {

                try
                {
                    // Als er een bestaand product is
                    if (this.ProductId > 0)
                    {
                        Bericht = NaamTb.Text + " is gewijzigd.";
                    }
                    else
                    {
                        var Prod = context.Set<Producten>();
                        Prod.Add(this.Product);
                        Bericht = NaamTb.Text + " is toegevoegd.";
                    }
                    // Wijzigingen opslaan.
                    context.SaveChanges();
                }
                catch
                {
                    Bericht = "Er is iets fout gegaan...";
                }

                this.Hide();

                MessageBox.Show(Bericht);

                // Dit scherm sluiten.
                this.Close();
            }
            else
            {
                MessageBox.Show("Vul alle velden correct in a.u.b.");
            }


        }

        private void AnnulerenBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        public void AddImageRow(int id, Image img, String bestandsNaam)
        {
            dataGridViewProduct.Rows.Add(id, img, bestandsNaam);
        }

    }
}

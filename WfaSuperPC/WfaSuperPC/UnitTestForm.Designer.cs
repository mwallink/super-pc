﻿namespace WfaSuperPC
{
    partial class UnitTestForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnDeleteSetNull = new System.Windows.Forms.Button();
            this.btnPartialClassExecute = new System.Windows.Forms.Button();
            this.btnLinqFetchLimit = new System.Windows.Forms.Button();
            this.btnInlogForm = new System.Windows.Forms.Button();
            this.btnAdoSelect = new System.Windows.Forms.Button();
            this.btnAdoInsert = new System.Windows.Forms.Button();
            this.btnAuthorizedForm = new System.Windows.Forms.Button();
            this.btnListForms = new System.Windows.Forms.Button();
            this.lblDbFormControlCount = new System.Windows.Forms.Label();
            this.btnStatistieken = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnDeleteSetNull
            // 
            this.btnDeleteSetNull.Location = new System.Drawing.Point(13, 13);
            this.btnDeleteSetNull.Name = "btnDeleteSetNull";
            this.btnDeleteSetNull.Size = new System.Drawing.Size(257, 32);
            this.btnDeleteSetNull.TabIndex = 0;
            this.btnDeleteSetNull.Text = "On Delete set null";
            this.btnDeleteSetNull.UseVisualStyleBackColor = true;
            this.btnDeleteSetNull.Click += new System.EventHandler(this.btnDeleteSetNull_Click);
            // 
            // btnPartialClassExecute
            // 
            this.btnPartialClassExecute.Location = new System.Drawing.Point(13, 52);
            this.btnPartialClassExecute.Name = "btnPartialClassExecute";
            this.btnPartialClassExecute.Size = new System.Drawing.Size(257, 32);
            this.btnPartialClassExecute.TabIndex = 1;
            this.btnPartialClassExecute.Text = "Execute method partial class";
            this.btnPartialClassExecute.UseVisualStyleBackColor = true;
            this.btnPartialClassExecute.Click += new System.EventHandler(this.btnPartialClassExecute_Click);
            // 
            // btnLinqFetchLimit
            // 
            this.btnLinqFetchLimit.Location = new System.Drawing.Point(13, 91);
            this.btnLinqFetchLimit.Name = "btnLinqFetchLimit";
            this.btnLinqFetchLimit.Size = new System.Drawing.Size(257, 33);
            this.btnLinqFetchLimit.TabIndex = 2;
            this.btnLinqFetchLimit.Text = "Linq Fetch Limit";
            this.btnLinqFetchLimit.UseVisualStyleBackColor = true;
            this.btnLinqFetchLimit.Click += new System.EventHandler(this.btnLinqFetchLimit_Click);
            // 
            // btnInlogForm
            // 
            this.btnInlogForm.Location = new System.Drawing.Point(13, 131);
            this.btnInlogForm.Name = "btnInlogForm";
            this.btnInlogForm.Size = new System.Drawing.Size(257, 34);
            this.btnInlogForm.TabIndex = 3;
            this.btnInlogForm.Text = "InlogForm";
            this.btnInlogForm.UseVisualStyleBackColor = true;
            this.btnInlogForm.Click += new System.EventHandler(this.btnInlogForm_Click);
            // 
            // btnAdoSelect
            // 
            this.btnAdoSelect.Location = new System.Drawing.Point(13, 172);
            this.btnAdoSelect.Name = "btnAdoSelect";
            this.btnAdoSelect.Size = new System.Drawing.Size(257, 36);
            this.btnAdoSelect.TabIndex = 4;
            this.btnAdoSelect.Text = "&ADO.NET Select";
            this.btnAdoSelect.UseVisualStyleBackColor = true;
            this.btnAdoSelect.Click += new System.EventHandler(this.btnAdoSelect_Click);
            // 
            // btnAdoInsert
            // 
            this.btnAdoInsert.Location = new System.Drawing.Point(13, 215);
            this.btnAdoInsert.Name = "btnAdoInsert";
            this.btnAdoInsert.Size = new System.Drawing.Size(257, 35);
            this.btnAdoInsert.TabIndex = 5;
            this.btnAdoInsert.Text = "ADO.NET Insert";
            this.btnAdoInsert.UseVisualStyleBackColor = true;
            this.btnAdoInsert.Click += new System.EventHandler(this.btnAdoInsert_Click);
            // 
            // btnAuthorizedForm
            // 
            this.btnAuthorizedForm.Location = new System.Drawing.Point(13, 257);
            this.btnAuthorizedForm.Name = "btnAuthorizedForm";
            this.btnAuthorizedForm.Size = new System.Drawing.Size(257, 35);
            this.btnAuthorizedForm.TabIndex = 6;
            this.btnAuthorizedForm.Text = "AuthorizedForm";
            this.btnAuthorizedForm.UseVisualStyleBackColor = true;
            this.btnAuthorizedForm.Click += new System.EventHandler(this.btnAuthorizedForm_Click);
            // 
            // btnListForms
            // 
            this.btnListForms.Location = new System.Drawing.Point(13, 299);
            this.btnListForms.Name = "btnListForms";
            this.btnListForms.Size = new System.Drawing.Size(257, 33);
            this.btnListForms.TabIndex = 7;
            this.btnListForms.Text = "List Forms";
            this.btnListForms.UseVisualStyleBackColor = true;
            this.btnListForms.Click += new System.EventHandler(this.btnListForms_Click);
            // 
            // lblDbFormControlCount
            // 
            this.lblDbFormControlCount.AutoSize = true;
            this.lblDbFormControlCount.Location = new System.Drawing.Point(416, 13);
            this.lblDbFormControlCount.Name = "lblDbFormControlCount";
            this.lblDbFormControlCount.Size = new System.Drawing.Size(114, 17);
            this.lblDbFormControlCount.TabIndex = 8;
            this.lblDbFormControlCount.Text = "formcontrolcount";
            // 
            // btnStatistieken
            // 
            this.btnStatistieken.Location = new System.Drawing.Point(296, 299);
            this.btnStatistieken.Name = "btnStatistieken";
            this.btnStatistieken.Size = new System.Drawing.Size(253, 33);
            this.btnStatistieken.TabIndex = 9;
            this.btnStatistieken.Text = "Statistieken";
            this.btnStatistieken.UseVisualStyleBackColor = true;
            this.btnStatistieken.Click += new System.EventHandler(this.btnStatistieken_Click);
            // 
            // UnitTestForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(744, 354);
            this.Controls.Add(this.btnStatistieken);
            this.Controls.Add(this.lblDbFormControlCount);
            this.Controls.Add(this.btnListForms);
            this.Controls.Add(this.btnAuthorizedForm);
            this.Controls.Add(this.btnAdoInsert);
            this.Controls.Add(this.btnAdoSelect);
            this.Controls.Add(this.btnInlogForm);
            this.Controls.Add(this.btnLinqFetchLimit);
            this.Controls.Add(this.btnPartialClassExecute);
            this.Controls.Add(this.btnDeleteSetNull);
            this.Name = "UnitTestForm";
            this.Text = "UnitTest";
            this.Load += new System.EventHandler(this.UnitTest_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnDeleteSetNull;
        private System.Windows.Forms.Button btnPartialClassExecute;
        private System.Windows.Forms.Button btnLinqFetchLimit;
        private System.Windows.Forms.Button btnInlogForm;
        private System.Windows.Forms.Button btnAdoSelect;
        private System.Windows.Forms.Button btnAdoInsert;
        private System.Windows.Forms.Button btnAuthorizedForm;
        private System.Windows.Forms.Button btnListForms;
        private System.Windows.Forms.Label lblDbFormControlCount;
        private System.Windows.Forms.Button btnStatistieken;
    }
}
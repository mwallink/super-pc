﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Transactions;

namespace WfaSuperPC
{
    public partial class AssemblageForm : AuthorizedForm
    {
        private bool bestelregelspopulating = false;

        public AssemblageForm()
        {
            InitializeComponent();
        }

        public AssemblageForm(int persoonid)
        {
            this.PersoonId = persoonid;
            InitializeComponent();
            PopulateBestellingenListView();
        }

        public void AssemblageForm_Load(object sender, EventArgs e)
        {

        }

        private void PopulateBestellingenListView()
        {
            this.lvBestellingenOnvoltooid.Items.Clear();

            SuperPcContainer context = new SuperPcContainer();
            var bestellingen = 
                
                context.Bestellingen
                .Where(b => b.Bestelregels.Any(br => br.AssemblageVoltooid == 0 && br.InBehandeling == 0))
                .Select(b => new{
                    Id = b.Id,
                    Gecreeerd = b.AanmaakDatum,
                    nBestelregels = b.Bestelregels.Count()
                });

            this.lvBestellingenOnvoltooid.BeginUpdate();
            foreach (var bestelling in bestellingen)
            {
                ListViewItem lvi = new ListViewItem(
                    new string[]{ bestelling.Id.ToString(), bestelling.Gecreeerd.ToString(), bestelling.nBestelregels.ToString()});
                this.lvBestellingenOnvoltooid.Items.Add(lvi);
            }
            this.lvBestellingenOnvoltooid.EndUpdate();
        }

        private void PopulateBestelRegelsListView()
        {
            this.bestelregelspopulating = true;
            this.lvBestelRegels.Items.Clear();
            this.ReleaseAllBestelregelsInBehandeling();

            //check focus op item => dit event wordt twee keer afgevuurd, een keer zonder focuseditem en een keer met focuseditem
            if (this.lvBestellingenOnvoltooid.SelectedIndices.Count == 0)
                return;
            
            SuperPcContainer context = new SuperPcContainer();
            
            //get bestelling id
            int currentBestellingenId = Convert.ToInt32(lvBestellingenOnvoltooid.FocusedItem.SubItems[0].Text.ToString());

            var bestelregels =
                context.Bestelregels
                .Where(b => b.BestellingenId == currentBestellingenId)
                .Select(b => b);

            this.lvBestelRegels.BeginUpdate();
            foreach (var bestelregel in bestelregels)
            {
                ListViewItem lvi = new ListViewItem(
                    new string[] {
                        " ",
                        bestelregel.Id.ToString(), 
                        bestelregel.BestellingenId.ToString(),
                        bestelregel.Producten.Id.ToString(),
                        bestelregel.Producten.Naam.ToString(),
                        bestelregel.Aantal.ToString(),
                        bestelregel.Producten.Voorraad.ToString()
                    });
                lvi.Checked = bestelregel.AssemblageVoltooid != 0 ? true : false;
                this.lvBestelRegels.Items.Add(lvi);
            }
            this.lvBestelRegels.EndUpdate();
            this.bestelregelspopulating = false;

        }

        private void lvBestellingenOnvoltooid_SelectedIndexChanged(object sender, EventArgs e)
        {
            //ReleaseBestelregelInBehandeling() => wordt geregeld in de populate functie
            PopulateBestelRegelsListView();
        }


        private void lvBestelRegels_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.logEventFiring("lvBestelRegels", "SelectedIndexChanged", "fired");
            SuperPcContainer context = new SuperPcContainer();
        }

        private void ReleaseAllBestelregelsInBehandeling()
        {
            SuperPcContainer context = new SuperPcContainer();
            
                var br = context.Bestelregels.Where(b => b.InBehandeling == this.PersoonId).Select(b => b);
                foreach(Bestelregels b in br){
                b.InBehandeling = 0;
                }
                context.SaveChanges();
        }

        private void ReleaseBestelregelInBehandeling(int bestelregelid)
        {
            SuperPcContainer context = new SuperPcContainer();

            var br = (context.Bestelregels.Where(b => b.Id == bestelregelid).Select(b => b)).Single();
            br.InBehandeling = 0;
            context.SaveChanges();
        }

        private bool LockBestelregelInBehandeling(int currentBestelRegelId)
        {
            
            SuperPcContainer context = new SuperPcContainer();

            int alreadyLocked = Convert.ToInt32(context.Bestelregels.Where(b => b.Id == currentBestelRegelId).Select(b => b.InBehandeling).Single());
            if (alreadyLocked == this.PersoonId)
            {
                return true;
            }
            
            if (alreadyLocked != 0)
            {
                MessageBox.Show("Deze bestelregel is reeds door een andere medewerker in behandeling genomen. U kunt de bestelregel niet in behandeling nemen.");
                return false;
            }
            else
            {
                Bestelregels b = (from br in context.Bestelregels where br.Id == currentBestelRegelId select br).Single();
                b.InBehandeling = this.PersoonId;
                context.SaveChanges();
                return true;
            }
             
        }
        
        private void frmAssemblage_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.ReleaseAllBestelregelsInBehandeling();
        }

        private void frmAssemblage_Deactivate(object sender, EventArgs e)
        {
            //this.ReleaseAllBestelregelsInBehandeling();
            //MessageBox.Show("Als er een bestelregel in behandeling was, wordt die nu vrijgegeven voor behandeling.");
        }

        private void lvBestelRegels_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            if (this.bestelregelspopulating == true)
                return;

            string focuseditem = this.lvBestelRegels.FocusedItem == null ? "null" : this.lvBestelRegels.FocusedItem.Index.ToString();
            this.logEventFiring("lvBestelRegels", "ItemCheck", "index: " + e.Index.ToString() +
                " huidige waarde: " + e.CurrentValue.ToString() + " nieuwe waarde: " +
                e.NewValue.ToString() + " focused item index:" + focuseditem);
            
            //resultaat => 0 als niet voltooid, anders persoonsid
            int checkboxresult = e.NewValue == 0 ? 0 : this.PersoonId;
            
            //get bestelregel van id
            int bestelregelid = Convert.ToInt32(lvBestelRegels.Items[e.Index].SubItems[1].Text.ToString());
            SuperPcContainer context = new SuperPcContainer();
            Bestelregels b = context.Bestelregels.Where(br => br.Id == bestelregelid).Select(br => br).Single();
            
            //check in behandeling door een andere user
            if (b.InBehandeling != 0 && b.InBehandeling != this.PersoonId)
            {
                //e.NewValue = e.CurrentValue;
                MessageBox.Show("Bestelregel is al in behandeling door een andere gebruiker.");
                return;
            }

            //probeer een lock te verkrijgen op de database
            if (this.LockBestelregelInBehandeling(bestelregelid) == true)
            {
                b.InBehandeling = this.PersoonId;
                context.SaveChanges();

                int checkboxstate = e.CurrentValue == CheckState.Checked ? 1 : 0;
                //check dbvalue en checkbox gelijk
                if ((b.AssemblageVoltooid == 0 && checkboxstate != 0) || (b.AssemblageVoltooid != 0 && checkboxstate == 0))
                {
                    MessageBox.Show("De waarde is intussen door een medewerker gewijzigd. De waarde is nu weer gesynchroniseerd. U kunt een wijziging aanbrengen.");                //      => lock nee=> melding => populate met release
                }
                else
                {
                    //checkboxresult is de nieuwe waarde
                    if (checkboxresult == 0)
                    {
                        this.TransactieAnnuleer(b.Id);
                    }
                    else
                    {
                        this.TransactieBestel(bestelregelid, e);
                    }
                   
                }
            }
            else
            {
                MessageBox.Show("Bestelregel al in behandeling door een andere gebruiker.");
            }
            this.PopulateBestelRegelsListView();

            //check user heeft hem niet in behandeling && bestelregel is al voltooid
            //if (b.InBehandeling != this.PersoonId && b.AssemblageVoltooid != 0 && b.AssemblageVoltooid != this.PersoonId)
           // {
            //    if (e.CurrentValue != e.NewValue)
           //     {
                  //  MessageBox.Show("Deze bestelregel is al voltooid door een andere gebruiker.");
           //     }
           //     return;
           // }

        }

        private void btnClearEventLog_Click(object sender, EventArgs e)
        {
            string focuseditem = this.lvBestelRegels.FocusedItem == null ? "null" : this.lvBestelRegels.FocusedItem.Index.ToString();
            this.tbEventLog.Text = "current bestelregel focus: " + focuseditem + "\r\n";
        }

        private void logEventFiring(string controlname, string eventname, string message)
        {
        
        string focusid = lvBestelRegels.FocusedItem == null ? "null" : lvBestelRegels.FocusedItem.SubItems[1].Text.ToString();
        string line = "Control: " + controlname + "\t Event " + eventname + "\t Focusid " + focusid + "\t Bericht: " + message + "\r\n";

        string indices = "indices: \t";
        
        var ind = lvBestelRegels.SelectedIndices;
            foreach(var i in ind){
                indices = indices + i.ToString();
            }
       indices = indices + "\r\n";


       string items = "items: \t";
            foreach(ListViewItem lvi in lvBestelRegels.SelectedItems){
                foreach(var j in lvi.SubItems){
                    items = items + j.ToString();
                }
            }
            items = items + "\r\n"; 
    
        this.tbEventLog.Text = this.tbEventLog.Text + line + indices + items;
        }

        private void lvBestelRegels_ItemSelectionChanged(object sender, ListViewItemSelectionChangedEventArgs e)
        {
            this.logEventFiring("lvBestelRegels", "ItemSelectionChanged", "fired");
        }

        private void lvBestelRegels_KeyDown(object sender, KeyEventArgs e)
        {
            this.logEventFiring("lvBestelRegels", "KeyDown", "fired");
        }

        private void lvBestelRegels_MouseClick(object sender, MouseEventArgs e)
        {
            this.logEventFiring("lvBestelRegels", "MouseClick", "fired");

            //release bestelregel die op behandeling staat
            ReleaseAllBestelregelsInBehandeling();

            ListViewItem theClickedOne = this.lvBestelRegels.GetItemAt(e.X, e.Y);

            int currentBestelRegelId;

            if (theClickedOne != null)
            {
                currentBestelRegelId = Convert.ToInt32(lvBestelRegels.Items[theClickedOne.Index].SubItems[1].Text.ToString());
                if (LockBestelregelInBehandeling(currentBestelRegelId) == false)
                {
                    this.PopulateBestelRegelsListView();
                }
            }
        }

        private void TransactieBestel(int bestelregelid, ItemCheckEventArgs e)
        {
            //transactie => 
            using (SuperPcContainer ctx = new SuperPcContainer())
            {
                using (var scope = new TransactionScope())
                {
                    bool result = false;
                    Bestelregels b = ctx.Bestelregels.Where(br => br.Id == bestelregelid).Single();
                    Producten p = ctx.Producten.Where(pr => pr.Id == b.ProductenId).Single();
                   
                    //check op voorraad
                    if ((p.Voorraad - b.Aantal) >= 0)
                    {
                        result = true;
                        // voorraad afboeken
                        p.Voorraad = p.Voorraad - b.Aantal;

                        //bestelregel voltooid
                        b.AssemblageVoltooid = this.PersoonId;
                    }
                    
                    if (result == true)
                    {
                        ctx.SaveChanges();
                        scope.Complete();
                    }else{
                        scope.Dispose();
                        e.NewValue = e.CurrentValue;

                        MessageBox.Show("Er is onvoldoende voorraad, er is een automatische bestelling uitgegaan.");
                        p.Voorraad = p.Voorraad + 10;
                        ctx.SaveChanges();
                    }
                }
                ctx.Dispose();
            }
            
        }

        private void TransactieAnnuleer(int bestelregelid)
        {
            //transactie => 
            using (SuperPcContainer ctx = new SuperPcContainer())
            {
                using (var scope = new TransactionScope())
                {
                    Bestelregels b = ctx.Bestelregels.Where(br => br.Id == bestelregelid).Single();
                    Producten p = ctx.Producten.Where(pr => pr.Id == b.ProductenId).Single();

                    //verhoog voorraad
                    p.Voorraad = p.Voorraad + b.Aantal;
                   
                    //bestelregel voltooid
                    b.AssemblageVoltooid = 0;

                    //waarde opslaan
                    //b.AssemblageVoltooid = this.PersoonId;
                    ctx.SaveChanges();
                    scope.Complete();
                }
                ctx.Dispose();
            }
        }

        private void BestelProduct(Producten p)
        {

        }
    }
}

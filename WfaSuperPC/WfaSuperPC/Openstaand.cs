﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WfaSuperPC
{
    public partial class Openstaand : Form
    {
        private int persoonId;
        SuperPcContainer context = new SuperPcContainer();

        public Openstaand(int persoonid)
        {

            InitializeComponent();
            FillGrid();
            this.persoonId = persoonid;


        }

        public void FillGrid()
        {
            for (int i = 0; i < dataGridView1.Rows.Count - 1; i++)
            {
                dataGridView1.Rows.RemoveAt(i);
                i--;
                while (dataGridView1.Rows.Count == 0)
                    continue;
            }

            var bestellingen =
               from b in context.Bestellingen
               where b.Statussen.Naam == "aangemaakt" ||

                    b.Statussen.Naam == "managerGoedkeuring" ||
                    b.Statussen.Naam == "managerAfkeuring" ||
                    b.Statussen.Naam == "klantGoedkeuring" ||
                    b.Statussen.Naam == "klantAfkeuring"

               select new
               {

                   bestellingId = b.Id,
                   status = b.Statussen.Naam,
                   klant = b.Klanten.Voornaam + " " + b.Klanten.Achternaam
               };

            foreach (var b in bestellingen)
            {
                this.dataGridView1.Rows.Add(b.bestellingId.ToString(), b.status, b.klant);
            }
        }

        private void Openstaand_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'bestellingenDataSet.Bestellingen' table. You can move, or remove it, as needed.
            this.bestellingenTableAdapter.Fill(this.bestellingenDataSet.Bestellingen);

        }
        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int rowIndex = e.RowIndex;
            int id = Convert.ToInt32(dataGridView1.Rows[rowIndex].Cells[0].Value);
            BestellenForm bf = new BestellenForm(this.persoonId, id);
            bf.ShowDialog();
            this.FillGrid();
        }
    }
}

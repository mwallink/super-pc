﻿namespace WfaSuperPC
{
    partial class ProductenForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.naamDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.prijsDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.omschrijvingDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.voorraadDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.leveranciersIdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.productenBindingSource2 = new System.Windows.Forms.BindingSource(this.components);
            this.productenDataSet = new WfaSuperPC.ProductenDataSet();
            this.productenBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.ToevoegenBtn = new System.Windows.Forms.Button();
            this.productenBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.productenTableAdapter = new WfaSuperPC.ProductenDataSetTableAdapters.ProductenTableAdapter();
            this.productenDataSetBindingSource = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.productenBindingSource2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.productenDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.productenBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.productenBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.productenDataSetBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.naamDataGridViewTextBoxColumn,
            this.prijsDataGridViewTextBoxColumn,
            this.omschrijvingDataGridViewTextBoxColumn,
            this.voorraadDataGridViewTextBoxColumn,
            this.leveranciersIdDataGridViewTextBoxColumn});
            this.dataGridView1.DataSource = this.productenBindingSource2;
            this.dataGridView1.Location = new System.Drawing.Point(0, 0);
            this.dataGridView1.Margin = new System.Windows.Forms.Padding(2);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowTemplate.Height = 24;
            this.dataGridView1.Size = new System.Drawing.Size(731, 351);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellClick);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "Id";
            this.dataGridViewTextBoxColumn1.HeaderText = "Id";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // naamDataGridViewTextBoxColumn
            // 
            this.naamDataGridViewTextBoxColumn.DataPropertyName = "Naam";
            this.naamDataGridViewTextBoxColumn.HeaderText = "Naam";
            this.naamDataGridViewTextBoxColumn.Name = "naamDataGridViewTextBoxColumn";
            // 
            // prijsDataGridViewTextBoxColumn
            // 
            this.prijsDataGridViewTextBoxColumn.DataPropertyName = "Prijs";
            this.prijsDataGridViewTextBoxColumn.HeaderText = "Prijs";
            this.prijsDataGridViewTextBoxColumn.Name = "prijsDataGridViewTextBoxColumn";
            // 
            // omschrijvingDataGridViewTextBoxColumn
            // 
            this.omschrijvingDataGridViewTextBoxColumn.DataPropertyName = "Omschrijving";
            this.omschrijvingDataGridViewTextBoxColumn.HeaderText = "Omschrijving";
            this.omschrijvingDataGridViewTextBoxColumn.Name = "omschrijvingDataGridViewTextBoxColumn";
            // 
            // voorraadDataGridViewTextBoxColumn
            // 
            this.voorraadDataGridViewTextBoxColumn.DataPropertyName = "Voorraad";
            this.voorraadDataGridViewTextBoxColumn.HeaderText = "Voorraad";
            this.voorraadDataGridViewTextBoxColumn.Name = "voorraadDataGridViewTextBoxColumn";
            // 
            // leveranciersIdDataGridViewTextBoxColumn
            // 
            this.leveranciersIdDataGridViewTextBoxColumn.DataPropertyName = "LeveranciersId";
            this.leveranciersIdDataGridViewTextBoxColumn.HeaderText = "LeveranciersId";
            this.leveranciersIdDataGridViewTextBoxColumn.Name = "leveranciersIdDataGridViewTextBoxColumn";
            // 
            // productenBindingSource2
            // 
            this.productenBindingSource2.DataMember = "Producten";
            this.productenBindingSource2.DataSource = this.productenDataSet;
            // 
            // productenDataSet
            // 
            this.productenDataSet.DataSetName = "ProductenDataSet";
            this.productenDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // ToevoegenBtn
            // 
            this.ToevoegenBtn.Location = new System.Drawing.Point(583, 368);
            this.ToevoegenBtn.Name = "ToevoegenBtn";
            this.ToevoegenBtn.Size = new System.Drawing.Size(136, 23);
            this.ToevoegenBtn.TabIndex = 1;
            this.ToevoegenBtn.Text = "Product toevoegen";
            this.ToevoegenBtn.UseVisualStyleBackColor = true;
            this.ToevoegenBtn.Click += new System.EventHandler(this.ToevoegenBtn_Click);
            // 
            // productenBindingSource1
            // 
            this.productenBindingSource1.DataMember = "Producten";
            this.productenBindingSource1.DataSource = this.productenDataSet;
            // 
            // productenTableAdapter
            // 
            this.productenTableAdapter.ClearBeforeFill = true;
            // 
            // productenDataSetBindingSource
            // 
            this.productenDataSetBindingSource.DataSource = this.productenDataSet;
            this.productenDataSetBindingSource.Position = 0;
            // 
            // ProductenForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(731, 403);
            this.Controls.Add(this.ToevoegenBtn);
            this.Controls.Add(this.dataGridView1);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "ProductenForm";
            this.Text = "ProductenForm";
            this.Load += new System.EventHandler(this.ProductenForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.productenBindingSource2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.productenDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.productenBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.productenBindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.productenDataSetBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn;
        private System.Windows.Forms.BindingSource productenBindingSource;
        private System.Windows.Forms.Button ToevoegenBtn;
        private ProductenDataSet productenDataSet;
        private System.Windows.Forms.BindingSource productenBindingSource1;
        private ProductenDataSetTableAdapters.ProductenTableAdapter productenTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn naamDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn prijsDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn omschrijvingDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn voorraadDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn leveranciersIdDataGridViewTextBoxColumn;
        private System.Windows.Forms.BindingSource productenBindingSource2;
        private System.Windows.Forms.BindingSource productenDataSetBindingSource;
    }
}
﻿namespace WfaSuperPC
{
    partial class InlogForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label gebruikersnaamLabel;
            System.Windows.Forms.Label wachtwoordLabel;
            this.personenBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gebruikersnaamTextBox = new System.Windows.Forms.TextBox();
            this.wachtwoordTextBox = new System.Windows.Forms.TextBox();
            this.btnInlog = new System.Windows.Forms.Button();
            this.btnFormUnitTest = new System.Windows.Forms.Button();
            this.lblFeedback = new System.Windows.Forms.Label();
            gebruikersnaamLabel = new System.Windows.Forms.Label();
            wachtwoordLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.personenBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // gebruikersnaamLabel
            // 
            gebruikersnaamLabel.AutoSize = true;
            gebruikersnaamLabel.Location = new System.Drawing.Point(60, 89);
            gebruikersnaamLabel.Name = "gebruikersnaamLabel";
            gebruikersnaamLabel.Size = new System.Drawing.Size(117, 17);
            gebruikersnaamLabel.TabIndex = 1;
            gebruikersnaamLabel.Text = "Gebruikersnaam:";
            // 
            // wachtwoordLabel
            // 
            wachtwoordLabel.AutoSize = true;
            wachtwoordLabel.Location = new System.Drawing.Point(87, 131);
            wachtwoordLabel.Name = "wachtwoordLabel";
            wachtwoordLabel.Size = new System.Drawing.Size(90, 17);
            wachtwoordLabel.TabIndex = 3;
            wachtwoordLabel.Text = "Wachtwoord:";
            // 
            // gebruikersnaamTextBox
            // 
            this.gebruikersnaamTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.personenBindingSource, "Gebruikersnaam", true));
            this.gebruikersnaamTextBox.Location = new System.Drawing.Point(183, 86);
            this.gebruikersnaamTextBox.Name = "gebruikersnaamTextBox";
            this.gebruikersnaamTextBox.Size = new System.Drawing.Size(100, 22);
            this.gebruikersnaamTextBox.TabIndex = 2;
            // 
            // wachtwoordTextBox
            // 
            this.wachtwoordTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.personenBindingSource, "Wachtwoord", true));
            this.wachtwoordTextBox.Location = new System.Drawing.Point(183, 128);
            this.wachtwoordTextBox.Name = "wachtwoordTextBox";
            this.wachtwoordTextBox.Size = new System.Drawing.Size(100, 22);
            this.wachtwoordTextBox.TabIndex = 4;
            // 
            // btnInlog
            // 
            this.btnInlog.Location = new System.Drawing.Point(183, 173);
            this.btnInlog.Name = "btnInlog";
            this.btnInlog.Size = new System.Drawing.Size(99, 38);
            this.btnInlog.TabIndex = 5;
            this.btnInlog.Text = "&login!";
            this.btnInlog.UseVisualStyleBackColor = true;
            this.btnInlog.Click += new System.EventHandler(this.btnInlog_Click);
            // 
            // btnFormUnitTest
            // 
            this.btnFormUnitTest.Location = new System.Drawing.Point(36, 173);
            this.btnFormUnitTest.Name = "btnFormUnitTest";
            this.btnFormUnitTest.Size = new System.Drawing.Size(129, 38);
            this.btnFormUnitTest.TabIndex = 6;
            this.btnFormUnitTest.Text = "Form Unit Test";
            this.btnFormUnitTest.UseVisualStyleBackColor = true;
            this.btnFormUnitTest.Click += new System.EventHandler(this.btnFormUnitTest_Click);
            // 
            // lblFeedback
            // 
            this.lblFeedback.AutoSize = true;
            this.lblFeedback.Location = new System.Drawing.Point(60, 34);
            this.lblFeedback.MaximumSize = new System.Drawing.Size(250, 34);
            this.lblFeedback.MinimumSize = new System.Drawing.Size(250, 34);
            this.lblFeedback.Name = "lblFeedback";
            this.lblFeedback.Size = new System.Drawing.Size(250, 34);
            this.lblFeedback.TabIndex = 7;
            this.lblFeedback.Text = "lblFeedback";
            // 
            // InlogForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(361, 255);
            this.Controls.Add(this.lblFeedback);
            this.Controls.Add(this.btnFormUnitTest);
            this.Controls.Add(this.btnInlog);
            this.Controls.Add(wachtwoordLabel);
            this.Controls.Add(this.wachtwoordTextBox);
            this.Controls.Add(gebruikersnaamLabel);
            this.Controls.Add(this.gebruikersnaamTextBox);
            this.Name = "InlogForm";
            this.Text = "Inlog";
            this.Load += new System.EventHandler(this.InlogForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.personenBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.BindingSource personenBindingSource;
        private System.Windows.Forms.TextBox gebruikersnaamTextBox;
        private System.Windows.Forms.TextBox wachtwoordTextBox;
        private System.Windows.Forms.Button btnInlog;
        private System.Windows.Forms.Button btnFormUnitTest;
        private System.Windows.Forms.Label lblFeedback;

    }
}
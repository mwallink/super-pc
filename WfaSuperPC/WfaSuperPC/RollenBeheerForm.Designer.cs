﻿namespace WfaSuperPC
{
    partial class RollenBeheerForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RollenBeheerForm));
            this.personenRollenDataSet = new WfaSuperPC.PersonenRollenDataSet();
            this.rollenBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.rollenTableAdapter = new WfaSuperPC.PersonenRollenDataSetTableAdapters.RollenTableAdapter();
            this.tableAdapterManager = new WfaSuperPC.PersonenRollenDataSetTableAdapters.TableAdapterManager();
            this.rollenBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.rollenBindingNavigatorSaveItem = new System.Windows.Forms.ToolStripButton();
            this.rollenDataGridView = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.permissiesDataGridView = new System.Windows.Forms.DataGridView();
            this.viewPermissiesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.viewPermissiesDataSet = new WfaSuperPC.ViewPermissiesDataSet();
            this.viewPermissiesTableAdapter = new WfaSuperPC.ViewPermissiesDataSetTableAdapters.ViewPermissiesTableAdapter();
            this.lblRollen = new System.Windows.Forms.Label();
            this.lblPermissies = new System.Windows.Forms.Label();
            this.rolidDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rolnameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.controlidDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.controlnameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.parentDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rollenIdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.permissieidDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.visibleDataGridViewCeckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.editableDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.personenRollenDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rollenBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rollenBindingNavigator)).BeginInit();
            this.rollenBindingNavigator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rollenDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.permissiesDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.viewPermissiesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.viewPermissiesDataSet)).BeginInit();
            this.SuspendLayout();
            // 
            // personenRollenDataSet
            // 
            this.personenRollenDataSet.DataSetName = "PersonenRollenDataSet";
            this.personenRollenDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // rollenBindingSource
            // 
            this.rollenBindingSource.DataMember = "Rollen";
            this.rollenBindingSource.DataSource = this.personenRollenDataSet;
            // 
            // rollenTableAdapter
            // 
            this.rollenTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.PersonenRollenTableAdapter = null;
            this.tableAdapterManager.PersonenTableAdapter = null;
            this.tableAdapterManager.RollenTableAdapter = this.rollenTableAdapter;
            this.tableAdapterManager.UpdateOrder = WfaSuperPC.PersonenRollenDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // rollenBindingNavigator
            // 
            this.rollenBindingNavigator.AddNewItem = this.bindingNavigatorAddNewItem;
            this.rollenBindingNavigator.BindingSource = this.rollenBindingSource;
            this.rollenBindingNavigator.CountItem = this.bindingNavigatorCountItem;
            this.rollenBindingNavigator.DeleteItem = this.bindingNavigatorDeleteItem;
            this.rollenBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorMoveFirstItem,
            this.bindingNavigatorMovePreviousItem,
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.bindingNavigatorSeparator1,
            this.bindingNavigatorMoveNextItem,
            this.bindingNavigatorMoveLastItem,
            this.bindingNavigatorSeparator2,
            this.bindingNavigatorAddNewItem,
            this.bindingNavigatorDeleteItem,
            this.rollenBindingNavigatorSaveItem});
            this.rollenBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.rollenBindingNavigator.MoveFirstItem = this.bindingNavigatorMoveFirstItem;
            this.rollenBindingNavigator.MoveLastItem = this.bindingNavigatorMoveLastItem;
            this.rollenBindingNavigator.MoveNextItem = this.bindingNavigatorMoveNextItem;
            this.rollenBindingNavigator.MovePreviousItem = this.bindingNavigatorMovePreviousItem;
            this.rollenBindingNavigator.Name = "rollenBindingNavigator";
            this.rollenBindingNavigator.PositionItem = this.bindingNavigatorPositionItem;
            this.rollenBindingNavigator.Size = new System.Drawing.Size(1054, 27);
            this.rollenBindingNavigator.TabIndex = 0;
            this.rollenBindingNavigator.Text = "bindingNavigator1";
            // 
            // bindingNavigatorAddNewItem
            // 
            this.bindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorAddNewItem.Image")));
            this.bindingNavigatorAddNewItem.Name = "bindingNavigatorAddNewItem";
            this.bindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorAddNewItem.Size = new System.Drawing.Size(23, 24);
            this.bindingNavigatorAddNewItem.Text = "Add new";
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(45, 24);
            this.bindingNavigatorCountItem.Text = "of {0}";
            this.bindingNavigatorCountItem.ToolTipText = "Total number of items";
            // 
            // bindingNavigatorDeleteItem
            // 
            this.bindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorDeleteItem.Image")));
            this.bindingNavigatorDeleteItem.Name = "bindingNavigatorDeleteItem";
            this.bindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorDeleteItem.Size = new System.Drawing.Size(23, 24);
            this.bindingNavigatorDeleteItem.Text = "Delete";
            // 
            // bindingNavigatorMoveFirstItem
            // 
            this.bindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveFirstItem.Image")));
            this.bindingNavigatorMoveFirstItem.Name = "bindingNavigatorMoveFirstItem";
            this.bindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 24);
            this.bindingNavigatorMoveFirstItem.Text = "Move first";
            // 
            // bindingNavigatorMovePreviousItem
            // 
            this.bindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMovePreviousItem.Image")));
            this.bindingNavigatorMovePreviousItem.Name = "bindingNavigatorMovePreviousItem";
            this.bindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 24);
            this.bindingNavigatorMovePreviousItem.Text = "Move previous";
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 27);
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "Position";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 27);
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "Current position";
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator1";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 27);
            // 
            // bindingNavigatorMoveNextItem
            // 
            this.bindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveNextItem.Image")));
            this.bindingNavigatorMoveNextItem.Name = "bindingNavigatorMoveNextItem";
            this.bindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 24);
            this.bindingNavigatorMoveNextItem.Text = "Move next";
            // 
            // bindingNavigatorMoveLastItem
            // 
            this.bindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveLastItem.Image")));
            this.bindingNavigatorMoveLastItem.Name = "bindingNavigatorMoveLastItem";
            this.bindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 24);
            this.bindingNavigatorMoveLastItem.Text = "Move last";
            // 
            // bindingNavigatorSeparator2
            // 
            this.bindingNavigatorSeparator2.Name = "bindingNavigatorSeparator2";
            this.bindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 27);
            // 
            // rollenBindingNavigatorSaveItem
            // 
            this.rollenBindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.rollenBindingNavigatorSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("rollenBindingNavigatorSaveItem.Image")));
            this.rollenBindingNavigatorSaveItem.Name = "rollenBindingNavigatorSaveItem";
            this.rollenBindingNavigatorSaveItem.Size = new System.Drawing.Size(23, 24);
            this.rollenBindingNavigatorSaveItem.Text = "Save Data";
            this.rollenBindingNavigatorSaveItem.Click += new System.EventHandler(this.rollenBindingNavigatorSaveItem_Click);
            // 
            // rollenDataGridView
            // 
            this.rollenDataGridView.AutoGenerateColumns = false;
            this.rollenDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.rollenDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2});
            this.rollenDataGridView.DataSource = this.rollenBindingSource;
            this.rollenDataGridView.Location = new System.Drawing.Point(12, 84);
            this.rollenDataGridView.Name = "rollenDataGridView";
            this.rollenDataGridView.RowTemplate.Height = 24;
            this.rollenDataGridView.Size = new System.Drawing.Size(300, 582);
            this.rollenDataGridView.TabIndex = 1;
            this.rollenDataGridView.SelectionChanged += new System.EventHandler(this.rollenDataGridView_SelectionChanged);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "Id";
            this.dataGridViewTextBoxColumn1.HeaderText = "Id";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Width = 50;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "Naam";
            this.dataGridViewTextBoxColumn2.HeaderText = "Naam";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.Width = 200;
            // 
            // permissiesDataGridView
            // 
            this.permissiesDataGridView.AutoGenerateColumns = false;
            this.permissiesDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.permissiesDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.rolidDataGridViewTextBoxColumn,
            this.rolnameDataGridViewTextBoxColumn,
            this.controlidDataGridViewTextBoxColumn,
            this.controlnameDataGridViewTextBoxColumn,
            this.parentDataGridViewTextBoxColumn,
            this.rollenIdDataGridViewTextBoxColumn,
            this.permissieidDataGridViewTextBoxColumn,
            this.visibleDataGridViewCeckBoxColumn,
            this.editableDataGridViewCheckBoxColumn});
            this.permissiesDataGridView.DataSource = this.viewPermissiesBindingSource;
            this.permissiesDataGridView.Location = new System.Drawing.Point(318, 84);
            this.permissiesDataGridView.Name = "permissiesDataGridView";
            this.permissiesDataGridView.RowTemplate.Height = 24;
            this.permissiesDataGridView.Size = new System.Drawing.Size(724, 582);
            this.permissiesDataGridView.TabIndex = 2;
            this.permissiesDataGridView.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.permissiesDataGridView_CellContentClick);
            // 
            // viewPermissiesBindingSource
            // 
            this.viewPermissiesBindingSource.DataMember = "ViewPermissies";
            this.viewPermissiesBindingSource.DataSource = this.viewPermissiesDataSet;
            // 
            // viewPermissiesDataSet
            // 
            this.viewPermissiesDataSet.DataSetName = "ViewPermissiesDataSet";
            this.viewPermissiesDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // viewPermissiesTableAdapter
            // 
            this.viewPermissiesTableAdapter.ClearBeforeFill = true;
            // 
            // lblRollen
            // 
            this.lblRollen.AutoSize = true;
            this.lblRollen.Location = new System.Drawing.Point(13, 61);
            this.lblRollen.Name = "lblRollen";
            this.lblRollen.Size = new System.Drawing.Size(48, 17);
            this.lblRollen.TabIndex = 3;
            this.lblRollen.Text = "Rollen";
            // 
            // lblPermissies
            // 
            this.lblPermissies.AutoSize = true;
            this.lblPermissies.Location = new System.Drawing.Point(318, 61);
            this.lblPermissies.Name = "lblPermissies";
            this.lblPermissies.Size = new System.Drawing.Size(76, 17);
            this.lblPermissies.TabIndex = 4;
            this.lblPermissies.Text = "Permissies";
            // 
            // rolidDataGridViewTextBoxColumn
            // 
            this.rolidDataGridViewTextBoxColumn.DataPropertyName = "rol_id";
            this.rolidDataGridViewTextBoxColumn.HeaderText = "rol_id";
            this.rolidDataGridViewTextBoxColumn.Name = "rolidDataGridViewTextBoxColumn";
            this.rolidDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.rolidDataGridViewTextBoxColumn.Visible = false;
            // 
            // rolnameDataGridViewTextBoxColumn
            // 
            this.rolnameDataGridViewTextBoxColumn.DataPropertyName = "rol_name";
            this.rolnameDataGridViewTextBoxColumn.HeaderText = "Rol naam";
            this.rolnameDataGridViewTextBoxColumn.Name = "rolnameDataGridViewTextBoxColumn";
            this.rolnameDataGridViewTextBoxColumn.ReadOnly = true;
            this.rolnameDataGridViewTextBoxColumn.Width = 150;
            // 
            // controlidDataGridViewTextBoxColumn
            // 
            this.controlidDataGridViewTextBoxColumn.DataPropertyName = "control_id";
            this.controlidDataGridViewTextBoxColumn.HeaderText = "control_id";
            this.controlidDataGridViewTextBoxColumn.Name = "controlidDataGridViewTextBoxColumn";
            this.controlidDataGridViewTextBoxColumn.Visible = false;
            // 
            // controlnameDataGridViewTextBoxColumn
            // 
            this.controlnameDataGridViewTextBoxColumn.DataPropertyName = "control_name";
            this.controlnameDataGridViewTextBoxColumn.HeaderText = "Control naam";
            this.controlnameDataGridViewTextBoxColumn.Name = "controlnameDataGridViewTextBoxColumn";
            this.controlnameDataGridViewTextBoxColumn.ReadOnly = true;
            this.controlnameDataGridViewTextBoxColumn.Width = 150;
            // 
            // parentDataGridViewTextBoxColumn
            // 
            this.parentDataGridViewTextBoxColumn.DataPropertyName = "Parent";
            this.parentDataGridViewTextBoxColumn.HeaderText = "Parent naam";
            this.parentDataGridViewTextBoxColumn.Name = "parentDataGridViewTextBoxColumn";
            this.parentDataGridViewTextBoxColumn.ReadOnly = true;
            this.parentDataGridViewTextBoxColumn.Width = 150;
            // 
            // rollenIdDataGridViewTextBoxColumn
            // 
            this.rollenIdDataGridViewTextBoxColumn.DataPropertyName = "RollenId";
            this.rollenIdDataGridViewTextBoxColumn.HeaderText = "RollenId";
            this.rollenIdDataGridViewTextBoxColumn.Name = "rollenIdDataGridViewTextBoxColumn";
            this.rollenIdDataGridViewTextBoxColumn.Visible = false;
            // 
            // permissieidDataGridViewTextBoxColumn
            // 
            this.permissieidDataGridViewTextBoxColumn.DataPropertyName = "permissie_id";
            this.permissieidDataGridViewTextBoxColumn.HeaderText = "permissie_id";
            this.permissieidDataGridViewTextBoxColumn.Name = "permissieidDataGridViewTextBoxColumn";
            this.permissieidDataGridViewTextBoxColumn.Visible = false;
            // 
            // visibleDataGridViewCeckBoxColumn
            // 
            this.visibleDataGridViewCeckBoxColumn.DataPropertyName = "Visible";
            this.visibleDataGridViewCeckBoxColumn.FalseValue = "0";
            this.visibleDataGridViewCeckBoxColumn.HeaderText = "Visible";
            this.visibleDataGridViewCeckBoxColumn.IndeterminateValue = "-1";
            this.visibleDataGridViewCeckBoxColumn.Name = "visibleDataGridViewCeckBoxColumn";
            this.visibleDataGridViewCeckBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.visibleDataGridViewCeckBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.visibleDataGridViewCeckBoxColumn.TrueValue = "1";
            // 
            // editableDataGridViewCheckBoxColumn
            // 
            this.editableDataGridViewCheckBoxColumn.DataPropertyName = "Editable";
            this.editableDataGridViewCheckBoxColumn.FalseValue = "0";
            this.editableDataGridViewCheckBoxColumn.HeaderText = "Editable";
            this.editableDataGridViewCheckBoxColumn.IndeterminateValue = "-1";
            this.editableDataGridViewCheckBoxColumn.Name = "editableDataGridViewCheckBoxColumn";
            this.editableDataGridViewCheckBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.editableDataGridViewCheckBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.editableDataGridViewCheckBoxColumn.TrueValue = "1";
            // 
            // RollenBeheerForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1054, 678);
            this.Controls.Add(this.lblPermissies);
            this.Controls.Add(this.lblRollen);
            this.Controls.Add(this.permissiesDataGridView);
            this.Controls.Add(this.rollenDataGridView);
            this.Controls.Add(this.rollenBindingNavigator);
            this.Name = "RollenBeheerForm";
            this.Text = "RollenBeheerForm";
            this.Load += new System.EventHandler(this.RollenBeheerForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.personenRollenDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rollenBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rollenBindingNavigator)).EndInit();
            this.rollenBindingNavigator.ResumeLayout(false);
            this.rollenBindingNavigator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rollenDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.permissiesDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.viewPermissiesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.viewPermissiesDataSet)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private PersonenRollenDataSet personenRollenDataSet;
        private System.Windows.Forms.BindingSource rollenBindingSource;
        private PersonenRollenDataSetTableAdapters.RollenTableAdapter rollenTableAdapter;
        private PersonenRollenDataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.BindingNavigator rollenBindingNavigator;
        private System.Windows.Forms.ToolStripButton bindingNavigatorAddNewItem;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorDeleteItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveFirstItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMovePreviousItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveNextItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveLastItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator2;
        private System.Windows.Forms.ToolStripButton rollenBindingNavigatorSaveItem;
        private System.Windows.Forms.DataGridView rollenDataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridView permissiesDataGridView;
        private ViewPermissiesDataSet viewPermissiesDataSet;
        private System.Windows.Forms.BindingSource viewPermissiesBindingSource;
        private ViewPermissiesDataSetTableAdapters.ViewPermissiesTableAdapter viewPermissiesTableAdapter;
        private System.Windows.Forms.Label lblRollen;
        private System.Windows.Forms.Label lblPermissies;
        private System.Windows.Forms.DataGridViewTextBoxColumn rolidDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn rolnameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn controlidDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn controlnameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn parentDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn rollenIdDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn permissieidDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn visibleDataGridViewCeckBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn editableDataGridViewCheckBoxColumn;
    }
}
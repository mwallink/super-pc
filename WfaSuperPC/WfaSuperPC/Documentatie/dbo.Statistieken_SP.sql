﻿ALTER PROCEDURE Statistieken
	@dagopbrengstdatum DateTime = null,
	@besteverkoperid int OUTPUT,
	@dagopbrengst float OUTPUT,
	@pctuitstaandekosten float OUTPUT
AS

SELECT TOP 1 @besteverkoperid = B.FkVerkoperId
FROM Producten as P 
JOIN Bestelregels  as BR ON P.Id = BR.ProductenId
JOIN Bestellingen as B ON B.Id = BR.BestellingenId
JOIN Personen as PER ON PER.Id = B.FkVerkoperId
GROUP BY B.FkVerkoperId
ORDER BY SUM(P.Prijs) DESC;

SELECT @dagopbrengst = SUM(P.Prijs)
FROM 
Bestellingen as B
JOIN Bestelregels as BR ON BR.BestellingenId = B.Id
JOIN Producten as P ON P.Id = BR.ProductenId
WHERE B.AanmaakDatum = @dagopbrengstdatum
;

SELECT @pctuitstaandekosten = SUM(B.Aanbetaling)/SUM(P.Prijs) * 100
FROM Bestellingen as B
JOIN Bestelregels as BR ON BR.BestellingenId = B.Id
JOIN Producten as P ON P.Id = BR.ProductenId
WHERE B.Betaald = 0
;

RETURN 0
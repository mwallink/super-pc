﻿SELECT TOP 1 B.FkVerkoperId
FROM Producten as P 
JOIN Bestelregels  as BR ON P.Id = BR.ProductenId
JOIN Bestellingen as B ON B.Id = BR.BestellingenId
JOIN Personen as PER ON PER.Id = B.FkVerkoperId
GROUP BY B.FkVerkoperId
ORDER BY SUM(P.Prijs) DESC
;

SELECT SUM(P.Prijs)
FROM 
Bestellingen as B
JOIN Bestelregels as BR ON BR.BestellingenId = B.Id
JOIN Producten as P ON P.Id = BR.ProductenId
WHERE B.AanmaakDatum = GETDATE()
;

SELECT SUM(P.Prijs) as sumprijs, SUM(B.Aanbetaling) as sumaanbetaling, SUM(B.Aanbetaling)/SUM(P.Prijs) * 100 as uitstaandekosten
FROM Bestellingen as B
JOIN Bestelregels as BR ON BR.BestellingenId = B.Id
JOIN Producten as P ON P.Id = BR.ProductenId
WHERE B.Betaald = 0
;








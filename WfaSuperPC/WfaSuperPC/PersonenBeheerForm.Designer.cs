﻿namespace WfaSuperPC
{
    partial class PersonenBeheerForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PersonenBeheerForm));
            this.personenRollenDataSet = new WfaSuperPC.PersonenRollenDataSet();
            this.personenBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.personenTableAdapter = new WfaSuperPC.PersonenRollenDataSetTableAdapters.PersonenTableAdapter();
            this.tableAdapterManager = new WfaSuperPC.PersonenRollenDataSetTableAdapters.TableAdapterManager();
            this.personenBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.personenBindingNavigatorSaveItem = new System.Windows.Forms.ToolStripButton();
            this.personenDataGridView = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rollenDataGridView = new System.Windows.Forms.DataGridView();
            this.personenIdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rollenIdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rol_naam = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fKPersonenRollenPersonenBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.personenRollenTableAdapter = new WfaSuperPC.PersonenRollenDataSetTableAdapters.PersonenRollenTableAdapter();
            this.lblHuidigeRollen = new System.Windows.Forms.Label();
            this.lblMogelijkeRollen = new System.Windows.Forms.Label();
            this.lbxMogelijkeRollen = new System.Windows.Forms.ListBox();
            this.lbxHuidigeRollen = new System.Windows.Forms.ListBox();
            this.btnVoegRolToe = new System.Windows.Forms.Button();
            this.btnVerwijderRol = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.personenRollenDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.personenBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.personenBindingNavigator)).BeginInit();
            this.personenBindingNavigator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.personenDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rollenDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fKPersonenRollenPersonenBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // personenRollenDataSet
            // 
            this.personenRollenDataSet.DataSetName = "PersonenRollenDataSet";
            this.personenRollenDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // personenBindingSource
            // 
            this.personenBindingSource.DataMember = "Personen";
            this.personenBindingSource.DataSource = this.personenRollenDataSet;
            // 
            // personenTableAdapter
            // 
            this.personenTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.PersonenRollenTableAdapter = null;
            this.tableAdapterManager.PersonenTableAdapter = this.personenTableAdapter;
            this.tableAdapterManager.RollenTableAdapter = null;
            this.tableAdapterManager.UpdateOrder = WfaSuperPC.PersonenRollenDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // personenBindingNavigator
            // 
            this.personenBindingNavigator.AddNewItem = this.bindingNavigatorAddNewItem;
            this.personenBindingNavigator.BindingSource = this.personenBindingSource;
            this.personenBindingNavigator.CountItem = this.bindingNavigatorCountItem;
            this.personenBindingNavigator.DeleteItem = this.bindingNavigatorDeleteItem;
            this.personenBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorMoveFirstItem,
            this.bindingNavigatorMovePreviousItem,
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.bindingNavigatorSeparator1,
            this.bindingNavigatorMoveNextItem,
            this.bindingNavigatorMoveLastItem,
            this.bindingNavigatorSeparator2,
            this.bindingNavigatorAddNewItem,
            this.bindingNavigatorDeleteItem,
            this.personenBindingNavigatorSaveItem});
            this.personenBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.personenBindingNavigator.MoveFirstItem = this.bindingNavigatorMoveFirstItem;
            this.personenBindingNavigator.MoveLastItem = this.bindingNavigatorMoveLastItem;
            this.personenBindingNavigator.MoveNextItem = this.bindingNavigatorMoveNextItem;
            this.personenBindingNavigator.MovePreviousItem = this.bindingNavigatorMovePreviousItem;
            this.personenBindingNavigator.Name = "personenBindingNavigator";
            this.personenBindingNavigator.PositionItem = this.bindingNavigatorPositionItem;
            this.personenBindingNavigator.Size = new System.Drawing.Size(1116, 27);
            this.personenBindingNavigator.TabIndex = 0;
            this.personenBindingNavigator.Text = "bindingNavigator1";
            // 
            // bindingNavigatorAddNewItem
            // 
            this.bindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorAddNewItem.Image")));
            this.bindingNavigatorAddNewItem.Name = "bindingNavigatorAddNewItem";
            this.bindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorAddNewItem.Size = new System.Drawing.Size(23, 24);
            this.bindingNavigatorAddNewItem.Text = "Add new";
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(45, 24);
            this.bindingNavigatorCountItem.Text = "of {0}";
            this.bindingNavigatorCountItem.ToolTipText = "Total number of items";
            // 
            // bindingNavigatorDeleteItem
            // 
            this.bindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorDeleteItem.Image")));
            this.bindingNavigatorDeleteItem.Name = "bindingNavigatorDeleteItem";
            this.bindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorDeleteItem.Size = new System.Drawing.Size(23, 24);
            this.bindingNavigatorDeleteItem.Text = "Delete";
            // 
            // bindingNavigatorMoveFirstItem
            // 
            this.bindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveFirstItem.Image")));
            this.bindingNavigatorMoveFirstItem.Name = "bindingNavigatorMoveFirstItem";
            this.bindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 24);
            this.bindingNavigatorMoveFirstItem.Text = "Move first";
            // 
            // bindingNavigatorMovePreviousItem
            // 
            this.bindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMovePreviousItem.Image")));
            this.bindingNavigatorMovePreviousItem.Name = "bindingNavigatorMovePreviousItem";
            this.bindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 24);
            this.bindingNavigatorMovePreviousItem.Text = "Move previous";
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 27);
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "Position";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 27);
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "Current position";
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator1";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 27);
            // 
            // bindingNavigatorMoveNextItem
            // 
            this.bindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveNextItem.Image")));
            this.bindingNavigatorMoveNextItem.Name = "bindingNavigatorMoveNextItem";
            this.bindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 24);
            this.bindingNavigatorMoveNextItem.Text = "Move next";
            // 
            // bindingNavigatorMoveLastItem
            // 
            this.bindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveLastItem.Image")));
            this.bindingNavigatorMoveLastItem.Name = "bindingNavigatorMoveLastItem";
            this.bindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 24);
            this.bindingNavigatorMoveLastItem.Text = "Move last";
            // 
            // bindingNavigatorSeparator2
            // 
            this.bindingNavigatorSeparator2.Name = "bindingNavigatorSeparator2";
            this.bindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 27);
            // 
            // personenBindingNavigatorSaveItem
            // 
            this.personenBindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.personenBindingNavigatorSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("personenBindingNavigatorSaveItem.Image")));
            this.personenBindingNavigatorSaveItem.Name = "personenBindingNavigatorSaveItem";
            this.personenBindingNavigatorSaveItem.Size = new System.Drawing.Size(23, 24);
            this.personenBindingNavigatorSaveItem.Text = "Save Data";
            this.personenBindingNavigatorSaveItem.Click += new System.EventHandler(this.personenBindingNavigatorSaveItem_Click);
            // 
            // personenDataGridView
            // 
            this.personenDataGridView.AutoGenerateColumns = false;
            this.personenDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.personenDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn5});
            this.personenDataGridView.DataSource = this.personenBindingSource;
            this.personenDataGridView.Location = new System.Drawing.Point(31, 52);
            this.personenDataGridView.Name = "personenDataGridView";
            this.personenDataGridView.RowTemplate.Height = 24;
            this.personenDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.personenDataGridView.Size = new System.Drawing.Size(546, 278);
            this.personenDataGridView.TabIndex = 1;
            this.personenDataGridView.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.personenDataGridView_CellContentClick);
            this.personenDataGridView.SelectionChanged += new System.EventHandler(this.personenDataGridView_SelectionChanged);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "Id";
            this.dataGridViewTextBoxColumn1.HeaderText = "Id";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "Voornaam";
            this.dataGridViewTextBoxColumn2.HeaderText = "Voornaam";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "Achternaam";
            this.dataGridViewTextBoxColumn3.HeaderText = "Achternaam";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "Gebruikersnaam";
            this.dataGridViewTextBoxColumn4.HeaderText = "Gebruikersnaam";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "Wachtwoord";
            this.dataGridViewTextBoxColumn5.HeaderText = "Wachtwoord";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            // 
            // rollenDataGridView
            // 
            this.rollenDataGridView.AllowUserToAddRows = false;
            this.rollenDataGridView.AllowUserToDeleteRows = false;
            this.rollenDataGridView.AutoGenerateColumns = false;
            this.rollenDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.rollenDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.personenIdDataGridViewTextBoxColumn,
            this.rollenIdDataGridViewTextBoxColumn,
            this.rol_naam});
            this.rollenDataGridView.DataSource = this.fKPersonenRollenPersonenBindingSource;
            this.rollenDataGridView.Location = new System.Drawing.Point(610, 52);
            this.rollenDataGridView.Name = "rollenDataGridView";
            this.rollenDataGridView.ReadOnly = true;
            this.rollenDataGridView.RowTemplate.Height = 24;
            this.rollenDataGridView.Size = new System.Drawing.Size(494, 278);
            this.rollenDataGridView.TabIndex = 2;
            this.rollenDataGridView.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.rollenDataGridView_DataBindingComplete);
            this.rollenDataGridView.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.rollenDataGridView_RowsAdded);
            // 
            // personenIdDataGridViewTextBoxColumn
            // 
            this.personenIdDataGridViewTextBoxColumn.DataPropertyName = "Personen_Id";
            this.personenIdDataGridViewTextBoxColumn.HeaderText = "Personen_Id";
            this.personenIdDataGridViewTextBoxColumn.Name = "personenIdDataGridViewTextBoxColumn";
            this.personenIdDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // rollenIdDataGridViewTextBoxColumn
            // 
            this.rollenIdDataGridViewTextBoxColumn.DataPropertyName = "Rollen_Id";
            this.rollenIdDataGridViewTextBoxColumn.HeaderText = "Rollen_Id";
            this.rollenIdDataGridViewTextBoxColumn.Name = "rollenIdDataGridViewTextBoxColumn";
            this.rollenIdDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // rol_naam
            // 
            this.rol_naam.HeaderText = "Rol naam";
            this.rol_naam.Name = "rol_naam";
            this.rol_naam.ReadOnly = true;
            // 
            // fKPersonenRollenPersonenBindingSource
            // 
            this.fKPersonenRollenPersonenBindingSource.DataMember = "FK_PersonenRollen_Personen";
            this.fKPersonenRollenPersonenBindingSource.DataSource = this.personenBindingSource;
            // 
            // personenRollenTableAdapter
            // 
            this.personenRollenTableAdapter.ClearBeforeFill = true;
            // 
            // lblHuidigeRollen
            // 
            this.lblHuidigeRollen.AutoSize = true;
            this.lblHuidigeRollen.Location = new System.Drawing.Point(31, 354);
            this.lblHuidigeRollen.Name = "lblHuidigeRollen";
            this.lblHuidigeRollen.Size = new System.Drawing.Size(100, 17);
            this.lblHuidigeRollen.TabIndex = 4;
            this.lblHuidigeRollen.Text = "Huidige Rollen";
            // 
            // lblMogelijkeRollen
            // 
            this.lblMogelijkeRollen.AutoSize = true;
            this.lblMogelijkeRollen.Location = new System.Drawing.Point(339, 354);
            this.lblMogelijkeRollen.Name = "lblMogelijkeRollen";
            this.lblMogelijkeRollen.Size = new System.Drawing.Size(111, 17);
            this.lblMogelijkeRollen.TabIndex = 5;
            this.lblMogelijkeRollen.Text = "Mogelijke Rollen";
            // 
            // lbxMogelijkeRollen
            // 
            this.lbxMogelijkeRollen.FormattingEnabled = true;
            this.lbxMogelijkeRollen.ItemHeight = 16;
            this.lbxMogelijkeRollen.Location = new System.Drawing.Point(342, 374);
            this.lbxMogelijkeRollen.Name = "lbxMogelijkeRollen";
            this.lbxMogelijkeRollen.Size = new System.Drawing.Size(120, 228);
            this.lbxMogelijkeRollen.TabIndex = 6;
            // 
            // lbxHuidigeRollen
            // 
            this.lbxHuidigeRollen.FormattingEnabled = true;
            this.lbxHuidigeRollen.ItemHeight = 16;
            this.lbxHuidigeRollen.Location = new System.Drawing.Point(34, 375);
            this.lbxHuidigeRollen.Name = "lbxHuidigeRollen";
            this.lbxHuidigeRollen.Size = new System.Drawing.Size(120, 228);
            this.lbxHuidigeRollen.TabIndex = 7;
            // 
            // btnVoegRolToe
            // 
            this.btnVoegRolToe.Location = new System.Drawing.Point(211, 400);
            this.btnVoegRolToe.Name = "btnVoegRolToe";
            this.btnVoegRolToe.Size = new System.Drawing.Size(75, 57);
            this.btnVoegRolToe.TabIndex = 8;
            this.btnVoegRolToe.Text = "<=";
            this.btnVoegRolToe.UseVisualStyleBackColor = true;
            this.btnVoegRolToe.Click += new System.EventHandler(this.btnVoegRolToe_Click);
            // 
            // btnVerwijderRol
            // 
            this.btnVerwijderRol.Location = new System.Drawing.Point(211, 464);
            this.btnVerwijderRol.Name = "btnVerwijderRol";
            this.btnVerwijderRol.Size = new System.Drawing.Size(75, 63);
            this.btnVerwijderRol.TabIndex = 9;
            this.btnVerwijderRol.Text = "=>";
            this.btnVerwijderRol.UseVisualStyleBackColor = true;
            this.btnVerwijderRol.Click += new System.EventHandler(this.btnVerwijderRol_Click);
            // 
            // PersonenBeheerForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1116, 702);
            this.Controls.Add(this.btnVerwijderRol);
            this.Controls.Add(this.btnVoegRolToe);
            this.Controls.Add(this.lbxHuidigeRollen);
            this.Controls.Add(this.lbxMogelijkeRollen);
            this.Controls.Add(this.lblMogelijkeRollen);
            this.Controls.Add(this.lblHuidigeRollen);
            this.Controls.Add(this.rollenDataGridView);
            this.Controls.Add(this.personenDataGridView);
            this.Controls.Add(this.personenBindingNavigator);
            this.Name = "PersonenBeheerForm";
            this.Text = "PersonenBeheerForm";
            this.Load += new System.EventHandler(this.PersonenBeheerForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.personenRollenDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.personenBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.personenBindingNavigator)).EndInit();
            this.personenBindingNavigator.ResumeLayout(false);
            this.personenBindingNavigator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.personenDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rollenDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fKPersonenRollenPersonenBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private PersonenRollenDataSet personenRollenDataSet;
        private System.Windows.Forms.BindingSource personenBindingSource;
        private PersonenRollenDataSetTableAdapters.PersonenTableAdapter personenTableAdapter;
        private PersonenRollenDataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.BindingNavigator personenBindingNavigator;
        private System.Windows.Forms.ToolStripButton bindingNavigatorAddNewItem;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorDeleteItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveFirstItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMovePreviousItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveNextItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveLastItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator2;
        private System.Windows.Forms.ToolStripButton personenBindingNavigatorSaveItem;
        private System.Windows.Forms.DataGridView personenDataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridView rollenDataGridView;
        private System.Windows.Forms.BindingSource fKPersonenRollenPersonenBindingSource;
        private PersonenRollenDataSetTableAdapters.PersonenRollenTableAdapter personenRollenTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn personenIdDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn rollenIdDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn rol_naam;
        private System.Windows.Forms.Label lblHuidigeRollen;
        private System.Windows.Forms.Label lblMogelijkeRollen;
        private System.Windows.Forms.ListBox lbxMogelijkeRollen;
        private System.Windows.Forms.ListBox lbxHuidigeRollen;
        private System.Windows.Forms.Button btnVoegRolToe;
        private System.Windows.Forms.Button btnVerwijderRol;

    }
}
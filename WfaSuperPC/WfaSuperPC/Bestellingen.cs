//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WfaSuperPC
{
    using System;
    using System.Collections.Generic;
    
    public partial class Bestellingen
    {
        public Bestellingen()
        {
            this.KlantAkkoord = false;
            this.ManagerAkkoord = 0;
            this.HeeftOfferte = false;
            this.Betaald = false;
            this.Bestelregels = new HashSet<Bestelregels>();
        }
    
        public int Id { get; set; }
        public int KlantenId { get; set; }
        public int FkVerkoperId { get; set; }
        public short Korting { get; set; }
        public bool KlantAkkoord { get; set; }
        public int ManagerAkkoord { get; set; }
        public bool HeeftOfferte { get; set; }
        public Nullable<System.DateTime> AanmaakDatum { get; set; }
        public double Aanbetaling { get; set; }
        public bool Betaald { get; set; }
        public int StatussenId { get; set; }
    
        public virtual ICollection<Bestelregels> Bestelregels { get; set; }
        public virtual Klanten Klanten { get; set; }
        public virtual Personen Personen { get; set; }
        public virtual Statussen Statussen { get; set; }
    }
}

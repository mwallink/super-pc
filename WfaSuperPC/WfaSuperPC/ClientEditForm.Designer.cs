﻿namespace WfaSuperPC
{
    partial class ClientEditForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.OpslaanBtn = new System.Windows.Forms.Button();
            this.voornaamTb = new System.Windows.Forms.TextBox();
            this.achternaamTb = new System.Windows.Forms.TextBox();
            this.bedrijfsnaamTb = new System.Windows.Forms.TextBox();
            this.landTb = new System.Windows.Forms.TextBox();
            this.straatnaamTb = new System.Windows.Forms.TextBox();
            this.huisnummerTb = new System.Windows.Forms.TextBox();
            this.huisnummerToevoegingTb = new System.Windows.Forms.TextBox();
            this.gsmTb = new System.Windows.Forms.TextBox();
            this.telTb = new System.Windows.Forms.TextBox();
            this.emailTb = new System.Windows.Forms.TextBox();
            this.postcodeTb = new System.Windows.Forms.TextBox();
            this.voornaamLabel = new System.Windows.Forms.Label();
            this.achternaamLabel = new System.Windows.Forms.Label();
            this.bedrijfsnaamLabel = new System.Windows.Forms.Label();
            this.landLabel = new System.Windows.Forms.Label();
            this.postcodeLabel = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.huisnummerLabel = new System.Windows.Forms.Label();
            this.straatnaamLabel = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.telLabel = new System.Windows.Forms.Label();
            this.emailLabel = new System.Windows.Forms.Label();
            this.annulerenBtn = new System.Windows.Forms.Button();
            this.label12 = new System.Windows.Forms.Label();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // OpslaanBtn
            // 
            this.OpslaanBtn.Location = new System.Drawing.Point(340, 331);
            this.OpslaanBtn.Name = "OpslaanBtn";
            this.OpslaanBtn.Size = new System.Drawing.Size(75, 23);
            this.OpslaanBtn.TabIndex = 0;
            this.OpslaanBtn.Text = "Opslaan";
            this.OpslaanBtn.UseVisualStyleBackColor = true;
            this.OpslaanBtn.Click += new System.EventHandler(this.OpslaanBtn_Click);
            // 
            // voornaamTb
            // 
            this.voornaamTb.Location = new System.Drawing.Point(137, 6);
            this.voornaamTb.Name = "voornaamTb";
            this.voornaamTb.Size = new System.Drawing.Size(278, 20);
            this.voornaamTb.TabIndex = 1;
            // 
            // achternaamTb
            // 
            this.achternaamTb.Location = new System.Drawing.Point(137, 32);
            this.achternaamTb.Name = "achternaamTb";
            this.achternaamTb.Size = new System.Drawing.Size(278, 20);
            this.achternaamTb.TabIndex = 2;
            // 
            // bedrijfsnaamTb
            // 
            this.bedrijfsnaamTb.Location = new System.Drawing.Point(137, 292);
            this.bedrijfsnaamTb.Name = "bedrijfsnaamTb";
            this.bedrijfsnaamTb.Size = new System.Drawing.Size(278, 20);
            this.bedrijfsnaamTb.TabIndex = 3;
            // 
            // landTb
            // 
            this.landTb.Location = new System.Drawing.Point(137, 58);
            this.landTb.Name = "landTb";
            this.landTb.Size = new System.Drawing.Size(278, 20);
            this.landTb.TabIndex = 4;
            // 
            // straatnaamTb
            // 
            this.straatnaamTb.Location = new System.Drawing.Point(137, 84);
            this.straatnaamTb.Name = "straatnaamTb";
            this.straatnaamTb.Size = new System.Drawing.Size(278, 20);
            this.straatnaamTb.TabIndex = 5;
            // 
            // huisnummerTb
            // 
            this.huisnummerTb.Location = new System.Drawing.Point(340, 110);
            this.huisnummerTb.Name = "huisnummerTb";
            this.huisnummerTb.Size = new System.Drawing.Size(75, 20);
            this.huisnummerTb.TabIndex = 6;
            // 
            // huisnummerToevoegingTb
            // 
            this.huisnummerToevoegingTb.Location = new System.Drawing.Point(340, 136);
            this.huisnummerToevoegingTb.Name = "huisnummerToevoegingTb";
            this.huisnummerToevoegingTb.Size = new System.Drawing.Size(75, 20);
            this.huisnummerToevoegingTb.TabIndex = 7;
            // 
            // gsmTb
            // 
            this.gsmTb.Location = new System.Drawing.Point(137, 240);
            this.gsmTb.Name = "gsmTb";
            this.gsmTb.Size = new System.Drawing.Size(278, 20);
            this.gsmTb.TabIndex = 11;
            // 
            // telTb
            // 
            this.telTb.Location = new System.Drawing.Point(137, 214);
            this.telTb.Name = "telTb";
            this.telTb.Size = new System.Drawing.Size(278, 20);
            this.telTb.TabIndex = 10;
            // 
            // emailTb
            // 
            this.emailTb.Location = new System.Drawing.Point(137, 188);
            this.emailTb.Name = "emailTb";
            this.emailTb.Size = new System.Drawing.Size(278, 20);
            this.emailTb.TabIndex = 9;
            // 
            // postcodeTb
            // 
            this.postcodeTb.ForeColor = System.Drawing.SystemColors.WindowText;
            this.postcodeTb.Location = new System.Drawing.Point(340, 162);
            this.postcodeTb.Name = "postcodeTb";
            this.postcodeTb.Size = new System.Drawing.Size(75, 20);
            this.postcodeTb.TabIndex = 8;
            // 
            // voornaamLabel
            // 
            this.voornaamLabel.AutoSize = true;
            this.voornaamLabel.Location = new System.Drawing.Point(12, 9);
            this.voornaamLabel.Name = "voornaamLabel";
            this.voornaamLabel.Size = new System.Drawing.Size(62, 13);
            this.voornaamLabel.TabIndex = 12;
            this.voornaamLabel.Text = "* Voornaam";
            // 
            // achternaamLabel
            // 
            this.achternaamLabel.AutoSize = true;
            this.achternaamLabel.Location = new System.Drawing.Point(12, 35);
            this.achternaamLabel.Name = "achternaamLabel";
            this.achternaamLabel.Size = new System.Drawing.Size(71, 13);
            this.achternaamLabel.TabIndex = 13;
            this.achternaamLabel.Text = "* Achternaam";
            // 
            // bedrijfsnaamLabel
            // 
            this.bedrijfsnaamLabel.AutoSize = true;
            this.bedrijfsnaamLabel.Location = new System.Drawing.Point(12, 295);
            this.bedrijfsnaamLabel.Name = "bedrijfsnaamLabel";
            this.bedrijfsnaamLabel.Size = new System.Drawing.Size(74, 13);
            this.bedrijfsnaamLabel.TabIndex = 14;
            this.bedrijfsnaamLabel.Text = "* Bedrijfsnaam";
            // 
            // landLabel
            // 
            this.landLabel.AutoSize = true;
            this.landLabel.Location = new System.Drawing.Point(12, 61);
            this.landLabel.Name = "landLabel";
            this.landLabel.Size = new System.Drawing.Size(38, 13);
            this.landLabel.TabIndex = 15;
            this.landLabel.Text = "* Land";
            // 
            // postcodeLabel
            // 
            this.postcodeLabel.AutoSize = true;
            this.postcodeLabel.ForeColor = System.Drawing.SystemColors.WindowText;
            this.postcodeLabel.Location = new System.Drawing.Point(12, 165);
            this.postcodeLabel.Name = "postcodeLabel";
            this.postcodeLabel.Size = new System.Drawing.Size(59, 13);
            this.postcodeLabel.TabIndex = 19;
            this.postcodeLabel.Text = "* Postcode";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 139);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(118, 13);
            this.label6.TabIndex = 18;
            this.label6.Text = "Huisnummertoevoeging";
            // 
            // huisnummerLabel
            // 
            this.huisnummerLabel.AutoSize = true;
            this.huisnummerLabel.Location = new System.Drawing.Point(12, 113);
            this.huisnummerLabel.Name = "huisnummerLabel";
            this.huisnummerLabel.Size = new System.Drawing.Size(72, 13);
            this.huisnummerLabel.TabIndex = 17;
            this.huisnummerLabel.Text = "* Huisnummer";
            // 
            // straatnaamLabel
            // 
            this.straatnaamLabel.AutoSize = true;
            this.straatnaamLabel.Location = new System.Drawing.Point(12, 87);
            this.straatnaamLabel.Name = "straatnaamLabel";
            this.straatnaamLabel.Size = new System.Drawing.Size(68, 13);
            this.straatnaamLabel.TabIndex = 16;
            this.straatnaamLabel.Text = "* Straatnaam";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(12, 243);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(71, 13);
            this.label9.TabIndex = 22;
            this.label9.Text = "GSM-nummer";
            // 
            // telLabel
            // 
            this.telLabel.AutoSize = true;
            this.telLabel.Location = new System.Drawing.Point(12, 217);
            this.telLabel.Name = "telLabel";
            this.telLabel.Size = new System.Drawing.Size(93, 13);
            this.telLabel.TabIndex = 21;
            this.telLabel.Text = "* Telefoonnummer";
            // 
            // emailLabel
            // 
            this.emailLabel.AutoSize = true;
            this.emailLabel.Location = new System.Drawing.Point(12, 191);
            this.emailLabel.Name = "emailLabel";
            this.emailLabel.Size = new System.Drawing.Size(68, 13);
            this.emailLabel.TabIndex = 20;
            this.emailLabel.Text = "* E-mailadres";
            // 
            // annulerenBtn
            // 
            this.annulerenBtn.Location = new System.Drawing.Point(259, 331);
            this.annulerenBtn.Name = "annulerenBtn";
            this.annulerenBtn.Size = new System.Drawing.Size(75, 23);
            this.annulerenBtn.TabIndex = 23;
            this.annulerenBtn.Text = "annuleren";
            this.annulerenBtn.UseVisualStyleBackColor = true;
            this.annulerenBtn.Click += new System.EventHandler(this.annulerenBtn_Click);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(12, 336);
            this.label12.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(152, 13);
            this.label12.TabIndex = 24;
            this.label12.Text = "Velden met een * zijn verplicht.";
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(15, 274);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(95, 17);
            this.checkBox1.TabIndex = 25;
            this.checkBox1.Text = "Zakelijke klant";
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // ClientEditForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(544, 466);
            this.Controls.Add(this.checkBox1);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.annulerenBtn);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.telLabel);
            this.Controls.Add(this.emailLabel);
            this.Controls.Add(this.postcodeLabel);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.huisnummerLabel);
            this.Controls.Add(this.straatnaamLabel);
            this.Controls.Add(this.landLabel);
            this.Controls.Add(this.bedrijfsnaamLabel);
            this.Controls.Add(this.achternaamLabel);
            this.Controls.Add(this.voornaamLabel);
            this.Controls.Add(this.gsmTb);
            this.Controls.Add(this.telTb);
            this.Controls.Add(this.emailTb);
            this.Controls.Add(this.postcodeTb);
            this.Controls.Add(this.huisnummerToevoegingTb);
            this.Controls.Add(this.huisnummerTb);
            this.Controls.Add(this.straatnaamTb);
            this.Controls.Add(this.landTb);
            this.Controls.Add(this.bedrijfsnaamTb);
            this.Controls.Add(this.achternaamTb);
            this.Controls.Add(this.voornaamTb);
            this.Controls.Add(this.OpslaanBtn);
            this.Name = "ClientEditForm";
            this.Text = "ClientEditForm";
            this.Load += new System.EventHandler(this.ClientEditForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button OpslaanBtn;
        private System.Windows.Forms.TextBox voornaamTb;
        private System.Windows.Forms.TextBox achternaamTb;
        private System.Windows.Forms.TextBox bedrijfsnaamTb;
        private System.Windows.Forms.TextBox landTb;
        private System.Windows.Forms.TextBox straatnaamTb;
        private System.Windows.Forms.TextBox huisnummerTb;
        private System.Windows.Forms.TextBox huisnummerToevoegingTb;
        private System.Windows.Forms.TextBox gsmTb;
        private System.Windows.Forms.TextBox telTb;
        private System.Windows.Forms.TextBox emailTb;
        private System.Windows.Forms.TextBox postcodeTb;
        private System.Windows.Forms.Label voornaamLabel;
        private System.Windows.Forms.Label achternaamLabel;
        private System.Windows.Forms.Label bedrijfsnaamLabel;
        private System.Windows.Forms.Label landLabel;
        private System.Windows.Forms.Label postcodeLabel;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label huisnummerLabel;
        private System.Windows.Forms.Label straatnaamLabel;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label telLabel;
        private System.Windows.Forms.Label emailLabel;
        private System.Windows.Forms.Button annulerenBtn;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.CheckBox checkBox1;
    }
}
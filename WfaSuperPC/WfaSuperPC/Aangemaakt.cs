﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WfaSuperPC
{
    class Aangemaakt : BestellingenState
    {
        BestellingenMachine bestellingenMachine;

        public Aangemaakt(BestellingenMachine bm)
        {
            this.bestellingenMachine = bm;
        }


        // Wordt initieel aangeroepen
        public void maakBestelling(Klanten klant, List<ProductenTemp> productenLijst, Bestellingen bestelling, Personen ingelogdePersoon, SuperPcContainer context)
        {
            // Bestellenform wordt opgedeeld in klant, producten, etc
            // Onderdelen worden opgeslagen in db.


            var bestellingen = context.Set<Bestellingen>();
            var bestelregels = context.Set<Bestelregels>();


            //bestellingen.
            bestellingen.Add(bestelling);

            // nieuwe bestelling
            // var bestellingRow = bestellingenContext.Bestellingen.NewBestellingenRow();
            
            // Voor elke ProductenTemp een nieuwe bestelregel aanmaken.
            foreach (ProductenTemp productenTemp in productenLijst)
            {
                Bestelregels bestelRegel = new Bestelregels();
            
                bestelRegel.BestellingenId = bestelling.Id;
                bestelRegel.Aantal = productenTemp.Aantal;
                bestelRegel.ProductenId = productenTemp.Id;

                // Referentie van de bestelregel naar de bestelling plaatsten.
                bestelRegel.Bestellingen = bestelling;

                // Tijdelijk maar even de huidige persoon voor de assemblage.
                bestelRegel.Personen = ingelogdePersoon;

                bestelregels.Add(bestelRegel);
            }

            // Ingelogde persoon toevoegen
            bestelling.Personen = ingelogdePersoon;

            /* Statussen:
             * 
             * aangemaakt
             * managerGoedkeuring
             * managerAfkeuring
             * klantGoedkeuring
             * klantAfkeuring
             * geassembleerd
             */

            var statussen =
                from status in context.Statussen
                where status.Naam == "aangemaakt"
                select new
                {
                    statusId = status.Id
                };

            foreach (var status in statussen)
            {
                bestelling.StatussenId = status.statusId;
            }
            
            // Wijzigingen opslaan.
            context.SaveChanges();
            
        }
        

        public void managerKeurGoed(Bestellingen bestelling)
        {

        }

        public void managerKeurAf(Bestellingen bestelling)
        {

        }

        public void klantKeurGoed(Bestellingen bestelling)
        {

        }

        public void klantKeurAf(Bestellingen bestelling)
        {

        }

        public void assembleren(Bestellingen bestelling)
        {

        }
    }
}

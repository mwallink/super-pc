﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WfaSuperPC
{
    public partial class PersonenBeheerForm : AuthorizedForm
    {
        private List<Rollen> _huidigeRollen = new List<Rollen>();
        private List<Rollen> _mogelijkeRollen = new List<Rollen>();
        SuperPcContainer superpcontainer = new SuperPcContainer();

        SuperPcContainer Context
        {
            get
            {
                return superpcontainer;
            }
            set
            {
                superpcontainer = value;
            }
        }


        private Personen GeselecteerdPersoon{
            get
            {
                int persoonsid = Convert.ToInt32(personenDataGridView[0, personenDataGridView.CurrentRow.Index].Value.ToString());
                Personen huidigepersoon = (Personen)(from p in Context.Personen
                                                    where p.Id == persoonsid
                                                    select p).Single();
                return huidigepersoon;
            }
            set
            {
                throw new NotSupportedException();
            }
        }

        public PersonenBeheerForm()
        {
            InitializeComponent();
        }

        private void PersonenBeheerForm_Load(object sender, EventArgs e)
        {
            this.personenRollenTableAdapter.Fill(this.personenRollenDataSet.PersonenRollen);
            this.personenTableAdapter.Fill(this.personenRollenDataSet.Personen);

            //bind data to lists
            refreshListRollen();

            //bind data aan listboxen
            refreshListBoxDataSources();
        }

        private void personenBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.personenBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.personenRollenDataSet);
        }

        private void rollenDataGridView_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {

            /*
            LOOP DOOR ROWS VAN DATAGRIDVIEW ==== CELLEN AANPASSEN OP BASIS VAN LINQ QUERY
            //if(!dgv.Name.ToString().Equals('rollenDataGridView',StringComparison.Ordinal))
            
            //get rol
            SuperPcContainer context = new SuperPcContainer();
            var query = from r in context.Rollen select r;

            //loop door datagridview, elke cel in de de unbound column rol naam krijgt de vol naam uit de linq query
            for (int i = 0; i <= this.rollenDataGridView.Rows.Count - 2 && this.rollenDataGridView.Rows.Count > 1; i++)
            {
                //get rolid uit de datagridview
                var rolid = Convert.ToInt32(rollenDataGridView.Rows[i].Cells[1].Value);
                //op basis van het rolid neem de rol uit de mainquery op basis van het rolid in het datagridview
                Rollen r = (Rollen)query.Where(rol => rol.Id == rolid).Single();
                rollenDataGridView.Rows[i].Cells[2].Value = r.Naam;
            }
           */
        }

        private void rollenDataGridView_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            //get rol
            var rolid = Convert.ToInt32(rollenDataGridView.Rows[e.RowIndex].Cells[1].Value);
            Rollen rol = (Rollen)(from r in Context.Rollen where r.Id == rolid select r).Single();
            rollenDataGridView.Rows[e.RowIndex].Cells[2].Value = rol.Naam;
        }

        private void btnVoegRolToe_Click(object sender, EventArgs e)
        {
            if (this.lbxMogelijkeRollen.SelectedItems.Count != 0)
            {
                
                foreach(object item in lbxMogelijkeRollen.SelectedItems){
                    Rollen rolnaam = item as Rollen;
                    this._huidigeRollen.Add(rolnaam);
                    this._mogelijkeRollen.Remove(rolnaam);

                    GeselecteerdPersoon.Rollen.Add(rolnaam);
                }
                
                Context.SaveChanges();
                refreshListBoxDataSources();
            }
        }

        private void refreshListRollen()
        {
            if (personenDataGridView.SelectedRows.Count > 0)
            {
                if (Convert.ToInt32(personenDataGridView.CurrentRow.Cells[0].Value) != -1)
                {
                    //get huidige rollen
                    this._huidigeRollen = GeselecteerdPersoon.Rollen.ToList();

                    //get mogelijke
                    this._mogelijkeRollen = (from r in Context.Rollen
                                             from p in r.Personen
                                             where p.Id != GeselecteerdPersoon.Id
                                             select r).ToList();
                }
                else
                {
                    this._huidigeRollen.Clear();
                    this._mogelijkeRollen.Clear();
                }
            }
        }

        private void refreshListBoxDataSources()
        {
            this.lbxHuidigeRollen.DataSource = null;
            this.lbxHuidigeRollen.DataSource = this._huidigeRollen;
            this.lbxHuidigeRollen.DisplayMember = "Naam";
            this.lbxHuidigeRollen.ValueMember = "Id";

            this.lbxMogelijkeRollen.DataSource = null;
            this.lbxMogelijkeRollen.DataSource = this._mogelijkeRollen;
            this.lbxMogelijkeRollen.DisplayMember = "Naam";
            this.lbxMogelijkeRollen.ValueMember = "Id";
        }

        private void btnVerwijderRol_Click(object sender, EventArgs e)
        {
            if (this.lbxHuidigeRollen.SelectedItems.Count != 0)
            {
                
                foreach(object item in lbxHuidigeRollen.SelectedItems){
                    Rollen rolnaam = item as Rollen;
                    this._mogelijkeRollen.Add(rolnaam);
                    this._huidigeRollen.Remove(rolnaam);

                    GeselecteerdPersoon.Rollen.Add(rolnaam);
                }
                
                Context.SaveChanges();
                refreshListBoxDataSources();
            }
        }

        private void personenDataGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void personenDataGridView_SelectionChanged(object sender, EventArgs e)
        {
                    refreshListRollen();
                    refreshListBoxDataSources();
        }
    }
}

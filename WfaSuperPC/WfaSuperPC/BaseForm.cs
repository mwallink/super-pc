﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WfaSuperPC
{
    public partial class BaseForm : AuthorizedForm
    {
        private InlogForm inlogform;

        public BaseForm(): base()
        {
            InitializeComponent();

            //
            //Check op bestaande gebruiker
            //Deze methode doet een check of de applicatie controls in de db aan het registeren is.
            //
            if (AppConfigSuperPC.registeringConrolsOperation == false)
            {
                inlogform = new InlogForm();
                inlogform.ShowDialog();
                if (inlogform.LoggedIn)
                {
                    this.PersoonId = inlogform.PersoonId;
                    this.Visible = true;
                }
                else
                {
                    this.Close();
                }
            }
        }

        private void BaseForm_Load(object sender, EventArgs e)
        {

        }

        private void toolStripButtonRollen_Click(object sender, EventArgs e)
        {
            RollenBeheerForm rf = new RollenBeheerForm();
            rf.Show();
        }

        private void toolStripButtonPersonen_Click(object sender, EventArgs e)
        {
            PersonenBeheerForm pf = new PersonenBeheerForm();
            pf.Show();
        }

        private void toolStripButtonBestellen_Click(object sender, EventArgs e)
        {
            BestellenForm bf = new BestellenForm(this.PersoonId);
            bf.Show();
        }
        
        private void toolStripButtonProducten_Click(object sender, EventArgs e)
        {
            ProductenForm pf = new ProductenForm();
            pf.Show();
        }

        private void toolStripButtonKlanten_Click(object sender, EventArgs e)
        {
            ClientsForm cf = new ClientsForm();
            cf.Show();
        }

        private void toolStripButtonAssemblage_Click(object sender, EventArgs e)
        {
            AssemblageForm af = new AssemblageForm(this.PersoonId);
            af.Show();
        }

        private void toolStripButtonStatistieken_Click(object sender, EventArgs e)
        {
            StatistiekenForm sf = new StatistiekenForm(this.PersoonId);
            sf.Show();
        }

        private void BaseForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void bestellingenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            BestellenForm bf = new BestellenForm(this.PersoonId);
            bf.Show();
        }



        private void openstaandeBestellingenToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            // lijst met bestellingen
            Openstaand of = new Openstaand(this.PersoonId);
            of.Show();
        }
    }
}
